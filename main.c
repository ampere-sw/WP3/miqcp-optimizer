#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <float.h>
#include <unistd.h>
#include <json-c/json.h>
#include <fcntl.h>
#include <signal.h>

#include "dag.h"
#include "log.h"


sys_t *p_sys = NULL;
char *out_lp_fname = NULL;
char *out_sol_fname = NULL;
char *out_sols_pname = "gurobi_sols";
char *out_dot_fname = NULL;
char *out_yml_fname = NULL;
char *out_sh_fname = NULL;
char *out_yml_rtsim_dags_fname = NULL;
char *out_yml_rtsim_isl_fname = NULL;
char *out_yaml_fname = NULL;
char *out_json_fname = NULL;
char *gurobi_log_fname = NULL;
char *platform_name = NULL;
char *in_unrel_fname = NULL;
int gurobi_max_threads = MAX_GUROBI_THREADS;
double lp_scale = 0.0;
double max_util = DEF_MAX_UTIL;
double pow_exec_times_scale = 1.0;
double min_deadline = 0;
int create_non_binary_bounds = 0;
char *json_fname[MAX_JSON_FILES];
int n_json_files = 0;
int dot_hide_aaf = 0;
int num_simplify = 0;
int new_comp_unrelated = 0;

// Tom's simplest example
// s { {a1 a2} | {b1 b2} } e
dag_t *build_dag1() {
  printf("ex1:\n");
  dag_t *p_dag = malloc(sizeof(dag_t));
  assert(p_dag != NULL);
  dag_init(p_dag, 100, 100);
  int s = dag_add_elem(p_dag, 10, 0, -1);
  int a1 = dag_add_elem(p_dag, 5, 0, -1);
  int a2 = dag_add_elem(p_dag, 8, 0, -1);
  int b1 = dag_add_elem(p_dag, 7, 0, -1);
  int b2 = dag_add_elem(p_dag, 4, 0, -1);
  int e = dag_add_elem(p_dag, 3, 0, -1);
  dag_add_prev(p_dag, a1, s);
  dag_add_prev(p_dag, a2, a1);
  dag_add_prev(p_dag, b1, s);
  dag_add_prev(p_dag, b2, b1);
  dag_add_prev(p_dag, e, a2);
  dag_add_prev(p_dag, e, b2);
  dag_comp_unrelated(p_dag);
  dag_dump(p_dag);
  printf("Unrelated: ");
  dag_dump_unrelated(p_dag);

  dag_path_t p;
  dag_path_init(&p);
  int len = dag_longest_path(p_dag, p_sys, &p);
  printf("longest path: %d [", len);
  dag_path_dump(&p);
  printf(" ]\n");

  dag_set_deadlines(p_dag, p_sys, s, e, p_dag->deadline, 1);
  dag_dump(p_dag);

  printf("\n");

  return p_dag;
}

/* Alexandre's example (formatted with graph-easy :-(, dot -Tpdf makes a much better job!)
  +-------------------------------------+
  |                                     |
  |  +----+     +----+     +----+     +----+     +----+     +----+
  |  | n3 | <-- | n0 | --> | n1 | --> |    | --> | n6 | --> | n9 | <+
  |  +----+     +----+     +----+     |    |     +----+     +----+  |
  |    |          |                   |    |                  ^     |
  |    |          |                   | n4 |                  |     |
  |    v          v                   |    |                  |     |
  |  +----+     +----+                |    |     +----+       |     |
  |  | n5 | <-- | n2 | -------------> |    | --> | n8 | ------+-----+
  |  +----+     +----+                +----+     +----+       |
  |    |                                                      |
  |    |                                                      |
  |    v                                                      |
  |  +----+                                                   |
  +> | n7 | --------------------------------------------------+
     +----+
*/
void ex2() {
  printf("ex2:\n");
  dag_t d;
  dag_init(&d, 100, 100);
  int n[10]; 
  for (int i=0; i < 10; i++){
    n[i] = dag_add_elem(&d, 10, 0, -1);
  }
  d.elems[n[0]].C = 0;
  d.elems[n[3]].C = 15;
  d.elems[n[9]].C = 0;
  dag_add_prev(&d, n[1], n[0]);
  dag_add_prev(&d, n[2], n[0]);
  dag_add_prev(&d, n[3], n[0]);
  dag_add_prev(&d, n[4], n[1]);
  dag_add_prev(&d, n[4], n[2]);
  dag_add_prev(&d, n[5], n[2]);
  dag_add_prev(&d, n[5], n[3]);
  dag_add_prev(&d, n[8], n[3]);
  dag_add_prev(&d, n[6], n[4]);
  dag_add_prev(&d, n[7], n[4]);
  dag_add_prev(&d, n[8], n[4]);
  dag_add_prev(&d, n[7], n[5]);
  dag_add_prev(&d, n[9], n[6]);
  dag_add_prev(&d, n[9], n[7]);
  dag_add_prev(&d, n[9], n[8]);

  dag_update_first_last(&d);
  dag_comp_unrelated(&d);
  dag_dump(&d);
  printf("Unrelated: ");
  dag_dump_unrelated(&d);
  // { 0, }, { 9, }, { 6, 7, 8, }, { 3, 6, }, { 5, 6, 8, }, { 1, 5, }, { 4, 5, }, { 1, 2, 3, }, { 3, 4, }, 

  dag_path_t p;
  dag_path_init(&p);
  int len = dag_longest_path(&d, p_sys, &p);
  printf("longest path: %d [", len);
  dag_path_dump(&p);
  printf(" ]\n");

  dag_set_deadlines(&d, p_sys, n[0], n[9], d.deadline, 1);
  dag_dump(&d);
  dag_dump_dot(&d, "ex2.dot");

  dag_cleanup(&d);
  printf("\n");
}


/*
ATTENTION: the power data in this platform is mostl likely out of date compared to the most recent
platforms such as xu4 and zcu102. 
*/
// raspberry pi 4, model B,  with a Broadcom BCM2711B0, which includes four Cortex-A72 CPUs
sys_t *build_raspberry_pi4b() {
  sys_t *p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);
  island_t *cpus = sys_add_island(p_sys, 1.0, 4, SYS_SCHED_EDF, CPU, 0);
  // adopting MHz and W as standard units
  island_add_freq(cpus, 1500, 3.67352153779488,	3.110947);
  island_add_freq(cpus, 1400, 3.60407547355613,	3.084576);
  island_add_freq(cpus, 1300, 3.53263941668691,	3.057185);
  island_add_freq(cpus, 1200, 3.46428389764674,	3.03105516549649);
  island_add_freq(cpus, 1100, 3.39549793901795,	3.00343429286608);
  island_add_freq(cpus, 1000, 3.32966816187627,	2.97858358358358);
  island_add_freq(cpus, 900	, 3.26295850316887,	2.95137048192771);
  island_add_freq(cpus, 800	, 3.19891252784989,	2.92600250626566);
  island_add_freq(cpus, 700	, 3.13479404570291,	2.89901879699248);
  island_add_freq(cpus, 600	, 3.04526071906541,	2.855955);
  
  return p_sys;	
}


// ZCU102 board assuming the FPGA w a single slot with capacity 5x compared to the cpus
sys_t *build_zcu102_1slot() {
  sys_t *p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);
  island_t *cpus = sys_add_island(p_sys, 1.0, 4, SYS_SCHED_EDF, CPU, 0);
  // adopting MHz and W as standard units
  // this power data is updated using cpu-power-profiling-rt-dag.sh script in 
  // https://github.com/amamory-ampere/rtas22-dart-design.git
  // which runs 'rt-dag -t', which is the actual workload run by rt-dag.
  // Interestingly, cpu-power-profiling.sh, which is the equivalent power script that runs 'yes'
  // produced a veeery similar power profile as rt-dag. 
  island_add_freq(cpus, 1199.0, 1.82729 , 1.68586);
  island_add_freq(cpus, 599.0 , 1.72455 , 1.65163);
  island_add_freq(cpus, 399.0 , 1.6822  , 1.63534);
  island_add_freq(cpus, 299.0 , 1.65982 , 1.62216);

  island_t *p_slot0 = sys_add_island(p_sys, 5.0, 1, SYS_SCHED_EDF, ACCELERATOR, 0);
  // BIIIIG OBS: the idle PL power profile is accurate, but not the busy.
  // It turns out that the busy power depends not on the reconfig region size, but on the 
  // especific task size running. This means that, if a RR has a big and a small IP, 
  // the power consumption will change drastically. For instance, preliminary results 
  // shows that a 128x128 mat_mul consumes about 1.2W and a 16x16 mat_mul consumes 0.9W when idle.
  // Another very important thing is the instant fpga power depends on the last configuration.
  // Meaning that, even the IP is not used, it continues to draw the same power when it was being used
  // When the 128x128 IP is busy, we observed 0.119204 W of power increase. When the 16x16 Ip was busy,
  // the observed power increase was 0.07034 W. Since this variation are quite small, we are using the 
  // max delta power to generate the 'island busy power'. For this reason, the busy power is 
  // 0.812484 + 0.119204 = 0.93169
  island_add_freq(p_slot0, 100.00, 0.93169, 0.812484);
  
  return p_sys;	
}

// ZCU102 board assuming the FPGA w a single slot with mat64 IP
// Check power-aware-optimization/tools/zcu102_scripts/readme.md for more information how those numbers were obtained
sys_t *build_zcu102_mat64() {
  sys_t *p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);
  island_t *cpus = sys_add_island(p_sys, 1.0, 4, SYS_SCHED_EDF, CPU, 0);
  // adopting MHz and W as standard units
  // this power data is updated using cpu-power-profiling-rt-dag.sh script in 
  // https://github.com/amamory-ampere/rtas22-dart-design.git
  // which runs 'rt-dag -t', which is the actual workload run by rt-dag.
  // Interestingly, cpu-power-profiling.sh, which is the equivalent power script that runs 'yes'
  // produced a veeery similar power profile as rt-dag. 
  island_add_freq(cpus, 1199.0, 1.82729 , 1.68586);
  island_add_freq(cpus, 599.0 , 1.72455 , 1.65163);
  island_add_freq(cpus, 399.0 , 1.6822  , 1.63534);
  island_add_freq(cpus, 299.0 , 1.65982 , 1.62216);

  // configuration time is zero because there is a single IP
  island_t *p_slot0 = sys_add_island(p_sys, 2.12, 1, SYS_SCHED_EDF, ACCELERATOR, 0);
  // BIIIIG OBS: the idle PL power profile is accurate, but not the busy.
  // The same OBS in 'build_zcu102_1slot' applies here
  // Using the busy and idle power used in the paper submission
  island_add_freq(p_slot0, 100.00, 0.9, 0.88);
  
  return p_sys;	
}


// ZCU102 board assuming the FPGA w a single slot with mat128 IP
// Check power-aware-optimization/tools/zcu102_scripts/readme.md for more information how those numbers were obtained
sys_t *build_zcu102_mat128() {
  sys_t *p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);
  island_t *cpus = sys_add_island(p_sys, 1.0, 4, SYS_SCHED_EDF, CPU, 0);
  // adopting MHz and W as standard units
  // this power data is updated using cpu-power-profiling-rt-dag.sh script in 
  // https://github.com/amamory-ampere/rtas22-dart-design.git
  // which runs 'rt-dag -t', which is the actual workload run by rt-dag.
  // Interestingly, cpu-power-profiling.sh, which is the equivalent power script that runs 'yes'
  // produced a veeery similar power profile as rt-dag. 
  island_add_freq(cpus, 1199.0, 1.82729 , 1.68586);
  island_add_freq(cpus, 599.0 , 1.72455 , 1.65163);
  island_add_freq(cpus, 399.0 , 1.6822  , 1.63534);
  island_add_freq(cpus, 299.0 , 1.65982 , 1.62216);

  // configuration time is zero because there is a single IP
  island_t *p_slot0 = sys_add_island(p_sys, 7.87, 1, SYS_SCHED_EDF, ACCELERATOR, 0);
  // BIIIIG OBS: the idle PL power profile is accurate, but not the busy.
  // The same OBS in 'build_zcu102_1slot' applies here
  // Using the busy and idle power used in the paper submission
  island_add_freq(p_slot0, 100.00, 1.14, 1.19);
  
  return p_sys;	
}

// ZCU102 board assuming the FPGA w a single slot with mat64 and mat128 IPs
// the only difference compared w 'build_zcu102_mat128' is that now the reconfiguration
// time cannot be ignored since there are 2 diff IPs to the same slot
// Check power-aware-optimization/tools/zcu102_scripts/readme.md for more information how those numbers were obtained
sys_t *build_zcu102_mat64_mat128() {
  sys_t *p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);
  island_t *cpus = sys_add_island(p_sys, 1.0, 4, SYS_SCHED_EDF, CPU, 0);
  // adopting MHz and W as standard units
  // this power data is updated using cpu-power-profiling-rt-dag.sh script in 
  // https://github.com/amamory-ampere/rtas22-dart-design.git
  // which runs 'rt-dag -t', which is the actual workload run by rt-dag.
  // Interestingly, cpu-power-profiling.sh, which is the equivalent power script that runs 'yes'
  // produced a veeery similar power profile as rt-dag. 
  island_add_freq(cpus, 1199.0, 1.82729 , 1.68586);
  island_add_freq(cpus, 599.0 , 1.72455 , 1.65163);
  island_add_freq(cpus, 399.0 , 1.6822  , 1.63534);
  island_add_freq(cpus, 299.0 , 1.65982 , 1.62216);

  // configuration time is NOT zero because there are two IPs. The reconf time of the biggest IP is used in this case 
  // note that capacity of 1.33 is used, which is the speedup obtained assuming that fpga reconfiguration time is accounted.
  // check power-aware-optimization/tools/zcu102_scripts/readme.md for more information how those numbers were obtained
  island_t *p_slot0 = sys_add_island(p_sys, 1.33, 1, SYS_SCHED_EDF, ACCELERATOR, 22664000);
  // BIIIIG OBS: the idle PL power profile is accurate, but not the busy.
  // The same OBS in 'build_zcu102_1slot' applies here
  // Using the busy and idle power used in the paper submission
  island_add_freq(p_slot0, 100.00, 1.14, 1.19);
  
  return p_sys;	
}

// ZCU102 board assuming the FPGA w a single slot with mat64 and mat128 IPs
// the only difference compared w 'build_zcu102_mat128' is that now the reconfiguration
// time cannot be ignored since there are 2 diff IPs to the same slot
// Check power-aware-optimization/tools/zcu102_scripts/readme.md for more information how those numbers were obtained
sys_t *build_zcu102_mat64_mat128_highest_freq() {
  sys_t *p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);
  island_t *cpus = sys_add_island(p_sys, 1.0, 4, SYS_SCHED_EDF, CPU, 0);
  // adopting MHz and W as standard units
  // this power data is updated using cpu-power-profiling-rt-dag.sh script in
  // https://github.com/amamory-ampere/rtas22-dart-design.git
  // which runs 'rt-dag -t', which is the actual workload run by rt-dag.
  // Interestingly, cpu-power-profiling.sh, which is the equivalent power script that runs 'yes'
  // produced a veeery similar power profile as rt-dag.
  island_add_freq(cpus, 1199.0, 1.82729 , 1.68586);

  // configuration time is NOT zero because there are two IPs. The reconf time of the biggest IP is used in this case
  // note that capacity of 1.33 is used, which is the speedup obtained assuming that fpga reconfiguration time is accounted.
  // check power-aware-optimization/tools/zcu102_scripts/readme.md for more information how those numbers were obtained
  island_t *p_slot0 = sys_add_island(p_sys, 1.33, 1, SYS_SCHED_EDF, ACCELERATOR, 23664000);
  // BIIIIG OBS: the idle PL power profile is accurate, but not the busy.
  // The same OBS in 'build_zcu102_1slot' applies here
  // Using the busy and idle power used in the paper submission
  island_add_freq(p_slot0, 100.00, 1.14, 1.19);

  return p_sys;
}

/*
ATTENTION: the power data in this platform is mostl likely out of date compared to the most recent
platforms such as xu4 and zcu102. 
*/
// ODROID XU3 (half of the available frequencies)
sys_t *build_biglittle_half(int n_big, int n_little, double cap_little) {
  sys_t *p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);
  island_t *p_big = sys_add_island(p_sys, 1.0, n_big, SYS_SCHED_EDF, CPU, 0);
  // adopting MHz and W as standard units
  island_add_freq(p_big, 2000.00, 2.08265621783625	, 0.412970993421053);
  island_add_freq(p_big, 1800.00, 1.50253770423977	, 0.282202837719298);
  island_add_freq(p_big, 1600.00, 1.13424830635965	, 0.210462201754386);
  island_add_freq(p_big, 1400.00, 0.868902175	    , 0.161186936403509);
  island_add_freq(p_big, 1200.00, 0.667705702997076	, 0.12385000877193 );
  island_add_freq(p_big, 1000.00, 0.500931594005848	, 0.095939989035088);
  island_add_freq(p_big,  800.00, 0.360382067178363	, 0.070789282894737);
  island_add_freq(p_big,  600.00, 0.266906553801169	, 0.055386111842105);
  island_add_freq(p_big,  400.00, 0.18774763011696	, 0.04395965131579 );
  island_add_freq(p_big,  200.00, 0.10236486498538	, 0.029714405701754);

  island_t *p_lit = sys_add_island(p_sys, cap_little, n_little, SYS_SCHED_EDF, CPU, 0);
  island_add_freq(p_lit, 1400.00, 0.281703834722222	,0.093324004385965);
  island_add_freq(p_lit, 1200.00, 0.201248805994152	,0.06539486622807 );
  island_add_freq(p_lit, 1000.00, 0.146737925657895	,0.047670885964912);
  island_add_freq(p_lit,  800.00, 0.102453253435672	,0.033128201754386);
  island_add_freq(p_lit,  600.00, 0.067276892690058	,0.021402447368421);
  island_add_freq(p_lit,  400.00, 0.042896272660819	,0.014739322368421);
  island_add_freq(p_lit,  200.00, 0.023366738377193	,0.009238804824561);
  
  return p_sys;	
}

/*
ATTENTION: the power data in this platform is mostl likely out of date compared to the most recent
platforms such as xu4 and zcu102. 
*/
// ODROID XU3 with all available frequencies
sys_t *build_biglittle(int n_big, int n_little, double cap_little) {
  sys_t *p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);
  island_t *p_big = sys_add_island(p_sys, 1.0, n_big, SYS_SCHED_EDF, CPU, 0);
  // adopting MHz and W as standard units
  island_add_freq(p_big, 2000.00, 2.08265621783625	, 0.412970993421053);
  island_add_freq(p_big, 1900.00, 1.77142172266082	, 0.325980447368421);
  island_add_freq(p_big, 1800.00, 1.50253770423977	, 0.282202837719298);
  island_add_freq(p_big, 1700.00, 1.30678805233919	, 0.240915076754386);
  island_add_freq(p_big, 1600.00, 1.13424830635965	, 0.210462201754386);
  island_add_freq(p_big, 1500.00, 0.978278668055556	, 0.18216388377193 );
  island_add_freq(p_big, 1400.00, 0.868902175	    , 0.161186936403509);
  island_add_freq(p_big, 1300.00, 0.766021783625731	, 0.143356778508772);
  island_add_freq(p_big, 1200.00, 0.667705702997076	, 0.12385000877193 );
  island_add_freq(p_big, 1100.00, 0.582060522953216	, 0.110361934210526);
  island_add_freq(p_big, 1000.00, 0.500931594005848	, 0.095939989035088);
  island_add_freq(p_big,  900.00, 0.426750057236842	, 0.082052885964912);
  island_add_freq(p_big,  800.00, 0.360382067178363	, 0.070789282894737);
  island_add_freq(p_big,  700.00, 0.308482990862573	, 0.062486721491228);
  island_add_freq(p_big,  600.00, 0.266906553801169	, 0.055386111842105);
  island_add_freq(p_big,  500.00, 0.227691598099415	, 0.049942203947368);
  island_add_freq(p_big,  400.00, 0.18774763011696	, 0.04395965131579 );
  island_add_freq(p_big,  300.00, 0.145339782821638	, 0.036667206140351);
  island_add_freq(p_big,  200.00, 0.10236486498538	, 0.029714405701754);

  island_t *p_lit = sys_add_island(p_sys, cap_little, n_little, SYS_SCHED_EDF, CPU, 0);
  island_add_freq(p_lit, 1500.00, 0.322891321710526	,0.109331984649123);
  island_add_freq(p_lit, 1400.00, 0.281703834722222	,0.093324004385965);
  island_add_freq(p_lit, 1300.00, 0.239441758991228	,0.079200675438597);
  island_add_freq(p_lit, 1200.00, 0.201248805994152	,0.06539486622807 );
  island_add_freq(p_lit, 1100.00, 0.172303840789473	,0.05617760745614 );
  island_add_freq(p_lit, 1000.00, 0.146737925657895	,0.047670885964912);
  island_add_freq(p_lit,  900.00, 0.122801795321637	,0.039642149122807);
  island_add_freq(p_lit,  800.00, 0.102453253435672	,0.033128201754386);
  island_add_freq(p_lit,  700.00, 0.084834951169591	,0.027034129385965);
  island_add_freq(p_lit,  600.00, 0.067276892690058	,0.021402447368421);
  island_add_freq(p_lit,  500.00, 0.052146664035088	,0.017003475877193);
  island_add_freq(p_lit,  400.00, 0.042896272660819	,0.014739322368421);
  island_add_freq(p_lit,  300.00, 0.032978425950292	,0.011662326754386);
  island_add_freq(p_lit,  200.00, 0.023366738377193	,0.009238804824561);

  return p_sys;
}

// ODROID XU4 with all NON turbo-boosted available frequencies
sys_t *build_biglittle_xu4(int n_big, int n_little, double cap_little) {
  sys_t *p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);

  island_t *big    = sys_add_island(p_sys, 1.0, n_big, SYS_SCHED_EDF, CPU, 0);
  island_t *little = sys_add_island(p_sys, cap_little, n_little, SYS_SCHED_EDF, CPU, 0);

  // Adopting MHz and W as standard units. NOTE: using only frequencies up to
  // 1.4GHz also for the big, so that we know for sure (empirically tested) that
  // we will never be throttled.
  island_add_freq(   big, 1400., 2.863, 1.820);
  island_add_freq(   big, 1300., 2.719, 1.795);
  island_add_freq(   big, 1200., 2.594, 1.775);
  island_add_freq(   big, 1100., 2.473, 1.748);
  island_add_freq(   big, 1000., 2.358, 1.733);
  island_add_freq(   big,  900., 2.233, 1.708);
  island_add_freq(   big,  800., 2.166, 1.703);
  island_add_freq(   big,  700., 2.099, 1.693);
  island_add_freq(   big,  600., 2.031, 1.684);
  island_add_freq(   big,  500., 1.972, 1.680);
  island_add_freq(   big,  400., 1.903, 1.672);
  island_add_freq(   big,  300., 1.835, 1.666);
  island_add_freq(   big,  200., 1.767, 1.650);

  island_add_freq(little, 1400., 1.931, 1.763);
  island_add_freq(little, 1300., 1.893, 1.752);
  island_add_freq(little, 1200., 1.853, 1.732);
  island_add_freq(little, 1100., 1.822, 1.715);
  island_add_freq(little, 1000., 1.795, 1.707);
  island_add_freq(little,  900., 1.765, 1.695);
  island_add_freq(little,  800., 1.742, 1.681);
  island_add_freq(little,  700., 1.729, 1.676);
  island_add_freq(little,  600., 1.706, 1.670);
  island_add_freq(little,  500., 1.702, 1.666);
  island_add_freq(little,  400., 1.682, 1.663);
  island_add_freq(little,  300., 1.687, 1.659);
  island_add_freq(little,  200., 1.672, 1.650);

  return p_sys;
}

// this is a hypothetical hw platform with odroid-xu4 CPUs and ZCU102 FPGA. 
// Use to check how a acc model would perform if odroid-xu4 had support to acceleration
sys_t *build_xu4_fpga(int n_big, int n_little, double cap_little) {
    sys_t *p_sys = build_biglittle_xu4(n_big,n_little, cap_little);

    // add only the acc island used in `build_zcu102_mat64_mat128`
    // configuration time is NOT zero because there are two IPs. The reconf time of the biggest IP is used in this case 
    // note that capacity of 1.33 is used, which is the speedup obtained assuming that fpga reconfiguration time is accounted.
    // check power-aware-optimization/tools/zcu102_scripts/readme.md for more information how those numbers were obtained
    island_t *p_slot0 = sys_add_island(p_sys, 1.33, 1, SYS_SCHED_EDF, ACCELERATOR, 22664000);
    // BIIIIG OBS: the idle PL power profile is accurate, but not the busy.
    // The same OBS in 'build_zcu102_1slot' applies here
    // Using the busy and idle power used in the paper submission
    island_add_freq(p_slot0, 100.00, 1.14, 1.19);   

    return p_sys; 
}


sys_t *build_dynamiq(int n_big, int n_middle, double cap_middle, int n_little, double cap_little) {
  sys_add_island(p_sys, 1.0, n_big, SYS_SCHED_EDF, CPU, 0);
  sys_add_island(p_sys, cap_middle, n_middle, SYS_SCHED_EDF, CPU, 0);
  sys_add_island(p_sys, cap_little, n_little, SYS_SCHED_EDF, CPU, 0);
  return p_sys;
}

// From /sys/devices/system/cpu/cpufreq/policy0/scaling_available_frequencies
//static double xavier_cpu_freqs[] = { 2265.600, 2188.800, 2112.000, 2035.200, 1958.400, 1881.600, 1804.800, 1728.000, 1651.200, 1574.400, 1497.600, 1420.800, 1344.000, 1267.200, 1190.400, 1113.600, 1036.800, 960.000, 883.200, 806.400, 729.600, 652.800, 576.000, 499.200, 422.400, 345.600, 268.800, 192.000 };
static double xavier_cpu_freqs[] = {2265.600, 1190.400, 729.600};
// From /sys/devices/17000000.gv11b/devfreq_dev/available_frequencies
static double xavier_gpu_freqs[] = { 1377.000000, 1338.750, 1236.750, 1198.500, 1032.750000, 905.250, 828.750, 675.750000, 624.750000};
// from ETHZ
static double xavier_gpu_busy_pows[] = {6.727369905956113, 6.282901111993646, 5.165062958671302, 4.779793984962406, 3.5055584648493546, 2.8488735244519394, 2.5167658330626828, 2.067785883047844, 1.9662901166018878};
// from ETHZ
static double xavier_gpu_idle_pows[] = {5.371, 4.939, 4.062, 3.752, 2.711, 2.213, 1.918, 1.589, 1.475};

// 1 8-CPU island with 28 frequencies, and a power consumption ~ f^2
// 1 single-GPU accelerator island with 14 frequencies, and a power consumption ~ f^2
// power numbers roughly inferred from:
//   https://forums.developer.nvidia.com/t/power-consumption-difference-between-agx-and-nx/175848
sys_t *build_xavier_agx() {
  island_t *p_isl = sys_add_island(p_sys, 1.0, 8, SYS_SCHED_EDF, CPU, 0);
  for (int i = 0; i < sizeof(xavier_cpu_freqs) / sizeof(xavier_cpu_freqs[0]); i++) {
    double fratio = xavier_cpu_freqs[i] / xavier_cpu_freqs[0];
//    island_add_freq(p_isl, xavier_cpu_freqs[i], 0.0, 7.622/8 * fratio*fratio);
    island_add_freq(p_isl, xavier_cpu_freqs[i], 0.0, 1.622/8 * fratio*fratio);
  }
  p_isl = sys_add_island(p_sys, 1.0, 1, SYS_SCHED_FIFO, ACCELERATOR, 28000000);
  for (int i = 0; i < sizeof(xavier_gpu_freqs) / sizeof(xavier_gpu_freqs[0]); i++)
    island_add_freq(p_isl, xavier_gpu_freqs[i], xavier_gpu_busy_pows[i], xavier_gpu_idle_pows[i]);

  return p_sys;
}

// old simplified model
//sys_t *build_xavier_agx() {
//  sys_t *p_sys = malloc(sizeof(sys_t));
//  assert(p_sys != NULL);
//  sys_init(p_sys);
//
//  for (int i = 0; i < 8; i++) {
//    island_t *p_isl = sys_add_island(p_sys, 1.0, 1, SYS_SCHED_EDF, CPU, 0);
//    island_add_freq(p_isl, 729.6, 0.0, 1.0);
//    island_add_freq(p_isl, 1190.4, 0.0, 1.0);
//    island_add_freq(p_isl, 2265.6, 0.0, 1.0);
//  }
//
////  island_t *gpu = sys_add_island(p_sys, 1.33, 1, SYS_SCHED_EDF, ACCELERATOR, 0);
////  island_add_freq(gpu, 100.00, 1.14, 1.19);
//  return p_sys;
//}

// set the predefined platforms by name
void set_platform(const char *name){
    if (strcmp(name, "odroid-xu3") == 0) {
      p_sys = build_biglittle(4, 4, 0.526);
      LOG(INFO,"Added new odroid-xu3 system%s\n","");
    } else if (strcmp(name, "odroid-xu3-half") == 0) {
      p_sys = build_biglittle_half(4, 4, 0.526);
      LOG(INFO,"Added new odroid-xu3-half system%s\n","");
    } else if (strcmp(name, "odroid-xu4") == 0) {
      p_sys = build_biglittle_xu4(4, 4, 0.24);
      LOG(INFO,"Added new odroid-xu4 system%s\n","");
    } else if (strcmp(name, "xu4-fpga") == 0) {
      p_sys = build_xu4_fpga(4, 4, 0.24);
      LOG(INFO,"Added new odroid-xu4 system w FPGA%s\n","");
    } else if (strcmp(name, "zcu102-1slot") == 0) {
      p_sys = build_zcu102_1slot();
      LOG(INFO,"Added new zcu102-1slot system%s\n","");
    } else if (strcmp(name, "zcu102-mat64") == 0) {
      p_sys = build_zcu102_mat64();
      LOG(INFO,"Added new zcu102-mat64 system%s\n","");
    } else if (strcmp(name, "zcu102-mat128") == 0) {
      p_sys = build_zcu102_mat128();
      LOG(INFO,"Added new zcu102-mat128 system%s\n","");
    } else if (strcmp(name, "zcu102-mat64-128") == 0) {
      p_sys = build_zcu102_mat64_mat128();
      LOG(INFO,"Added new zcu102-mat64-128 system%s\n","");
    } else if (strcmp(name, "raspberry-pi4b") == 0) {
      p_sys = build_raspberry_pi4b();
      LOG(INFO,"Added new raspberry-pi4b system%s\n","");
    } else if (strcmp(name, "xavier-agx") == 0) {
      p_sys = build_xavier_agx();
      LOG(INFO,"Added new xavier-agx system\n");
    } else if (strcmp(name, "zcu102-mat64-128-hf") == 0) {
      p_sys = build_zcu102_mat64_mat128_highest_freq();
      LOG(INFO,"Added new zcu102-mat64-128-hf system\n");
    } else {
      fprintf(stderr, "Unknown system %s\n", name);
      exit(1);
    }
}

int sys_get_freq_id(int isl_id, double freq) {
  for (int i = 0; i < p_sys->islands[isl_id].n_freqs; ++i) {
    if (p_sys->islands[isl_id].freqs[i] == freq) {
      return i;
    }
  }

  return -1;
}

int parse_dag(int *argc, char **argv[]) {
  unsigned ret_code;
  (void) ret_code; // this varaible can be used when running in Release mode or undefine NDEBUG. This hack makes sure that wont be a unused variable warning
  if (strcmp(**argv, "-d") == 0) {
    double deadline, period;
    check(*argc > 1, "Missing argument for %s option", **argv);
    (*argc)--;  (*argv)++;
    check(sscanf(**argv, "%lf,%lf", &deadline, &period) == 2, "Wrong argument to -d (needs a comma-separated float pair): %s", **argv);
    sys_new_dag(p_sys, deadline, period);
    LOG(INFO,"Added new DAG with deadline %f, period %f\n", deadline, period);
  } else if (strcmp(**argv, "-t") == 0) {
    double wcet, wcet_ns;
    int e; 
    (void) e; // this variable can be unused depending on the LOG_LEVEL
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    assert(p_sys->n_dags > 0);
    dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
    if (sscanf(**argv, "%lf,%lf", &wcet, &wcet_ns) == 2)
      e = dag_add_elem(p_dag, wcet, wcet_ns, -1);
    else if (sscanf(**argv, "%lf", &wcet) == 1)
      e = dag_add_elem(p_dag, wcet, 0, -1);
    else {
      fprintf(stderr, "Error: wrong format for -t argument (expecting wcet or wcet,wcet_ns)\n");
      exit(1);
    }
    LOG(INFO,"Added elem %d with WCET %f and WCET_ns %f\n", e, wcet, wcet_ns);
  } else if (strcmp(**argv, "-a") == 0) {
    assert(*argc >= 1);
    (*argc)--;  (*argv)++;
    int acc_id;
    double C = C_NONE;
    double C_ns = 0.0;
    if (sscanf(**argv, "%d,%lf,%lf", &acc_id, &C, &C_ns) != 3 && sscanf(**argv, "%d,%lf", &acc_id, &C) != 2 && sscanf(**argv, "%d", &acc_id) != 1) {
      fprintf(stderr, "Error: wrong syntax for -a argument!\n");
      exit(1);
    }
    dag_add_acc(acc_id,C,C_ns);
  } else if (strcmp(**argv, "-w") == 0) {
    assert(*argc >= 1);
    (*argc)--;  (*argv)++;
    int isl_id, freq_id;
    double C = C_NONE;
    if (sscanf(**argv, "%d,%d,%lf", &isl_id, &freq_id, &C) != 3) {
      fprintf(stderr, "Error: wrong syntax for -w argument!\n");
      exit(1);
    }
    dag_set_wcet(isl_id, freq_id, C);
  } else if (strcmp(**argv, "-p") == 0) {
    int e, p;
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    ret_code = sscanf(**argv, "%d,%d", &e, &p);
    assert(ret_code == 2);
    dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
    dag_add_prev(p_dag, e, p);
    LOG(INFO,"Added for elem %d prev %d\n", e, p);
  } else if (strcmp(**argv, "-aaf") == 0) {
    int t1, t2;
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    ret_code = sscanf(**argv, "%d,%d", &t1, &t2);
    assert(ret_code == 2);
    dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
    // the call on t2,t1 shouldn't be needed
    dag_set_aaf(p_dag, t1, t2);
    LOG(INFO,"Elem %d and elem %d in dag %d won't be mapped to the same PU\n", t1, t2, p_sys->n_dags - 1);
  } else if (strcmp(**argv, "-n") == 0) {
    int e, n;
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    ret_code = sscanf(**argv, "%d,%d", &e, &n);
    assert(ret_code == 2);
    dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
    dag_add_next(p_dag, e, n);
    LOG(INFO,"Added for elem %d next %d\n", e, n);
  } else if (strcmp(**argv, "-di") == 0) {
    check(*argc > 1, "Missing argument for %s option", **argv);
    (*argc)--;  (*argv)++;
    double deadline;
    int tbeg, tend;
    check(sscanf(**argv, "%lf,%d,%d", &deadline, &tbeg, &tend) == 3, "Wrong argument to -di: %s", **argv);
    dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
    dag_add_intermediate_dl(p_dag, tbeg, tend, deadline);
    LOG(INFO,"Added new DAG intermediate deadline %lf from %d to %d\n", deadline, tbeg, tend);
  } else if (strcmp(**argv, "-ap") == 0) {
    assert(*argc >= 1);
    (*argc)--;  (*argv)++;
    int acc_id;
    double power = 0.0;
    if (sscanf(**argv, "%d,%lf", &acc_id, &power) != 2) {
      fprintf(stderr, "Error: wrong syntax for -ap argument!\n");
      exit(1);
    }
    dag_add_acc_power(acc_id, power);
  } else if (strcmp(**argv, "-csd") == 0) {
    assert(*argc >= 1);
    (*argc)--;  (*argv)++;
    int acc_id;
    double cs_delay = 0.0;
    if (sscanf(**argv, "%d,%lf", &acc_id, &cs_delay) != 2) {
      fprintf(stderr, "Error: wrong syntax for -csd argument!\n");
      exit(1);
    }
    assert(acc_id < p_sys->n_islands);
    island_t *p_isl = &p_sys->islands[acc_id];
    assert(p_isl->type == ACCELERATOR);
    p_isl->cs_delay = cs_delay;
    LOG(INFO,"Set context-switch delay for island %d to %lg\n", acc_id, cs_delay);
  } else if (strcmp(**argv, "-omp") == 0) {
    assert(*argc >= 1);
    (*argc)--;  (*argv)++;
    int omp_env_id;
    ret_code = sscanf(**argv, "%d", &omp_env_id);
    dag_add_omp_env(omp_env_id);
  } else {
    return 0;
  }
  return 1;
}


int parse_sys(int *argc, char **argv[]) {
  unsigned ret_code;
  (void) ret_code;
  if (strcmp(**argv, "-s") == 0) {
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    platform_name = **argv;
    set_platform(platform_name);
  } else if (strcmp(**argv, "-i") == 0) {
    int n_cores;
    double capacity;
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    ret_code = sscanf(**argv, "%d,%lf", &n_cores, &capacity);
    assert(ret_code == 2);
    assert(p_sys != NULL);
    island_t *p_island = sys_add_island(p_sys, capacity, n_cores, SYS_SCHED_EDF, CPU, 0);
    assert(p_island != NULL);
    LOG(INFO,"Added new island with n_cores %d, capacity %f\n", n_cores, capacity);
  } else if (strcmp(**argv, "-ai") == 0) {
    int n_cores;
    double capacity;
    double context_switch_delay;
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;

    if (!(sscanf(**argv, "%d,%lf,%lf", &n_cores, &capacity, &context_switch_delay) == 3)) {
      context_switch_delay = 0;
      if (!(sscanf(**argv, "%d,%lf", &n_cores, &capacity) == 2)) {
        fprintf(stderr, "Wrong syntax!\n");
        exit(1);
      }
    }

    assert(p_sys != NULL);
    island_t *p_island = sys_add_island(p_sys, capacity, n_cores, SYS_SCHED_EDF, ACCELERATOR, context_switch_delay);
    assert(p_island != NULL);
    LOG(INFO,"Added new accelerated island with n_cores %d, capacity %f\n", n_cores, capacity);
  } else if (strcmp(**argv, "-f") == 0) {
    double freq, pow_busy, pow_idle;
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    ret_code = sscanf(**argv, "%lf,%lf,%lf", &freq, &pow_busy, &pow_idle);
    assert(ret_code == 3);
    island_add_freq(&p_sys->islands[p_sys->n_islands - 1], freq, pow_busy, pow_idle);
    LOG(INFO,"Added freq with freq %f, pow_busy %f, pow_idle %f\n", freq, pow_busy, pow_idle);
  } else if (strcmp(**argv, "-ompenv") == 0) {
    int n_envs;
    assert(*argc >= 1);
    (*argc)--;  (*argv)++;
    ret_code = sscanf(**argv, "%d", &n_envs);
    assert(ret_code == 1);
    sys_add_omp_envs(p_sys, n_envs);
    LOG(INFO,"The system has %d OpenMP environments\n", n_envs);
  } else {
    return 0;
  }
  return 1;
}

unsigned long micros(){
    struct timeval ts;
    gettimeofday(&ts,NULL);
    unsigned long us = 1000000 * ts.tv_sec + ts.tv_usec;
    return us;
}

int file_exists(const char * filename) {
    FILE * f = fopen(filename, "r");
    if (f != NULL) {
        fclose(f);
        return 1;
    }
    return 0;
}

void json_parse_dag_amalthea_name(struct json_object *json_dag, char **name) {
  struct json_object *obj_dag_name;
  const char* json_name;

  json_object_object_get_ex(json_dag, "amalthea_task_id", &obj_dag_name);
  json_name = json_object_get_string(obj_dag_name);

  *name = malloc(strlen(json_name) + 1);
  strcpy(*name, json_name);
}

void json_parse_dag_constraints(struct json_object *jdag, double *period, double *deadline) {
  json_object *tmp = NULL;
  json_object_object_get_ex(jdag, "constraints", &tmp);
  if (tmp != NULL) {
    // parsing dag_deadline and dag_period to create the dag struct
    struct json_object *obj_deadline, *obj_period;
    json_object_object_get_ex(tmp, "deadline", &obj_deadline);
    //printf("dline type = %d\n",json_object_get_type (obj_deadline));
    check(obj_deadline != NULL, "deadline key not found");
    *deadline = json_object_get_double(obj_deadline);
    json_object_object_get_ex(tmp, "period", &obj_period);
    check(obj_period != NULL, "period key not found");
    *period = json_object_get_double(obj_period);
  }
}

double json_parse_freq(struct json_object *json_dag, island_type_enum pu_type) {
  int result;
  struct json_object *metadata_obj;
  struct json_object *pu_obj;
  struct json_object *freq_obj;
  double freq = 0;

  result = json_object_object_get_ex(json_dag, "metadata", &metadata_obj);
  check(result != 0, "Could not read the metadata field\n");

  if (pu_type == CPU) {
    result = json_object_object_get_ex(metadata_obj, "cpu", &pu_obj);
  } else if(pu_type == ACCELERATOR) {
    result = json_object_object_get_ex(metadata_obj, "gpu", &pu_obj);
  }

  check(result != 0, "Could not read the PU field\n");

  result = json_object_object_get_ex(pu_obj, "frequency", &freq_obj);
  if (result != 0) {
    freq = json_object_get_double(freq_obj);
  }

  return freq;
}

void json_read_dag_dependency(struct json_object *json_dag, char *amalthea_dag_name, char **dag_dependency) {
  struct json_object *obj_dag_ins;
  json_object_object_get_ex(json_dag, "ins", &obj_dag_ins);

  int n_ins = json_object_array_length(obj_dag_ins);

  if (n_ins == 0) {
    *dag_dependency = NULL;
    return;
  }

  // TODO: implement it, eventually
  check(n_ins == 1, "More than one precedence found for DAG %s, currently not supported", amalthea_dag_name);

  struct json_object *obj_val = json_object_array_get_idx(obj_dag_ins, n_ins - 1);
  check(json_object_get_string_len(obj_val) > 0, "The (preceding) DAG name %s length is 0", amalthea_dag_name);

  const char *dependency = json_object_get_string(obj_val);

  *dag_dependency = malloc(json_object_get_string_len(obj_val));
  strcpy(*dag_dependency, dependency);
}

double json_read_task_wcet(struct json_object *task, char *key) {
  check(json_object_get_type(task) == json_type_object, "Wrong type%s","");
  struct json_object *obj_metrics;
  json_object_object_get_ex(task, "metrics", &obj_metrics);
  check(obj_metrics != NULL, "Could not retrieve 'metrics' entry from task key '%s'", key);
  struct json_object *obj_wcet;
  json_object_object_get_ex(obj_metrics, "wcet", &obj_wcet);
  check(obj_wcet != NULL, "Could not retrieve 'wcet' entry from task key '%s'", key);

  return json_object_get_double(obj_wcet);
}

double json_read_task_avg_exec_time(struct json_object *task, char *key) {
  check(json_object_get_type(task) == json_type_object, "Wrong type%s","");
  struct json_object *obj_metrics;
  json_object_object_get_ex(task, "metrics", &obj_metrics);
  check(obj_metrics != NULL, "Could not retrieve 'metrics' entry from task key '%s'", key);
  struct json_object *obj_wcet;
  json_object_object_get_ex(obj_metrics, "avg_time", &obj_wcet);
  check(obj_wcet != NULL, "Could not retrieve 'avg_time' entry from task key '%s'", key);

  return json_object_get_double(obj_wcet);
}

double json_read_task_pow_busy(struct json_object *task, char *key) {
  check(json_object_get_type(task) == json_type_object, "Wrong type%s","");
  struct json_object *obj_metrics;
  json_object_object_get_ex(task, "metrics", &obj_metrics);
  check(obj_metrics != NULL, "Could not retrieve 'metrics' entry from task key '%s'", key);
  struct json_object *obj_wcet;
  json_object_object_get_ex(obj_metrics, "power", &obj_wcet);
  check(obj_wcet != NULL, "Could not retrieve 'power' entry from task key '%s'", key);

  return json_object_get_double(obj_wcet) / 1000;
}

int get_dag_file_id_from_amalthea_name(const char *name) {
  for (int j = 0; j < p_sys->n_amalthea_dags; ++j) {
    if (strcmp(name, p_sys->amalthea_dag_names[j]) == 0 && p_sys->amalthea_to_dag_id[j] != -1)
      return j;
  }

  return -1;
}

int get_dag_id_from_amalthea_name(const char *name) {
  if (name == NULL) {
    return -1;
  }

  int dag_file_id = get_dag_file_id_from_amalthea_name(name);
  if (dag_file_id == -1) {
    return -1;
  }

  return p_sys->amalthea_to_dag_id[dag_file_id];
}

int json_parse_task_replicas(struct json_object *json_task, int **replicas) {
  struct json_object *obj_replicas;
  int found = json_object_object_get_ex(json_task, "replicas", &obj_replicas);

  if (!found) {
    *replicas = NULL;
    return 0;
  }

  int n_replicas = json_object_array_length(obj_replicas);
  *replicas = malloc(sizeof(int) * n_replicas);

  for (int i = 0; i < n_replicas; i++) {
    struct json_object *obj_val = json_object_array_get_idx(obj_replicas, i);
    check(obj_val != NULL, "Could not retrieve replica");

    int r = json_object_get_int(obj_val);
    (*replicas)[i] = r;
  }

  return n_replicas;
}

int json_load_dag(struct json_object *json_dag, int amalthea_dag_id, double *p_period, char *amalthea_dag_name) {
  dag_t *p_dag = NULL;
  double deadline;
  int dag_elem_offset = 0;
  int dag_id = -1;
  int first_elem_id = -1;
  int last_elem_id = -1;

  *p_period = deadline = -1;
  json_parse_dag_constraints(json_dag, p_period, &deadline);

  if (p_sys->amalthea_dag_names[amalthea_dag_id] == NULL) {
    p_sys->amalthea_dag_names[amalthea_dag_id] = malloc(strlen(amalthea_dag_name) + 1);
    strcpy(p_sys->amalthea_dag_names[amalthea_dag_id], amalthea_dag_name);
    p_sys->n_amalthea_dags++;
  }

  LOG(DEBUG, "Creating DAG %s\n", amalthea_dag_name);

  double cpu_freq = json_parse_freq(json_dag, CPU) / 1000;
  double gpu_freq = json_parse_freq(json_dag, ACCELERATOR) / 1000;
  LOG(DEBUG, "CPU running at frequency %f\n", cpu_freq);
  LOG(DEBUG, "GPU running at frequency %f\n", gpu_freq);

  // sequential DAG --> finding its preceding DAG
  if (*p_period == -1) {
    char* dependency;
    json_read_dag_dependency(json_dag, amalthea_dag_name, &dependency);
    dag_id = get_dag_id_from_amalthea_name(dependency);

    if (dag_id == -1) {
      LOG(DEBUG, "Could not determine DAG %s as dependency for DAG %s, postponed\n", dependency, amalthea_dag_name);
      return 0;
    }

    // adding the new elements to the preceding DAG
    p_dag = p_sys->dags[dag_id];
    dag_elem_offset = p_dag->num_elems;
    last_elem_id = p_dag->last;
    LOG(DEBUG, "DAG %s is activated after %s, whose application id is %d\n", amalthea_dag_name, dependency, dag_id);

    free(dependency);
  } else {
    LOG(DEBUG, "Adding DAG %s with period=%lf, deadline=%lf\n", amalthea_dag_name, *p_period, deadline);
    p_dag = sys_new_dag(p_sys, deadline, *p_period);
    dag_set_name(p_dag, amalthea_dag_name);
    dag_id = p_sys->n_dags - 1;

    // checking whether this is the last DAG of the sequential computation
    struct json_object *obj_dag_ins;
    json_object_object_get_ex(json_dag, "outs", &obj_dag_ins);

    if (json_object_array_length(obj_dag_ins) == 0) {
      p_sys->last_dag = p_dag;
    }
  }

  p_sys->amalthea_to_dag_id[amalthea_dag_id] = dag_id;
  p_sys->amalthea_to_dag_task_id_offset[amalthea_dag_id] = dag_elem_offset;

  struct json_object *obj_nodes;
  json_object_object_get_ex(json_dag, "nodes", &obj_nodes);
  int n_nodes=(int)json_object_object_length(obj_nodes);
  LOG(DEBUG, "DAG %s (id %d) has %d tasks\n", amalthea_dag_name, dag_id, n_nodes);

  for (int i = 0; i < n_nodes; ++i) {
    dag_add_elem(p_dag, C_NONE, 0.0, -1);
  }

  // iterate through the tasks just to create the dag elements. no depedency is added
  json_object_object_foreach(obj_nodes, key, task) {
    int n;
    if (sscanf(key, "%d", &n) == 1) {
      double C = json_read_task_wcet(task, key);
      double C_avg = json_read_task_avg_exec_time(task, key);
      double pow_busy = json_read_task_pow_busy(task, key);

      //dag_add_elem(p_dag, C_NONE, Cns, -1);
//      dag_add_omp_env_task_id(amalthea_dag_id, dag_id, n + dag_elem_offset);

//      if (!strcmp(amalthea_dag_name, "classification_tsr")) {
//        dag_add_omp_env_task_id(0, dag_id, n + dag_elem_offset);
//      }

      //checking whether the task is running on the GPU or on the CPU
      struct json_object *obj_spec;
      int spec_present = json_object_object_get_ex(task, "spec", &obj_spec);
      int accelerated = 0;

      if (spec_present) {
        // this task has an implementation for the GPU,
        // eventually its WCETs will be imported

        // FIXME: hp: the first file to be loaded is that with CPU-only
        // data and at the highest frequency
//        dag_add_acc_task_id(1, dag_id, n + dag_elem_offset, 0.0, 0);
//        dag_add_acc_task_id(1, dag_id, n + dag_elem_offset, C / GPU_TIMES_SCALE, 0);
        dag_add_acc_task_id(1, dag_id, n + dag_elem_offset, 0.0, 0);
//        dag_add_acc(1, C / GPU_TIMES_SCALE, 0);
//        dag_add_acc(1, 0.0, 0);
        LOG(DEBUG, "Task %d in DAG %d may run on the GPU\n", n + dag_elem_offset, dag_id);

        const char *spec_val = json_object_get_string(obj_spec);
        if (strcmp(spec_val, "GPU") == 0) {
          accelerated = 1;
        }
      }

      int s = 0;  // assuming 0 is the CPU island
      double freq = cpu_freq;

      if (accelerated) {
        s = 1;    // assuming 1 is the GPU island
        freq = gpu_freq;
      }

      int freq_id = sys_get_freq_id(s, freq);
      dag_set_wcet_task_id(s, freq_id, dag_id, n + dag_elem_offset, C);
      dag_set_avg_exec_time_task_id(s, freq_id, dag_id, n + dag_elem_offset, C_avg);
      dag_set_pow_diff_task_id(s, freq_id, dag_id, n + dag_elem_offset, pow_busy);

      int *replicas;
      int n_replicas = json_parse_task_replicas(task, &replicas);

      // if the task has not been created yet it is not added
      for (int r = 0; r < n_replicas; ++r) {
        assert(replicas[r] + dag_elem_offset < p_dag->num_elems
               && n + dag_elem_offset < p_dag->num_elems);
        dag_set_aaf(p_dag, replicas[r] + dag_elem_offset, n + dag_elem_offset);
        LOG(DEBUG, "Task %d is a replica of task %d\n", n + dag_elem_offset, replicas[r] + dag_elem_offset);
      }

      if (n_replicas > 0) {
        free(replicas);
      }

      LOG(DEBUG, "Adding task %d in DAG %d with C=%lf avg_C=%lf pow_diff=%lf at frequency %f (index %d) on island %d, \n", n + dag_elem_offset, dag_id, C, C_avg, pow_busy, cpu_freq, freq_id, s);
    } else {
      LOG(DEBUG, "key = %s expects an integer string. received value = %s\n", key, json_object_get_string(task));
    }
  }

  // iterate though the tasks again, now to add the dag precedence
  long first_tasks = 0;    // saving the IDs of the tasks that do not have precedence constraints
  long last_tasks = 0;    // saving the IDs of the tasks that do not have nodes connected to its output
  int unique_first_task = 1;
  int n_last_tasks = 0;
  int n_precedences = 0;
  json_object_object_foreach(obj_nodes, key2, task2) {
    int n;
    if (sscanf(key2, "%d", &n) == 1) {
      n += dag_elem_offset;

      struct json_object *obj_ins;
      json_object_object_get_ex(task2, "ins", &obj_ins);
      // add the precedence
      int n_ins = json_object_array_length(obj_ins);

      // saving the first element of the DAG
      if (n_ins == 0) {
        first_tasks |= (1 << n);

        if (first_elem_id != -1) {
          unique_first_task = 0;
        }

        //check(first_elem_id == -1, "MORE THAN ONE STARTING TASK FOR DAG %s", amalthea_dag_name);
        first_elem_id = n;
      }

      for (int i = 0; i < n_ins; i++) {
        struct json_object *obj_val = json_object_array_get_idx(obj_ins, i);
        check(obj_val != NULL, "Could not retrieve wcet entry from json obj%s","");

        int p = json_object_get_int(obj_val) + dag_elem_offset;
        LOG(DEBUG, "Adding precedence %d -> %d\n", p, n);
        dag_add_prev(p_dag, n, p);

        n_precedences++;
      }

      struct json_object *obj_outs;
      json_object_object_get_ex(task2, "outs", &obj_outs);
      int n_outs = json_object_array_length(obj_outs);
      if (n_outs == 0) {
        n_last_tasks++;
        last_tasks |= (1 << n);
      }

    } else {
      LOG(DEBUG, "key = %s expects an integer string. received value = %s\n", key, json_object_get_string(task));
    }
  }

  if (n_precedences == 0 && n_nodes > 1) {
    // creating a sequential DAG
    for (int i = 1; i < p_dag->num_elems; ++i) {
      dag_add_prev(p_dag, i, i - 1);
    }

    first_elem_id = 0;
    unique_first_task = 1;
    n_last_tasks = 1;
  }

  if (!unique_first_task) {
    // creating a new fictitious node
    LOG(DEBUG, "More than one first task for DAG %s, creating a fictitious node\n", amalthea_dag_name);
    int fake_task_id = p_dag->num_elems;

    dag_add_elem(p_dag, 0, 0, -1);
//    dag_add_omp_env_task_id(amalthea_dag_id, dag_id, fake_task_id);

    p_dag->elems[fake_task_id].fictitious = 1;

    for (int n = 0; n < p_dag->num_elems; ++n) {
      if (first_tasks & (1 << n)) {
        LOG(DEBUG, "Adding precedence %d -> %d\n", fake_task_id, n);
        dag_add_prev(p_dag, n, fake_task_id);
      }
    }

    first_elem_id = fake_task_id;
  }

  if (n_last_tasks > 1) {
      // creating a new fictitious node
      LOG(DEBUG, "More than one last task for DAG %s, creating a fictitious node\n", amalthea_dag_name);
      int fake_task_id = p_dag->num_elems;

      dag_add_elem(p_dag, 0, 0, -1);
//      dag_add_omp_env_task_id(amalthea_dag_id, dag_id, fake_task_id);

      p_dag->elems[fake_task_id].fictitious = 1;

      for (int n = 0; n < p_dag->num_elems; ++n) {
        if (last_tasks & (1 << n)) {
          LOG(DEBUG, "Adding precedence %d -> %d\n", n, fake_task_id);
          dag_add_prev(p_dag, fake_task_id, n);
        }
      }

      last_elem_id = fake_task_id;
    }

  // in case of a sequential DAG, connecting its first node with the last
  // of its preceding DAG
  if (*p_period == -1) {
    dag_add_prev(p_dag, first_elem_id, last_elem_id);
    LOG(DEBUG, "Adding merging precedence %d -> %d\n", last_elem_id, first_elem_id);
  }

  // saving intermediate deadline
  if (deadline != -1) {
    dag_add_intermediate_dl(p_dag, first_elem_id, p_dag->num_elems - 1, deadline);
    LOG(DEBUG, "Adding intermediate deadline %g between %d and %d in DAG %s (ID %d)\n", deadline, first_elem_id, p_dag->num_elems - 1, amalthea_dag_name, dag_id);
  }

  dag_update_first_last(p_dag);

  p_dag->deadline = *p_period;

  return 1;
}

void json_add_wcets(struct json_object *json_dag, int amalthea_dag_file_id) {
  char* amalthea_dag_name = p_sys->amalthea_dag_names[amalthea_dag_file_id];
  LOG(DEBUG, "Processing again DAG %s\n", amalthea_dag_name);

  // obtaining the actual DAG id and the offset
  int dag_id = p_sys->amalthea_to_dag_id[amalthea_dag_file_id];
  int dag_task_offset = p_sys->amalthea_to_dag_task_id_offset[amalthea_dag_file_id];

  check(dag_id != -1, "Could not find DAG id for DAG %d\n", amalthea_dag_file_id);
  check(dag_task_offset != -1, "Could not find DAG task offset for DAG %d\n", amalthea_dag_file_id);

  struct json_object *obj_nodes;
  json_object_object_get_ex(json_dag, "nodes", &obj_nodes);

  double cpu_freq = json_parse_freq(json_dag, CPU) / 1000;
  double gpu_freq = json_parse_freq(json_dag, ACCELERATOR) / 1000;
  LOG(DEBUG, "CPU running at frequency %f\n", cpu_freq);
  LOG(DEBUG, "GPU running at frequency %f\n", gpu_freq);

  // iterate through the tasks just to create the dag elements. no depedency is added
  json_object_object_foreach(obj_nodes, key, task) {
    int n;
    if (sscanf(key, "%d", &n) == 1) {
      double C = json_read_task_wcet(task, key);
      double C_avg = json_read_task_avg_exec_time(task, key);
      double pow_diff = json_read_task_pow_busy(task, key);

      //checking whether the task is running on the GPU or on the CPU
      struct json_object *obj_spec;
      int spec_present = json_object_object_get_ex(task, "spec", &obj_spec);
      int accelerated = 0;

      if (spec_present) {
        const char *spec_val = json_object_get_string(obj_spec);
        if (strcmp(spec_val, "GPU") == 0) {
          accelerated = 1;
        }
      }

      int s = 0;      // assuming 0 is the CPU island
      double freq = cpu_freq;

      if (accelerated) {
        s = 1;    // assuming 1 is the GPU island
        freq = gpu_freq;
      }

      int freq_id = sys_get_freq_id(s, freq);

      dag_set_wcet_task_id(s, freq_id, dag_id, dag_task_offset + n, C);
      dag_set_avg_exec_time_task_id(s, freq_id, dag_id, dag_task_offset + n, C_avg);

      if (!accelerated) {

        dag_set_pow_diff_task_id(s, freq_id, dag_id, dag_task_offset + n, pow_diff);
      }

      LOG(DEBUG, "Updating task %d in DAG %d with C=%lf avg_C=%lf pow_diff=%lf at frequency %f (index %d) on island %d, \n", dag_task_offset + n, dag_id, C, C_avg, pow_diff, freq, freq_id, s);
    } else {
      LOG(DEBUG, "key = %s expects an integer string. received value = %s\n", key, json_object_get_string(task));
    }
  }
}

void json_set_dag_dependencies(struct json_object *json_dag, int amalthea_dag_file_id, char *amalthea_dag_name, double *period) {
  dag_t *p_dag = NULL;

  if (*period == -1) {
    // sequential DAG --> skipping it, as it's merged with another periodic one
    return;
  } else {
    int dag_id = p_sys->amalthea_to_dag_id[amalthea_dag_file_id];
    p_dag = p_sys->dags[dag_id];

    // finding the DAG from which the current one takes its input
    char* dependency;
    json_read_dag_dependency(json_dag, amalthea_dag_name, &dependency);
    int dep_dag_id = get_dag_id_from_amalthea_name(dependency);

    if (dep_dag_id != -1) {
      p_dag->previous = p_sys->dags[dep_dag_id];
      free(dependency);
    }
  }
}

void json_set_sequential_dag_deadlines(double end_to_end_dl) {
  // in case of sequential DAGs, setting their deadlines appropriately,
  // considering the end-to-end deadline of the whole chain
  double first_dag_dl = end_to_end_dl;
  dag_t *current = p_sys->last_dag;

  while (current != NULL) {
    if (current->previous == NULL) {
      // first DAG of the chain -> its deadline is
      // computed as the difference of all the (known) others
      current->deadline = first_dag_dl;
      check(first_dag_dl > 0, "Invalid deadline %f for the first DAG %s", current->deadline, current->name);
      LOG(DEBUG, "Setting deadline %f to the first DAG %s\n", current->deadline, current->name);
    } else {
      // intermediate periodic DAG -> the deadline is double the period
//      current->deadline = current->period * 2;
//      first_dag_dl -= current->deadline;

      current->deadline = current->period;
      first_dag_dl -= (current->deadline * 2);

      LOG(DEBUG, "Setting deadline %f to periodic DAG %s with period %f\n", current->deadline, current->name, current->period);
    }

    current = current->previous;
  }
}

void load_json(char *fname) {
  char *json;
  int fd;
  struct json_object *obj;
  struct json_object *main_app = NULL;
  struct stat st;
  double end_to_end_dl = 0;   // us probably
  double dag_periods[MAX_DAGS];

  LOG(DEBUG,"Processing file%s\n", fname);

  check(stat(fname, &st) == 0, "error on stat()");
  check((fd = open(fname, O_RDONLY)) >= 0, "error on open()");
  json = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
  close(fd);

  obj = json_tokener_parse(json);

  // reading the end-to-end deadline of the application
  struct json_object *obj_deadline;
  json_object_object_get_ex(obj, "end_to_end_deadline", &obj_deadline);
  end_to_end_dl = json_object_get_double(obj_deadline) + 31000000;

  check(end_to_end_dl > 0, "Invalid deadline");
  LOG(DEBUG, "E2E deadline: %f\n", end_to_end_dl);

  // Get the json_object associated to the main application array (1 or more DAGs)
  json_object_object_foreach(obj, key, val) {
    if (json_object_get_type(val) == json_type_array) {
      key = key;                // silence compiler warning
      main_app = val;
      break;
    }
  }
  check(main_app != NULL, "Missing main application array in root json map");

  LOG(DEBUG, "Number of DAGs=%d\n", (int)json_object_array_length(main_app));

  // creating a dedicated OpenMP environment per DAG
//  if (p_sys->n_dags <= 0) {
//    sys_add_omp_envs(p_sys, (int)json_object_array_length(main_app));
//    LOG(DEBUG, "There are %d OpenMP environments\n", p_sys->n_omp_envs);
//  }

//  if (p_sys->n_dags <= 0) {
//    sys_add_omp_envs(p_sys, 1);
//    LOG(DEBUG, "There are %d OpenMP environments\n", p_sys->n_omp_envs);
//  }

  // build the target platform data structure
  struct json_object *obj_platform;
  json_object_object_get_ex(obj, "target_platform", &obj_platform);
  if (obj_platform != NULL) {
    const char *hw_platform_name = json_object_get_string(obj_platform);
    LOG(DEBUG, "Platform=%s selected\n", hw_platform_name);
    platform_name = (char *) hw_platform_name;
    set_platform(hw_platform_name);
  }

  // iterate over the DAGs of the application
  int all_dags_parsed = 1;
  for (int i = 0; i < json_object_array_length(main_app); i++) {
    // Set in tmp the json_object of the secu_code array at index i
    struct json_object *json_dag;
    char *dag_amalthea_name;

    json_dag = json_object_array_get_idx(main_app, i);

    json_parse_dag_amalthea_name(json_dag, &dag_amalthea_name);
    int dag_file_id = get_dag_file_id_from_amalthea_name(dag_amalthea_name);
    if (dag_file_id == -1) {
      // the DAG does not exist, creating it
      int ret = json_load_dag(json_dag, i, &dag_periods[i], dag_amalthea_name);
      if (!ret) {
        all_dags_parsed = 0;
      }
    } else {
      // the DAG already exists, adding the execution time of
      // its tasks with the new frequency value
      json_add_wcets(json_dag, dag_file_id);
    }

    free(dag_amalthea_name);
  }

  // keep spinning till all the dags are parsed (resolving dependency problems)
  int n_new_parsed = 1;
  while (!all_dags_parsed && n_new_parsed > 0) {
    n_new_parsed = 0;
    all_dags_parsed = 1;

    for (int i = 0; i < json_object_array_length(main_app); i++) {
      struct json_object *json_dag;
      char *dag_amalthea_name;

      json_dag = json_object_array_get_idx(main_app, i);

      json_parse_dag_amalthea_name(json_dag, &dag_amalthea_name);
      int dag_file_id = get_dag_file_id_from_amalthea_name(dag_amalthea_name);
      if (dag_file_id == -1) {
        // the DAG does not exist, creating it
        LOG(DEBUG, "Parsing again DAG %s\n", dag_amalthea_name);
        int ret = json_load_dag(json_dag, i, &dag_periods[i], dag_amalthea_name);
        if (!ret) {
          all_dags_parsed = 0;
        } else {
          n_new_parsed++;
        }
      }

      free(dag_amalthea_name);
    }
  }

  // setting the precedence relationships between DAGs
  for (int i = 0; i < json_object_array_length(main_app); i++) {
    struct json_object *json_dag;
    char *dag_amalthea_name;

    json_dag = json_object_array_get_idx(main_app, i);

    json_parse_dag_amalthea_name(json_dag, &dag_amalthea_name);
    int dag_file_id = get_dag_file_id_from_amalthea_name(dag_amalthea_name);
    json_set_dag_dependencies(json_dag, get_dag_file_id_from_amalthea_name(dag_amalthea_name), dag_amalthea_name, &dag_periods[dag_file_id]);

    free(dag_amalthea_name);
  }

  // computing the deadlines of the application
//  json_set_sequential_dag_deadlines(end_to_end_dl);

  LOG(DEBUG, "File %s loaded\n",fname);
}

void json_write_results(sys_t *p_sys, char *fname, char * out_fname){
  struct json_object *obj;
  struct json_object *main_app = NULL;
  struct stat st;
  int fd;
  char *json;

  assert(stat(fname, &st) == 0);
  assert((fd = open(fname, O_RDONLY)) >= 0);
  json = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
  close(fd);

  obj = json_tokener_parse(json);
  // Get the json_object associated to the main application array (1 or more DAGs)
  json_object_object_foreach(obj, key, val) {
    if (json_object_get_type(val) == json_type_array) {
      key = key;                // silence compiler warning
      main_app = val;
      break;
    }
  }
  check(main_app != NULL, "Missing main application array in root json map");

  LOG(DEBUG, "Number of DAGs=%d\n", (int)json_object_array_length(main_app));

  // iterate in case of multi-dag definition
  for (int dag_file_id = 0; dag_file_id < json_object_array_length(main_app); dag_file_id++) {
    // Set in tmp the json_object of the secu_code array at index i
    struct json_object *json_dag;
    char *dag_amalthea_name;
    json_dag = json_object_array_get_idx(main_app, dag_file_id);

    json_parse_dag_amalthea_name(json_dag, &dag_amalthea_name);

    int dag_id = p_sys->amalthea_to_dag_id[dag_file_id];
    int dag_task_offset = p_sys->amalthea_to_dag_task_id_offset[dag_file_id];

    check(dag_id != -1, "Could not find DAG id for DAG %d\n", dag_file_id);
    check(dag_task_offset != -1, "Could not find DAG task offset for DAG %d\n", dag_file_id);

    struct json_object *obj_nodes;
    json_object_object_get_ex(json_dag, "nodes", &obj_nodes);
    // iterate through the tasks just to create the dag elements
    json_object_object_foreach(obj_nodes, key, task) {
      int n;
      if (sscanf(key, "%d", &n) == 1) {
        n += dag_task_offset;
        dag_t *p_dag = p_sys->dags[dag_id];
        dag_elem_t *p_elem = &p_dag->elems[n];
        island_t *p_isl = &p_sys->islands[p_elem->opt_island];

        int static_thread = p_elem->opt_punit;
        char* spec = "ARM";

        if (p_isl->type == ACCELERATOR) {
          static_thread = -1;
          spec = "GPU";
        }

        struct json_object *obj_static_thread, *obj_spec;

        obj_static_thread = json_object_new_int(static_thread);
        json_object_object_add(task, "static_thread", obj_static_thread);

        obj_spec = json_object_new_string(spec);
        json_object_object_add(task, "spec", obj_spec);

        LOG(DEBUG, "Adding task [%d,%d] placement static_thread=%d, island=%s\n", dag_file_id,n, static_thread, spec);
      } else {
        LOG(DEBUG, "key = %s expects an integer string. received value = %s\n", key, json_object_get_string(task));
      }
    }

    // setting the frequencies in the hardware metadata
    int result;
    struct json_object *metadata_obj;
    struct json_object *pu_obj;
    struct json_object *freq_obj;

    result = json_object_object_get_ex(json_dag, "metadata", &metadata_obj);
    check(result != 0, "Could not read the metadata field\n");

    for (int s = 0; s < p_sys->n_islands; ++s) {
      island_t *p_isl = &p_sys->islands[s];
      char* pu_type;

      if (p_isl->type == ACCELERATOR) {
        pu_type = "gpu";
      } else {
        pu_type = "cpu";
      }

      result = json_object_object_get_ex(metadata_obj, pu_type, &pu_obj);
      check(result != 0, "Could not read the %s field\n", pu_type);

      freq_obj = json_object_new_double(p_isl->freqs[p_isl->opt_freq] * 1000);
      json_object_object_add(pu_obj, "frequency", freq_obj);
    }

    if (p_sys->n_omp_envs < 1) {
      continue;
    }

    // setting the scheduling parameters for the OpenMP worker threads
    struct json_object *sched_params_obj = json_object_new_array();

    omp_env_t *env = &p_sys->omp_envs[dag_file_id];

    for (int s = 0; s < env->n_islands; ++s) {
      for (int p = 0; p < env->n_punits[s]; ++p) {
        omp_wt_t *wt = &env->worker_threads[s][p];

        struct json_object *wt_json_obj = json_object_new_object();

        struct json_object *wt_period_obj = json_object_new_double(wt->opt_period);
        json_object_object_add(wt_json_obj, "period", wt_period_obj);

        struct json_object *wt_deadline_obj = json_object_new_double(wt->opt_period);
        json_object_object_add(wt_json_obj, "deadline", wt_deadline_obj);

        struct json_object *wt_runtime_obj = json_object_new_double(wt->opt_budget);
        json_object_object_add(wt_json_obj, "runtime", wt_runtime_obj);

        json_object_array_add(sched_params_obj, wt_json_obj);
      }
    }

    json_object_object_add(json_dag, "scheduling_parameters", sched_params_obj);
  }

  // save the json file
  FILE *f = fopen(out_fname, "w");
  assert(f != NULL);
  fprintf(f,"\n%s\n\n", json_object_to_json_string_ext(obj,JSON_C_TO_STRING_PRETTY));
  fclose(f);
  json_object_put(obj);
}

char temp_out_sols_fname[100];
char temp_out_sols_fname2[128];
char temp_out_sols_fname_lastone[128];

int main(int argc, char *argv[]) {
  int call_solver_gurobi = 0;
  int call_solver_heur = 0;
  int parse_gurobisol = 0;
  int use_deadline_splitting = 0;
  int use_multi_objective = 0;
  int constrained_omp_servers = 0;
  double power_budget = 0.0;
  unsigned ret_code;
  (void) ret_code;

  /* new_comp_unrelated = 1; */
  /* dag_t *p_dag = build_dag1(); */
  /* dag_add_next(p_dag, 1, 4); */
  /* printf("\n"); */
  /* dag_comp_unrelated(p_dag); */
  /* dag_dump(p_dag); */
  /* printf("Unrelated new2: "); */
  /* dag_dump_unrelated(p_dag); */
  /* exit(0); */

  // this is set to make sure that the gurobi pid is killed when this process is killed.
  // this is a requiment to deal with timeout termination of dag
  printf("setsid() returned: %d\n", setsid());

  p_sys = malloc(sizeof(sys_t));
  check(p_sys != NULL, "Could not allocate memory%s","");
  sys_init(p_sys);

  // print the executed command
  printf("$> ");
  for (int i=0;i<argc;i++){
      printf("%s ", argv[i]);
  }
  printf("\n");

  argc--;  argv++;
  while (argc > 0) {
    if (strcmp(*argv, "-h") == 0 || strcmp(*argv, "--help") == 0) {
      printf("Usage: dag [options...]\n");
      printf("  Options:\n");
      printf("    -h|--help .................. Show this help message and exit\n");
      printf("    -s system .................. Use a named hardware platform (odroid-xu3[-half], odroid-xu4, xu4-fpga, zcu102-1slot, zcu102-mat64[-128], zcu102-mat64-128-hf, raspberry-pi4b, xavier-agx)\n");
      printf("    -i n_cores[,capacity] ...... Add an island with the specified number of cores and capacity (defaults to 1.0)\n");
      printf("    -ai n_cores,cap[,cs_delay] . Add an accelerated island with the specified number of cores, capacity and context switch delay (tasks need to be explicitly mapped using -a)\n");
      printf("    -f freq,pow_busy,pow_idle .. Add an OPP to the current island (added last with -i), with the specified frequency and per-island power values (busy power refers to one CPU busy, including the island idle power)\n");
      printf("    -d deadline,period ......... Add an empty DAG with the specified end-to-end deadline and minimum inter-arrival period\n");
      printf("    -t WCET[,WCET_ns] .......... Add a task to the current DAG (added last with -d), with the specified WCET and non-scalable WCET (defaults to 0)\n");
      printf("    -w island_id,freq_id,WCET .. Set WCET a specific island and frequency (specified by 0-based indexes) for the current task (added last with -t)\n");
      printf("    -a island_id[,C[,Cns]] ..... Let the current task (added last with -t) run also on any accelerator of the island identified by the provided ID, optionally with the specified C and Cns (can be used multiple times)\n");
      printf("    -ap acc_island_id,d_power .. Set an additional power consumption term due to the current task (added last with -t) when mapped on any accelerator of the island identified by the provided ID (can be used multiple times)\n");
      printf("    -csd island_id,cs_delay .... Customize context-switch delay for the specified accelerator island (must have been added with -a and/or -s)\n");
      printf("    -p elem,prev ............... Add a precedence constraint from task prev to task elem (both must have already been added with -t)\n");
      printf("    -n elem,next ............... Add a precedence constraint from task elem to task next (both must have already been added with -t)\n");
      printf("    -aaf task_id_1,task_id_2 ... Specify that task_id_1 and task_id_2 of the current DAG (added last with -d) cannot be mapped to the same PU (anti-affinity constraint)\n");
      printf("    -di deadline,t_beg,t_end ... Add end-to-end intermediate deadline between activation of task t_beg and completion of task t_end in current DAG\n");
      printf("    -olp filename.lp ........... Specify destination filename for the LP optimization problem\n");
      printf("    -lps|--lp-scale scale ...... Specify scaling factor for LP times (defaults to auto)\n");
      printf("    -u|--max-util bw ........... Specify the maximum per-core utilization (defaults to 0.95)\n");
      printf("    -r k,D,n,C,e[,seed] ........ Add k random DAGs to the model, with deadline up to D, up to n nodes, up to a WCET of C, up to e edges\n");
      printf("    --solve[=method] ........... Solve the generated LP problem using method, either gurobi (default), dl-split, or heur\n");
      printf("    --parse-gurobi-sol ......... Parse back the Gurobi sol file specified with -osol, instead of calling the Gurobi solver\n");
      printf("    --create-bounds ............ Create explicit bounds in the LP formulation for the values non-binary variables can take (GUROBI ONLY)\n");
      printf("    --use-dl-splitting ......... Call the Gurobi solver with fixed deadlines assigned with deadline splitting\n");
      printf("    --avg-pow .................. Call the Gurobi solver using the average power consumption instead of the worst case (average execution times are 95%% of WCETs)\n");
      printf("    --min-deadline ............. Set minimum possible deadline in LP formulation (defaults to 0.0)\n");
      printf("    --multi-objective .......... Run Gurobi to optimize for both minimum power and maximum slack (power optimization has higher priority)\n");
      printf("    -pb power_budget ........... Set a power budget so that the LP formulation maximizes the minimum slack within the power constraint\n");
      printf("    -osol filename.sol ......... Specify destination filename for the solution of the LP problem (requires --solve)\n");
      printf("    -osols pathname ............ Specify pathname/prefix for intermediate solutions (requires --solve)\n");
      printf("    -odot filename.dot ......... Specify destination filename for the DOT file containing the finally optimized system (requires --solve)\n");
      printf("    --dot-hide-aaf ............. Hide anti-affine nodes in .dot output (useful if replicas of same nodes)\n");
      printf("    -oyml filename.yml ......... Dump current DAGs into the specified YAML file\n");
      printf("    -osh filename.sh ........... Dump current system (islands and DAGs) into the specified SH file\n");
      printf("    -oyml_rtsim_dags fname.yml . Dump placed DAGs into the specified YAML file for RTSim\n");
      printf("    -oyml_rtsim_isl fname.yml .. Dump optimized islands into the specified YAML file for RTSim\n");
      printf("    -oyaml filename.yaml ....... Specify destination filename for the optimization output in YAML format\n");
      printf("    -ojson filename.json ....... Specify destination filename for the optimization output in JSON format\n");
      printf("    -olog filename.log ......... Specify destination filename for the gurobi log\n");
      printf("    -max-threads ............... Set Gurobi to use the max number of threads. Otherwise it uses the number of threads defined in MAX_GUROBI_THREADS\n");
      printf("    -json filename.json ........ Load DAGs from specified JSON file\n");
      printf("    -ompenv n_omp_envs ......... Define the number of OpenMP environments in the system\n");
      printf("    -omp omp_env_id ............ Specify that the current task (added last with -t) belongs to the OpenMP environment identified with omp_env_id [0,n_omp_envs)\n");
      printf("    --omp-budg-constr  ......... Constrain the budget of each OpenMP thread to be the maximum traversing time of the tasks served by it\n");
      printf("    --simplify num ............. Apply num simplify() steps to DAG before computing unrelated sets\n");
      printf("    --load-unrelated fname ..... Load unrelated sets from file\n");
      printf("    --new-comp-unrelated ....... Use experimental comp_unrelated_new()\n");
      exit(0);
    } else if (strcmp(*argv, "-olp") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_lp_fname = *argv;
    } else if (strcmp(*argv, "-osol") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_sol_fname = *argv;
    } else if (strcmp(*argv, "-osols") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_sols_pname = *argv;
    } else if (strcmp(*argv, "-odot") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_dot_fname = *argv;
    } else if (strcmp(*argv, "-oyml") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_yml_fname = *argv;
    } else if (strcmp(*argv, "-oyml_rtsim_dags") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_yml_rtsim_dags_fname = *argv;
    } else if (strcmp(*argv, "-oyml_rtsim_isl") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_yml_rtsim_isl_fname = *argv;
    } else if (strcmp(*argv, "-osh") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_sh_fname = *argv;
    } else if (strcmp(*argv, "-oyaml") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_yaml_fname = *argv;
    } else if (strcmp(*argv, "-ojson") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_json_fname = *argv;
    } else if (strcmp(*argv, "-olog") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      gurobi_log_fname = *argv;
    } else if (strcmp(*argv, "-max-threads") == 0) {
      gurobi_max_threads = 0;
    } else if (strcmp(*argv, "-lps") == 0 || strcmp(*argv, "--lp-scale") == 0) {
      check(argc > 1, "Missing argument for %s option", *argv);
      argc--;  argv++;
      check(sscanf(*argv, "%lf", &lp_scale) == 1, "wrong argument to --lp-scale (needs a float): %s", *argv);
    } else if (strcmp(*argv, "-u") == 0 || strcmp(*argv, "--max-util") == 0) {
      check(argc > 1, "Missing argument for %s option", *argv);
      argc--;  argv++;
      check(sscanf(*argv, "%lf", &max_util) == 1, "Wrong argument to --max-util (needs a float): %s", *argv);
    } else if (strcmp(*argv, "-r") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      int seed;
      int n_dags, max_period, max_elems, wcet_max, max_edges;
      if (sscanf(*argv,"%d,%d,%d,%d,%d,%d", &n_dags, &max_period, &max_elems, &wcet_max, &max_edges, &seed) == 6)
        srand(seed);
      else if (sscanf(*argv,"%d,%d,%d,%d,%d", &n_dags, &max_period, &max_elems, &wcet_max, &max_edges) == 5)
        srand(time(NULL));
      else {
        fprintf(stderr, "Error: wrong format for -r argument (expecting 'n_dags,max_period,max_elems,wcet_max,max_edges,seed' or 'n_dags,max_period,max_elems,wcet_max,max_edges')\n");
        exit(1);
      }
      /* the adopted approach is to create a set of individual dot files (each dot file represents a single dag) and latter combine 
      1 or more dags into a 'dag set', both in yaml and .sh formats.
      This approach works best when considering both random dag generators because they will have a uniform
      approach to generate the dags.
      */
      assert(max_elems >= 3 && max_elems <= MAX_TASKS_PER_DAG);
      char fname[20] = "rnd00.dot";
      int i=0;
      while(i < n_dags){
        dag_t *p_dag = build_dag_random(max_period, max_elems, wcet_max, max_edges);
        if (p_dag == NULL){
            continue;
        }
        // make sure that the critical path is < the deadline. Otherwise, discard the dag
        dag_path_t path;
        dag_path_init(&path);
        double C_sum = dag_longest_path(p_dag, p_sys, &path);
        if (((int)C_sum) > p_dag->deadline){
            LOG(DEBUG,"\n\nignoring dag with path=%f and deadline=%f\n\n", C_sum, p_dag->deadline);
            continue;
        }
        // now that we know that the dag is valid, print its spec
        #if LOG_LEVEL >= 3
            dag_dump(p_dag);
        #endif
        printf("deadline %f, period %f, n_elems %d\n", p_dag->deadline, p_dag->period, p_dag->num_elems);
        printf (" - critial path (C_sum=%f): [ ", C_sum);
        dag_path_dump(&path);
        printf(" ]\n");
        // TODO: is there any other check to make sure that the genreated dag is a valid one ?
        // compute this just to make sure that this is a valid dag
        // dag_comp_unrelated(p_dag);
        snprintf(fname, sizeof(fname), "rnd%02d.dot", i);
        printf (" - filename: %s\n", fname);
        dag_dump_dot2(p_dag, C_sum, fname, &path);
        sys_add_dag(p_sys, p_dag);
        i++;
      }
    } else if (strcmp(*argv, "--solve") == 0 || strcmp(*argv, "--solve=gurobi") == 0) {
      call_solver_gurobi = 1;
    } else if (strcmp(*argv, "--solve=heur") == 0) {
      call_solver_heur = 1;
    } else if (strcmp(*argv, "--solve=dl-split") == 0) {
      call_solver_gurobi = 1;
      use_deadline_splitting = 1;
    } else if (strcmp(*argv, "--parse-gurobi-sol") == 0) {
      parse_gurobisol = 1;
    } else if (strcmp(*argv, "--create-bounds") == 0) {
      create_non_binary_bounds = 1;
    } else if (strcmp(*argv, "--use-dl-splitting") == 0) {
      use_deadline_splitting = 1;
    } else if (strcmp(*argv, "--multi-objective") == 0) {
      use_multi_objective = 1;
    } else if (strcmp(*argv, "--omp-budg-constr") == 0) {
      constrained_omp_servers = 1;
    } else if (strcmp(*argv, "-pb") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      int ret = sscanf(*argv,"%lf", &power_budget);
      assert (ret == 1);
    } else if (strcmp(*argv, "--min-deadline") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      if (sscanf(*argv,"%lf", &min_deadline) < 0 || min_deadline < 0) {
        fprintf(stderr, "Argument to --min-deadline must be a positive number\n");
        exit(1);
      }
    } else if (strcmp(*argv, "--dot-hide-aaf") == 0) {
      dot_hide_aaf = 1;
    } else if (strcmp(*argv, "--simplify") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      if (sscanf(*argv,"%d", &num_simplify) != 1 || num_simplify < 0) {
        fprintf(stderr, "Argument to --simplify must be a positive number\n");
        exit(1);
      }
    } else if (strcmp(*argv, "--new-comp-unrelated") == 0) {
      new_comp_unrelated = 1;
    } else if (strcmp(*argv, "--avg-pow") == 0) {
      pow_exec_times_scale = AVG_EXEC_TIME_PERC;
    } else if (strcmp(*argv, "-json") == 0) {
      check(argc > 1, "Missing argument for %s option", *argv);
      argc--;  argv++;
      check (n_json_files + 1 <= MAX_JSON_FILES, "Exceeded number of json files");
      json_fname[n_json_files++] = *argv;
    } else if (strcmp(*argv, "--load-unrelated") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      in_unrel_fname = *argv;
    } else if (!parse_dag(&argc, &argv) && !parse_sys(&argc, &argv)) {
      fprintf(stderr, "Unknown command-line option: %s\n", *argv);
      exit(1);
    }
    argc--;  argv++;
  }

  for (int i = 0; i < n_json_files; ++i) {
    load_json(json_fname[i]);
  }

  if (in_unrel_fname != NULL)
    sys_load_unrelated(p_sys, in_unrel_fname);

  sys_dump(p_sys);

  unsigned long start = micros();

  double max_period = 0;
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    dag_update_first_last(p_dag);
    for (int i = 0; i < num_simplify && p_dag->num_elems >= 48; i++)
      dag_simplify_aaf(p_sys, p_dag);
    /* for (int i = 0; i < num_simplify && i < 2 && p_dag->num_elems >= 48; i++) */
    /*   dag_simplify(p_sys, p_dag); */

    sys_dump(p_sys);
    dag_comp_unrelated(p_dag);
    sys_dump(p_sys);

    if (/*(call_solver_gurobi || call_solver_heur) && */in_unrel_fname == NULL)
      dag_comp_unrelated(p_dag);

    if (p_dag->period > max_period)
      max_period = p_dag->period;
  }

  if (lp_scale == 0.0) {
    lp_scale = 1.0;
    while (max_period / lp_scale > 1000)
      lp_scale *= 1000;
  }

  printf("lp_scale=%g, max_period=%g\n", lp_scale, max_period);

  sys_dump(p_sys);

  if (out_lp_fname != NULL)
    dump_lp_sys(p_sys, out_lp_fname, power_budget, use_deadline_splitting, use_multi_objective, constrained_omp_servers);

  if (out_yml_fname != NULL)
    sys_dump_dags_yaml(p_sys, out_yml_fname);

  if (out_sh_fname != NULL)
    sys_dump_args(p_sys, out_sh_fname);

  assert(!call_solver_gurobi || (out_lp_fname != NULL && out_sol_fname != NULL));

  assert(!parse_gurobisol || (out_sol_fname != NULL && !call_solver_gurobi && !call_solver_heur));

  // at least one of the output formats (yaml or json) must be selected
  assert((out_yaml_fname != NULL) || (out_json_fname != NULL) || (out_dot_fname != NULL));
  printf("\n");

  int valid_solution=0; // 0 is invalid
  if (call_solver_gurobi) {
    // check if there is a island with capacity 1.0
    int has_capacity_1 = 0;
    for (int i = 0; i < p_sys->n_islands; i++) {
        if (p_sys->islands[i].capacity == 1.0) 
            has_capacity_1 = 1;
    }
    if (has_capacity_1 == 0){
      fprintf(stderr, "ERROR: make sure that the platform definition has one island with capacity 1.0\n");
      exit(1);
    }
    valid_solution = solve_gurobi(p_sys);
  }

  if (call_solver_heur) {
    sys_clear_deadlines(p_sys);
    sys_dump(p_sys);
    struct timespec ts_beg;
    clock_gettime(CLOCK_MONOTONIC, &ts_beg);
    valid_solution = solve_heur(p_sys);
    struct timespec ts_end;
    clock_gettime(CLOCK_MONOTONIC, &ts_end);
    unsigned long elapsed_ns = (ts_end.tv_sec - ts_beg.tv_sec) * 1000000000 + (ts_end.tv_nsec - ts_beg.tv_nsec);
    printf("solve_heur() took %lu ns\n", elapsed_ns);
    sys_dump(p_sys);
    sys_set_deadlines(p_sys);
  }

  if (valid_solution)
    printf("Valid solution found: power=%lf\n", sys_tot_pow(p_sys));

  int valid_partial_solution = 0;
  if (parse_gurobisol) {
    // OBS: these two lines break the partial result retrieval script.
    //if (out_sol_fname != NULL)
    //  valid_solution = parse_gurobi_sol(p_sys, out_sol_fname);
    if (valid_solution == 0) {
      // when running for parsing for partial solutions, these are the required options:
      // ${POWER_OPTIM_HOME}/dag/dag -s <plat-name> --parse-gurobi-sol -osol $dagset_name.sol -olp $dagset_name.lp -oyaml ${out_yaml}  

      // out_sol_fname.sol is only generated when optimal solution is found.
      // that's not the case here, where timeout exipered and we want to try to 
      // get any partial solution that was created until the timeout.
      // Partial solution files have a different format: out_sol_fname_x.sol,
      // where x is the number of partial solution. We need the last one, i.e., the one with biggest x
    
      // 1st step: get the scenario name by removing the extension from the original out_sol_fname
      strcpy(temp_out_sols_fname, out_sol_fname);
      for (int i=strlen(temp_out_sols_fname)-1; i > 0 ; --i){
        // replace the fextension marker by string delimiter. 
        if (temp_out_sols_fname[i] == '.'){
          temp_out_sols_fname[i] = 0;
        }
      }
      //printf ("YEEEEEE: %s\n", temp_out_sols_fname);
      // 2nd step: find the last partial solution
      int i=0;
      while (1){
        snprintf(temp_out_sols_fname2, sizeof(temp_out_sols_fname2),
                 "%s_%d.sol", temp_out_sols_fname, i);
        if (file_exists(temp_out_sols_fname2)){
          strcpy(temp_out_sols_fname_lastone,temp_out_sols_fname2);
        }else{
          break;
        }
        // just in case :)
        if (i>1000){
          printf ("something went wrong while searching for partial solutions ...\n");
          exit(1);
        } 
        ++i;
      }
      if (strlen(temp_out_sols_fname_lastone) == 0){
        printf ("gurobi was not able to determine any partial solution\n");
        exit(1);
      }
      // reaching this point means that the last partial solution was found
      //printf ("Last solution: %s\n", temp_out_sols_fname_lastone);
    
      // 3rd step) update out_sol_fname to refer to the last partial solution (fname pattern out_sol_fname_x.sol)
      out_sol_fname = temp_out_sols_fname_lastone;
    }
    valid_partial_solution = parse_gurobi_sol(p_sys, out_sol_fname);
  }

  printf("\n");

  if (out_yml_rtsim_dags_fname != NULL)
    sys_dump_dags_yaml_rtsim(p_sys, out_yml_rtsim_dags_fname);

  if (out_yml_rtsim_isl_fname != NULL)
    sys_dump_isl_yaml_rtsim(p_sys, out_yml_rtsim_isl_fname);

  if (out_dot_fname != NULL) {
    if (use_deadline_splitting)
      sys_set_deadlines(p_sys);

    if (!call_solver_gurobi && !call_solver_heur) {
      sys_dump_dot_input_dag(p_sys, out_dot_fname);
    } else {
      sys_dump_dot_placed(p_sys, out_dot_fname);
    }
  }

  sys_dump(p_sys);

  if (call_solver_gurobi || call_solver_heur || valid_partial_solution == 1) {
    // compact output format used to ease comparison with other optimization tools
    if (call_solver_gurobi)
       sys_dump_bbsearch(p_sys, out_sol_fname);

    if (valid_solution == 1 || valid_partial_solution == 1) {
        unsigned long duration;
        // when timeout is reached, only partial sub-optimal solutions are available.
        // These partial solutions can be parser in a 2nd run, when 'parse_gurobisol' is enabled.
        // However, the execution time must be ignored
        // Finally, when there is a scenario where execution time is 0, 
        // one can safely assume that it is a sub-optimal result
        duration = valid_solution != 0 ? micros() - start : 0;
        // keep it microseconds to ease the comparison among tools
        printf ("\n\nExecution time: %lu microseconds\n\n", duration);

        // get the dag_name from the lp filename
        char *dag_name = "";
        char solver_name[16];
        if (call_solver_heur)
            strcpy(solver_name,"dag.c.heur");
        else if (use_deadline_splitting)
            strcpy(solver_name,"dag.c.split");
        else
            strcpy(solver_name,"dag.c");
        if (out_lp_fname != NULL)
            dag_name = strdup(out_lp_fname);
        else if (out_sol_fname != NULL)
            dag_name = strdup(out_sol_fname);
        // the dag name ends when the 1st "-" is found
        for (unsigned ii=0; ii<strlen(dag_name); ++ii){
            if (dag_name[ii] == '-')
                dag_name[ii] = 0;
        }
        check(strlen(dag_name)>3, "No -olp nor -osol options were provided, cannot infer dag_name");

        // at least one output format (yaml or json) must be selected
        if (out_yaml_fname != NULL){
            sys_dump_output_yaml(p_sys, solver_name, out_yaml_fname, dag_name, platform_name, duration);
        }
        if (out_json_fname != NULL){
            json_write_results(p_sys, json_fname[0], out_json_fname);
        }
    }
  }
  if (valid_solution == 1 || valid_partial_solution == 1){
     printf ("Valid solution found !\n\n");
  }else{
     printf ("Unable to find a valid solution !\n\n");
  }
  sys_cleanup(p_sys);
  // return 0 if the solution is valid
  return valid_solution == 1 || valid_partial_solution == 1 ? 0 : 1;
}
