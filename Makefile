PROG=dag
# set -DLOG_LEVEL=X in the Makefile to select verbosity level. LOG_LEVEL 0 is the default.
# LOG_LEVEL 0: All logs are off
# LOG_LEVEL 1: Error logs or higher
# LOG_LEVEL 2: Warning logs or higher
# LOG_LEVEL 3: Info logs or higher
# LOG_LEVEL 4: Debug logs or higher
LOG_LEVEL?=0
CFLAGS=-Wall -pedantic -DLOG_LEVEL=$(LOG_LEVEL) -O3 -I../common/include -fopenmp
LDLIBS=-lm -ljson-c -fopenmp
VPATH=../common/src/

all: $(PROG) 

libs: libdag.a

# this static library is later used to link with the c++ wrapper
libdag.a: dag.o
	ar rcs libdag.a dag.o table.o 

dag: main.o table.o libdag.a

clean:
	rm -f *.o *.a $(PROG) *~ *.bak

dep:
	makedepend -Y -- *.c
# DO NOT DELETE

dag.o: dag.h
main.o: dag.h
