# MIQCP Optimizer

This repo contains the MIQCP Optimization Tool developed in the context of the AMPERE project.

# Dependencies
 - json-c-dev package (json-c-devel on rpm-based distributions)

# Building and Running

To build the software, simply run `make`.

To check all the options accepted by the `./dag` command, use the `-h` option.

```
 $ ./dag -h
Usage: dag [options...]
  Options:
    -h|--help .................. Show this help message and exit
    -s system .................. Use a named hardware platform (odroid-xu3[-half], odroid-xu4, xu4-fpga, zcu102-1slot, zcu102-mat64[-128], zcu102-mat64-128-hf, raspberry-pi4b, xavier-agx)
    -i n_cores[,capacity] ...... Add an island with the specified number of cores and capacity (defaults to 1.0)
    -ai n_cores,cap[,cs_delay] . Add an accelerated island with the specified number of cores, capacity and context switch delay (tasks need to be explicitly mapped using -a)
    -f freq,pow_busy,pow_idle .. Add an OPP to the current island (added last with -i), with the specified frequency and per-island power values (busy power refers to one CPU busy, including the island idle power)
    -d deadline,period ......... Add an empty DAG with the specified end-to-end deadline and minimum inter-arrival period
    -t WCET[,WCET_ns] .......... Add a task to the current DAG (added last with -d), with the specified WCET and non-scalable WCET (defaults to 0)
    -w island_id,freq_id,WCET .. Set WCET a specific island and frequency (specified by 0-based indexes) for the current task (added last with -t)
    -a island_id[,C[,Cns]] ..... Let the current task (added last with -t) run also on any accelerator of the island identified by the provided ID, optionally with the specified C and Cns (can be used multiple times)
    -ap acc_island_id,d_power .. Set an additional power consumption term due to the current task (added last with -t) when mapped on any accelerator of the island identified by the provided ID (can be used multiple times)
    -csd island_id,cs_delay .... Customize context-switch delay for the specified accelerator island (must have been added with -a and/or -s)
    -p elem,prev ............... Add a precedence constraint from task prev to task elem (both must have already been added with -t)
    -n elem,next ............... Add a precedence constraint from task elem to task next (both must have already been added with -t)
    -aaf task_id_1,task_id_2 ... Specify that task_id_1 and task_id_2 of the current DAG (added last with -d) cannot be mapped to the same PU (anti-affinity constraint)
    -di deadline,t_beg,t_end ... Add end-to-end intermediate deadline between activation of task t_beg and completion of task t_end in current DAG
    -olp filename.lp ........... Specify destination filename for the LP optimization problem
    -lps|--lp-scale scale ...... Specify scaling factor for LP times (defaults to auto)
    -u|--max-util bw ........... Specify the maximum per-core utilization (defaults to 0.95)
    -r k,D,n,C,e[,seed] ........ Add k random DAGs to the model, with deadline up to D, up to n nodes, up to a WCET of C, up to e edges
    --solve[=method] ........... Solve the generated LP problem using method, either gurobi (default), dl-split, or heur
    --parse-gurobi-sol ......... Parse back the Gurobi sol file specified with -osol, instead of calling the Gurobi solver
    --create-bounds ............ Create explicit bounds in the LP formulation for the values non-binary variables can take (GUROBI ONLY)
    --use-dl-splitting ......... Call the Gurobi solver with fixed deadlines assigned with deadline splitting
    --avg-pow .................. Call the Gurobi solver using the average power consumption instead of the worst case (average execution times are 95% of WCETs)
    --min-deadline ............. Set minimum possible deadline in LP formulation (defaults to 0.0)
    --multi-objective .......... Run Gurobi to optimize for both minimum power and maximum slack (power optimization has higher priority)
    -pb power_budget ........... Set a power budget so that the LP formulation maximizes the minimum slack within the power constraint
    -osol filename.sol ......... Specify destination filename for the solution of the LP problem (requires --solve)
    -osols pathname ............ Specify pathname/prefix for intermediate solutions (requires --solve)
    -odot filename.dot ......... Specify destination filename for the DOT file containing the finally optimized system (requires --solve)
    --dot-hide-aaf ............. Hide anti-affine nodes in .dot output (useful if replicas of same nodes)
    -oyml filename.yml ......... Dump current DAGs into the specified YAML file
    -osh filename.sh ........... Dump current system (islands and DAGs) into the specified SH file
    -oyml_rtsim_dags fname.yml . Dump placed DAGs into the specified YAML file for RTSim
    -oyml_rtsim_isl fname.yml .. Dump optimized islands into the specified YAML file for RTSim
    -oyaml filename.yaml ....... Specify destination filename for the optimization output in YAML format
    -ojson filename.json ....... Specify destination filename for the optimization output in JSON format
    -olog filename.log ......... Specify destination filename for the gurobi log
    -max-threads ............... Set Gurobi to use the max number of threads. Otherwise it uses the number of threads defined in MAX_GUROBI_THREADS
    -json filename.json ........ Load DAGs from specified JSON file
    -ompenv n_omp_envs ......... Define the number of OpenMP environments in the system
    -omp omp_env_id ............ Specify that the current task (added last with -t) belongs to the OpenMP environment identified with omp_env_id [0,n_omp_envs)
    --omp-budg-constr  ......... Constrain the budget of each OpenMP thread to be the maximum traversing time of the tasks served by it
    --simplify num ............. Apply num simplify() steps to DAG before computing unrelated sets
    --load-unrelated fname ..... Load unrelated sets from file
    --new-comp-unrelated ....... Use experimental comp_unrelated_new()
```
