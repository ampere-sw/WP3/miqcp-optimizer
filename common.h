#ifndef  COMMON_H_
#define COMMON_H_

#define MAX_ISLANDS 8
#define MAX_PREV 250 // max numbed of neighbor next nodes
#define MAX_NEXT 250 // max numbed of neighbor prev nodes
#define MAX_DAGS 10
#define MAX_TASKS_PER_DAG 250
#define MAX_FREQS 20
#define MAX_PUNITS 8
#define DEF_MAX_UTIL 0.95
#define MAX_DEADLINE_PERC 0.9999
#define AVG_EXEC_TIME_PERC 0.95
#define MAX_OMP_ENVS 10
#define MAX_JSON_FILES 40
#define MAX_INTERMEDIATE_DLS 40
#define GPU_TIMES_SCALE 4

#endif // COMMON_H_
