#ifndef DAG_H
#define DAG_H


// required to call from a C++ program
#ifdef __cplusplus
extern "C" {
#endif

#include "common.h"
#include "table.h"

#define MAX_MSG_SIZE 256 // this is required to be compatible w amalthea model
#define MAX_PATH_LEN 16
// this is required when lauching multiple optimization instances in parallel,
// avoiding gurobi to starve the other processes. When you need to run gurobi w 
// max number of threads, there is a new command-line argument for that: -max-threads
#define MAX_GUROBI_THREADS 4
// other definitions are now in common.h

#define C_NONE -1.0
#define POW_NONE -1.0

// TODO: in the future, when er integrate it w amalthea, it will require
// to have task name, edge name and edge size
// without these info, good look debugging a dag w thousands of nodes
// typedef struct edge {
//   int prev, next, size;
//   char name [MAX_NAME_LENGTH];
// } edge_t;

typedef struct {
  int elems[MAX_PATH_LEN];
  int n_elems; // number of used entries in elems[]
} dag_path_t;

typedef struct dag_elem {
  // TODO: for amalthea
  //char name [MAX_NAME_LENGTH];        // task name
  double C;              		// overall WCET at reference frequency and unit capacity
  double C_ns;           		// part of WCET that does not scale with frequency/capacity
  double d;              		// relative deadline
  int opt_island;     			// optimum placement island
  int opt_punit;      			// optimum placement punit
  int prev[MAX_PREV]; 			// index in dag.elems[]
  int n_prev;
  int next[MAX_NEXT]; 			// index in dag.elems[]
  int n_next;
  long accelerators;			// list of accelerators on which the task can run
  double isl_C[MAX_ISLANDS];		// per-island C at max island speed (or C_NONE if capacity is used)
  double isl_C_ns[MAX_ISLANDS];		// per-island C_ns at max island speed (or C_NONE if capacity is used)
  double isl_freq_C[MAX_ISLANDS][MAX_FREQS];	// per-island C at each island speed (if != C_NONE, overrides isl_C_ns[] and isl_C[])
  double isl_freq_avg_C[MAX_ISLANDS][MAX_FREQS];  // per-island average exec time at each island speed (if != C_NONE, overrides 0.95 * WCET)
  double isl_freq_pow_diff[MAX_ISLANDS][MAX_FREQS];  // per-island power diff of the task at each island speed (if != POW_NONE, overrides the power model)
  int anti_affinity[MAX_TASKS_PER_DAG]; // list of tasks (index in dag.elems[]) this task can't be mapped with
  int n_anti_affinity;
  double isl_pow[MAX_ISLANDS];          // additional power consumption constant term per accelerator
  double longest_len;                   // longest length from this node to sink (both included)
  int longest_next;                     // next (idx in dag.elems[]) in longest path from this node to sink, or -1
  int omp_env_id;                       // OpenMP environment id the task belongs to (-1 if not an OpenMP task)
  int fictitious;
} dag_elem_t;

typedef struct inter_dl {
  int start_elem_id;
  int end_elem_id;
  double deadline;
} intermediate_dl;

typedef struct dag_st {
  dag_elem_t elems[MAX_TASKS_PER_DAG];
  int num_elems;                // elements used in elems[]
  int first, last;              // indexes in elems[]
  int **relatives;              // matrix of transitive dependencies
  unsigned long *unrelated;     // subsets of unrelated tasks
  int unrelated_max_size;       // maximum number of elements unrelated[] was allocated for
  int unrelated_size;
  double deadline, period;
  char *name;
  struct dag_st *previous;                  // used in the case of sequential DAGs
  intermediate_dl inter_dls[MAX_INTERMEDIATE_DLS];
  int n_intermediate_dls;
} dag_t;

typedef enum {
  SYS_SCHED_EDF,
  SYS_SCHED_FIFO
} sys_sched_t;

struct tlist_node {
  dag_elem_t *p_task;
  struct tlist_node *p_next;
};

typedef struct island *p_island_t;

typedef struct punit {
  p_island_t p_island;
} punit_t;

typedef enum {
  CPU,
  ACCELERATOR
} island_type_enum;

typedef struct island {
  double capacity;
  sys_sched_t sched;
  int n_punits;
  punit_t punits[MAX_PUNITS];
  double freqs[MAX_FREQS];
  int n_freqs;
  double pows_busy[MAX_FREQS];
  double pows_idle[MAX_FREQS];
  int opt_freq;                 // freq after placement optimization
  struct table_t* table;        // just for print the power table in a nice format
  island_type_enum type;				// CPU or accelerator
  double cs_delay;               // context switch delay
} island_t;

typedef struct omp_wt {
  p_island_t p_island;
  double opt_budget;          // server budget found by the optimizer
  double opt_period;          // server period found by the optimizer
} omp_wt_t;

typedef struct omp_env {
  omp_wt_t worker_threads[MAX_ISLANDS][MAX_PUNITS];
  int n_islands;
  int n_punits[MAX_ISLANDS];
} omp_env_t;

typedef struct {
  int n_islands;
  island_t islands[MAX_ISLANDS];
  int n_dags;
  dag_t *dags[MAX_DAGS];
  long hyperperiod;
  double opt_pow;
  omp_env_t omp_envs[MAX_OMP_ENVS];
  int n_omp_envs;
  int amalthea_to_dag_id[MAX_DAGS];
  int amalthea_to_dag_task_id_offset[MAX_DAGS];
  int n_amalthea_dags;
  char* amalthea_dag_names[MAX_DAGS];
  dag_t *last_dag;                               // used in the case of sequential DAGs to keep track of where the computation ends
} sys_t;

#define foreach_elem_next(p_dag, e) for (int __i = 0, n = p_dag->elems[e].next[__i]; __i < p_dag->elems[e].n_next; __i++, n = p_dag->elems[e].next[__i])

#define foreach_elem_prev(p_dag, e) for (int __i = 0, p = p_dag->elems[e].prev[__i]; __i < p_dag->elems[e].n_prev; __i++, p = p_dag->elems[e].prev[__i])

#define foreach_longest_next(p_dag, s, n) for (int n = s; n != -1; n = p_dag->elems[n].longest_next)

extern sys_t *p_sys;
extern char *out_lp_fname;
extern char *out_sol_fname;
extern char *out_sols_pname;
extern char *out_dot_fname;
extern char *gurobi_log_fname;
extern int gurobi_max_threads;
extern double lp_scale;
extern double max_util;   // maximum utilization allowed on each punit
extern double pow_exec_times_scale;
extern int create_non_binary_bounds;
extern double min_deadline;
extern int dot_hide_aaf;
extern int new_comp_unrelated;

void dag_init(dag_t *p_dag, double deadline, double period) ;

void dag_set_name(dag_t *p_dag, char *name);

void dag_cleanup(dag_t *p_dag) ;

int dag_num_elems(dag_t *p_dag) ;

int dag_add_elem(dag_t *p_dag, double C, double C_ns, double d);

void dag_add_prev(dag_t *p_dag, int n, int prev) ;

void dag_add_next(dag_t *p_dag, int n, int next) ;

int dag_elem_del_prev(dag_elem_t *p_elem, int prev);

int dag_elem_del_next(dag_elem_t *p_elem, int next);

int dag_elem_del_aaf(dag_elem_t *p_elem, int n);
int dag_elem_find_aaf(dag_elem_t *p_elem, int e);

// Add to elem e AAF constraint towards elem f (does not enforce symmetrical AAF constraints)
// return 1 if AAF constraint was added, 0 if it was already there
int dag_set_aaf(dag_t *p_dag, int e, int f);

// Update first and last entries, and double-check topology has unique first and last nodes
void dag_update_first_last(dag_t *p_dag);

// Set WCET for specific island and frequency, for the last added task on the last added DAG
void dag_set_wcet(int isl_id, int freq_id, double C);

void dag_set_wcet_task_id(int isl_id, int freq_id, int dag_id, int task_id, double C);

void dag_set_avg_exec_time(int isl_id, int freq_id, double C);

void dag_set_avg_exec_time_task_id(int isl_id, int freq_id, int dag_id, int task_id, double C);

void dag_set_pow_diff(int isl_id, int freq_id, double pow_diff);

void dag_set_pow_diff_task_id(int isl_id, int freq_id, int dag_id, int task_id, double pow_diff);

// Check if b is among a's next elems
int dag_is_successor(dag_t *p_dag, int a, int b) ;

// Check if b reachable from a
int dag_is_reachable(dag_t *p_dag, int a, int b) ;

void dag_comp_unrelated(dag_t *p_dag);

void dag_dump_unrelated(dag_t *p_dag);

void sys_load_unrelated(sys_t *p_sys, char *fname);

double dag_elem_wcet_scaled(sys_t *p_sys, dag_t *p_dag, int i, island_t *p_isl, int m);
double dag_elem_wcet_unscaled(sys_t *p_sys, dag_t *p_dag, int e);

void dag_dump(dag_t *p_dag);

void dag_dump_args(dag_t *p_dag, FILE *f);

void sys_dump_args(sys_t *p_dag, char *fname);

void dag_dump_dot(dag_t *p_dag, char *fname);

void dag_dump_dot2(dag_t *p_dag, double C_sum, char *fname, dag_path_t *p_path);

void sys_dump_dags_yaml(sys_t *p_sys, char *fname);

void sys_dump_dags_yaml_rtsim(sys_t *p_sys, char *fname);

void sys_dump_isl_yaml_rtsim(sys_t *p_sys, char *fname);

void sys_dump_args(sys_t *p_sys, char *fname);

void sys_dump_dot_placed(sys_t *p_sys, char *fname);

// print the results in a format easier to compare with bb-search
void sys_dump_bbsearch(sys_t *p_sys, char *fname);

double sys_tot_pow(sys_t *p_sys);

// output binary variables for DAG d: x_dag_task_island_punit
void dump_lp_dag_bin(sys_t *p_sys, int d, FILE *f) ;

// output constraints for DAG d
void dump_lp_dag_constr(sys_t *p_sys, int d, FILE *f);

void dump_lp_dag_pow(FILE *f, sys_t *p_sys, island_t *p_isl, int s, int p, int m, int d);

// easy but pessimistic: sum of bandwidths of all deployed tasks
void dump_lp_punit_constr_easy(FILE *f, sys_t *p_sys, int s, int p, int d);

// less pessimistic: sum of bandwidths of all unrelated task subsets in each DAG
void dump_lp_punit_constr_unrelated(FILE *f, sys_t *p_sys, int s, int p, int d);

void dump_lp_island_constr(FILE *f, sys_t *p_sys, int s) ;

void dump_lp_sys(sys_t *p_sys, const char *fname, double power_budget, int use_dl_splitting, int multi_objective, int constrained_omp_servers);

void dag_path_init(dag_path_t *p_dag_path) ;

// copy src path to tail of dst path
void dag_path_cat(dag_path_t *p_dst_path, dag_path_t *p_src_path);

// return scaled WCET if elem e of dag is placed on island with assigned opt_freq;
// return scaled WCET at max island freq if elem e is placed on some island;
// otherwise, return just elem's WCET
double dag_elem_wcet_or_scaled(dag_t *p_dag, sys_t *p_sys, int e) ;

// return path length from p_dag->first to p_dag->last, both included,
// or -1 if last is not reachable from first
// Warning: dag_longest_path_dl() and dag_longest_path() overwite the cache of longest_next
double dag_longest_path(dag_t *p_dag, sys_t *p_sys, dag_path_t *p_path) ;

// use already stored deadlines instead of WCETs (returns the worst-case response-time)
// Warning: dag_longest_path_dl() and dag_longest_path() overwite the cache of longest_next
double dag_longest_path_dl(dag_t *p_dag, sys_t *p_sys, dag_path_t *p_path);

void dag_path_dump(dag_path_t *p_path);

void dag_clear_deadlines(dag_t *p_dag);

void dag_set_deadlines(dag_t *p_dag, sys_t *p_sys, int s, int e, double D, int clear);

void sys_set_deadlines(sys_t *p_sys);

void sys_clear_deadlines(sys_t *p_sys); 

void sys_init(sys_t *p_sys);

void island_dump(island_t *p_isl);

void sys_dump(sys_t *p_sys);

void sys_cleanup(sys_t *p_sys);

island_t *sys_add_island(sys_t *p_sys, double capacity, int n_punits, sys_sched_t sched, island_type_enum type, double cs_delay);

void sys_add_dag(sys_t *p_sys, dag_t *p_dag) ;

int sys_find_dag(sys_t *p_sys, dag_t *p_dag);

dag_t *sys_new_dag(sys_t *p_sys, double deadline, double period);

void island_add_freq(island_t *p_sys, double f, double pow_busy, double pow_idle);

int parse_gurobi_sol(sys_t *p_sys, const char *fname);

int solve_gurobi(sys_t *p_sys);

double punit_tot_util(sys_t *p_sys, int s, int p);

/*
 * worst-fit: search for (island, core) minimizing the max occupied bandwidth after addition of p_elem
 *   bandwidths obtained dividing by either the relative deadlines if present, or the dag deadlines
 *   occupied bandwidth computed according to the unrelated[] sets, that must have been pre-computed
 */
void place_wf_elem(sys_t *p_sys, dag_elem_t *p_elem);

void place_wf(sys_t *p_sys);

int solve_heur(sys_t *p_sys);

dag_t *build_dag_random(int max_period, int max_elems, int wcet_max, int max_edges);

void dag_simplify(sys_t *p_sys, dag_t *p_dag);
void dag_simplify_aaf(sys_t *p_sys, dag_t *p_dag);

/* This function saves all relavant values into the output yaml format.
This format is also generated by the heuristics, such that it facilitates to gater 
statistics and comparison amont the tools.
Check out the equivalent function 'detailed_yaml_output_format' for the heuristics.

 - This part of the yaml file has to be filled in by the optimization tool main function:
tool_name: "dag.c"
platform_name: "odroid-xu3"
platform_filename: "" // dag.c has no such file
dag_filename: "./ex1.sh"
execution_time_us: 1234

- Here is an example of the expected format:
n_dags: 2
power: 1.23
islands:
    - capacity: 0.3
      freq: 1200
      tasks: [(0,0),(0,2),(1,0)] // tuple format (dag,task)
      pus:
        - []
        - [(0.4,0,0),(0.2,0,2] // tuple format (utilization, dag, task)
        - []
        - [(0.5,1,0)]
    - capacity: 1.0
      freq: 500
      tasks: [(0,1),(1,1)]
      pus:
        - [(0.3,0,1)]
        - [(0.5,1,1)]
        - []
        - []
tasks:
    - id: (0,0)
      wcet: 1234
      deadline: 1500
    - id: (0,1)
      wcet: 1000
      deadline: 1500
    - id: (0,2)
      wcet: 1000
      deadline: 1200
    - id: (1,0)
      wcet: 500
      deadline: 1000
    - id: (1,1)
      wcet: 500
      deadline: 1000
*/
void sys_dump_output_yaml(sys_t *p_sys, char *solver_name, char *fname, 
    char *dag_name, char *platform_name, unsigned long exec_time_us);

void dag_add_acc_power(int acc_id, double power);
void dag_add_acc(int acc_id, double C, double C_ns);
void dag_add_acc_task_id(int acc_id, int dag_id, int task_id, double C, double C_ns);
void dag_add_omp_env(int omp_env_id);
void dag_add_omp_env_task_id(int omp_env_id, int dag_id, int task_id);
void sys_add_omp_envs(sys_t *p_sys, int n_envs);
void dag_add_intermediate_dl(dag_t *p_dag, int start_elem_id, int end_elem_id, double dl);
void sys_dump_dot_input_dag(sys_t *p_sys, char *fname);
double dag_elem_avg_exec_time_scaled(sys_t *p_sys, dag_t *p_dag, int i, island_t *p_isl, int m);

#ifdef __cplusplus
}
#endif

#endif //DAG_H
