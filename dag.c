#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <float.h>
#include <json-c/json.h>
#include <omp.h>

#include "dag.h"
#include "log.h"

// 99.9 % of the time gurobi prints 0 or 1 in the solution, but sometimes it prints 9.9999999999999967e-01 :/
#define FEQUAL(N1, N2) ((fabs(N1-N2)<0.000001f)?(1):(0))

const char *node_colors[] = { "darkred", "darkgreen", "darkblue", "darkorange", "darkviolet", "darkbrown", "darkseagreen", "darkslategray" };
const char *omp_env_colors[] = { "lightskyblue2", "lightpink1", "orange1", "lightskyblue2", "lightpink1", "orange1", "lightskyblue2", "lightpink1", "orange1"};
const char *pu_color = "gray93";
const int nodes_width = 3;

void dag_init(dag_t *p_dag, double deadline, double period) {
  assert(period >= deadline);
  p_dag->first = p_dag->last = -1;
  p_dag->num_elems = 0;
  p_dag->relatives = NULL;
  p_dag->unrelated = NULL;
  p_dag->unrelated_max_size = p_dag->unrelated_size = 0;
  p_dag->deadline = deadline;
  p_dag->period = period;
  p_dag->previous = NULL;
  p_dag->name = NULL;
  p_dag->n_intermediate_dls = 0;
}

void dag_set_name(dag_t *p_dag, char *name) {
  p_dag->name = malloc(strlen(name) + 1);
  strcpy(p_dag->name, name);
}

void dag_cleanup(dag_t *p_dag) {
  if (p_dag->unrelated != NULL)
    free(p_dag->unrelated);
  p_dag->unrelated = 0;
  if (p_dag->relatives != NULL) {
    for (int i = 0; i < p_dag->num_elems; i++) {
      if ((p_dag->relatives[i] != NULL)) {
        free(p_dag->relatives[i]);
      }
    }
  }
  free(p_dag->relatives);
  free(p_dag);
  // p_dag->relatives = 0;
}

int dag_num_elems(dag_t *p_dag) {
  return p_dag->num_elems;
}

int dag_add_elem(dag_t *p_dag, double C, double C_ns, double d) {
  assert(p_dag->num_elems <= MAX_TASKS_PER_DAG);
//  assert(C > 0);
  assert(C_ns >= 0);
  int n = p_dag->num_elems++;
  if (n == 0)
    p_dag->first = n;
  p_dag->last = n;
  p_dag->elems[n].C = C;
  p_dag->elems[n].C_ns = C_ns;
  p_dag->elems[n].d = d;
  p_dag->elems[n].opt_island = -1;
  p_dag->elems[n].opt_punit = -1;
  p_dag->elems[n].n_prev = 0;
  p_dag->elems[n].n_next = 0;
  p_dag->elems[n].n_anti_affinity = 0;
  p_dag->elems[n].accelerators = 0;
  for (int i = 0; i < MAX_ISLANDS; i++) {
    p_dag->elems[n].isl_C[i] = C_NONE;
    p_dag->elems[n].isl_C_ns[i] = 0.0;
    for (int m = 0; m < MAX_FREQS; m++) {
      p_dag->elems[n].isl_freq_C[i][m] = C_NONE;
      p_dag->elems[n].isl_freq_avg_C[i][m] = C_NONE;
      p_dag->elems[n].isl_freq_pow_diff[i][m] = POW_NONE;
    }
    p_dag->elems[n].isl_pow[i] = 0.0;
  }
  p_dag->elems[n].longest_len = -1;
  p_dag->elems[n].longest_next = -1;
  p_dag->elems[n].omp_env_id = -1;
  p_dag->elems[n].fictitious = 0;
  return n;
}

// need to subtract 1 to all node identifiers after 'e'
void dag_del_elem(dag_t *p_dag, int e) {
  for (int f = e; f < p_dag->num_elems - 1; f++) {
    // TODO: check if need to free dynamic memory in p_dag->elems[f + 1]
    p_dag->elems[f] = p_dag->elems[f + 1];
  }
  p_dag->num_elems--;
  for (int f = 0; f < p_dag->num_elems; f++) {
    dag_elem_t *p_elem = &p_dag->elems[f];
    dag_elem_del_prev(p_elem, e);
    for (int i = 0; i < p_elem->n_prev; i++)
      if (p_elem->prev[i] > e)
        p_elem->prev[i]--;
    dag_elem_del_next(p_elem, e);
    for (int i = 0; i < p_elem->n_next; i++)
      if (p_elem->next[i] > e)
        p_elem->next[i]--;
    dag_elem_del_aaf(p_elem, e);
    for (int i = 0; i < p_elem->n_anti_affinity; i++)
      if (p_elem->anti_affinity[i] > e)
        p_elem->anti_affinity[i]--;
  }
  if (p_dag->first == e)
    p_dag->first = -1;
  else if (p_dag->first > e)
    p_dag->first--;
  if (p_dag->last == e)
    p_dag->last = -1;
  else if (p_dag->last > e)
    p_dag->last--;

  // FIXME: check the case in which the to-be-deleted element is one
  // of the start/end elems
  for (int i = 0; i < p_dag->n_intermediate_dls; i++) {
    if (p_dag->inter_dls[i].start_elem_id > e)
      p_dag->inter_dls[i].start_elem_id--;
    if (p_dag->inter_dls[i].end_elem_id > e)
      p_dag->inter_dls[i].end_elem_id--;
  }
}

void dag_add_intermediate_dl(dag_t *p_dag, int start_elem_id, int end_elem_id, double dl) {
  check(p_dag->n_intermediate_dls + 1 <= MAX_INTERMEDIATE_DLS, "Exceeded max number of intermediate deadlines");
  intermediate_dl *inter_dl = &(p_dag->inter_dls[p_dag->n_intermediate_dls++]);
  inter_dl->start_elem_id = start_elem_id;
  inter_dl->end_elem_id = end_elem_id;
  inter_dl->deadline = dl;
}

// TODO: for amalthea compatibility
// void dag_add_prev(dag_t *p_dag, int n, int prev, int size) {
void dag_add_prev(dag_t *p_dag, int e, int prev) {
  assert(prev >= 0 && prev < p_dag->num_elems);
  assert(e >= 0 && e < p_dag->num_elems);
  dag_elem_t *p_elem = &p_dag->elems[e];
  foreach_elem_prev(p_dag, e)
    if (p == prev)
      return;
  assert(p_elem->n_prev < MAX_PREV);
  p_elem->prev[p_elem->n_prev++] = prev;
  p_elem = &p_dag->elems[prev];
  assert(p_elem->n_next < MAX_NEXT);
  p_elem->next[p_elem->n_next++] = e;
}

// Return 1 if prev was removed, 0 if not found
int dag_elem_del_prev(dag_elem_t *p_elem, int prev) {
  for (int i = 0; i < p_elem->n_prev; i++) {
    if (p_elem->prev[i] == prev) {
      p_elem->prev[i] = p_elem->prev[--p_elem->n_prev];
      return 1;
    }
  }
  return 0;
}

// return 1 if AAF constraint was added, 0 if it was already there
int dag_set_aaf(dag_t *p_dag, int e, int f) {
  assert(e >= 0 && e < p_dag->num_elems && f >= 0 && f < p_dag->num_elems);
  dag_elem_t *p_elem = &p_dag->elems[e];
  if (dag_elem_find_aaf(p_elem, f) != -1)
    return 0;

  assert(p_elem->n_anti_affinity < MAX_TASKS_PER_DAG);
  p_elem->anti_affinity[p_elem->n_anti_affinity++] = f;
  return 1;
}

//TODO: add msg size
void dag_add_next(dag_t *p_dag, int e, int next) {
  dag_add_prev(p_dag, next, e);
}

// Return 1 if next was removed, 0 if not found
int dag_elem_del_next(dag_elem_t *p_elem, int next) {
  for (int i = 0; i < p_elem->n_next; i++) {
    if (p_elem->next[i] == next) {
      p_elem->next[i] = p_elem->next[--p_elem->n_next];
      return 1;
    }
  }
  return 0;
}

// Return 1 if n was removed, 0 if not found
int dag_elem_del_aaf(dag_elem_t *p_elem, int n) {
  for (int i = 0; i < p_elem->n_anti_affinity; i++) {
    if (p_elem->anti_affinity[i] == n) {
      p_elem->anti_affinity[i] = p_elem->anti_affinity[--p_elem->n_anti_affinity];
      return 1;
    }
  }
  return 0;
}

void dag_update_first_last(dag_t *p_dag) {
  p_dag->first = p_dag->last = -1;

  if (p_dag->num_elems == 1) {
    p_dag->first = p_dag->last = 0;
    return;
  }

  for (int i = 0; i < p_dag->num_elems; i++) {
    if (p_dag->elems[i].n_prev == 0) {
      assert(p_dag->first == -1);
      p_dag->first = i;
    } else if (p_dag->elems[i].n_next == 0) {
      assert(p_dag->last == -1);
      p_dag->last = i;
    }
  }
  assert(p_dag->first != -1 && p_dag->last != -1);
}

// Check if b is among a's next elems
int dag_is_successor(dag_t *p_dag, int a, int b) {
  foreach_elem_next(p_dag, a)
    if (n == b)
        return 1;
  return 0;
}

// Check if b reachable from a
int dag_is_reachable(dag_t *p_dag, int a, int b) {
  foreach_elem_next(p_dag, a)
    if (n == b)
        return 1;
  foreach_elem_next(p_dag, a)
    if (dag_is_reachable(p_dag, n, b))
        return 1;
  return 0;
}

void dag_set_wcet(int isl_id, int freq_id, double C) {
  check(isl_id >= 0 && isl_id < p_sys->n_islands, "Supplied island ID out of range");
  dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
  dag_set_wcet_task_id(isl_id, freq_id, p_sys->n_dags - 1, p_dag->num_elems - 1, C);
}

void dag_set_wcet_task_id(int isl_id, int freq_id, int dag_id, int task_id, double C) {
  check(isl_id >= 0 && isl_id < p_sys->n_islands, "Supplied island ID out of range");
  check(dag_id >=0 && dag_id < p_sys->n_dags && task_id >= 0 && task_id < p_sys->dags[dag_id]->num_elems, "Supplied task id out of range");
  dag_t *p_dag = p_sys->dags[dag_id];
  dag_elem_t *p_elem = &(p_dag->elems[task_id]);
  check(freq_id >= 0 && freq_id < p_sys->islands[isl_id].n_freqs, "Supplied frequency ID out of range");
  p_elem->isl_freq_C[isl_id][freq_id] = C;
}

void dag_set_avg_exec_time(int isl_id, int freq_id, double C) {
  check(isl_id >= 0 && isl_id < p_sys->n_islands, "Supplied island ID out of range");
  dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
  dag_set_avg_exec_time_task_id(isl_id, freq_id, p_sys->n_dags - 1, p_dag->num_elems - 1, C);
}

void dag_set_avg_exec_time_task_id(int isl_id, int freq_id, int dag_id, int task_id, double C) {
  check(isl_id >= 0 && isl_id < p_sys->n_islands, "Supplied island ID out of range");
  check(dag_id >=0 && dag_id < p_sys->n_dags && task_id >= 0 && task_id < p_sys->dags[dag_id]->num_elems, "Supplied task id out of range");
  dag_t *p_dag = p_sys->dags[dag_id];
  dag_elem_t *p_elem = &(p_dag->elems[task_id]);
  check(freq_id >= 0 && freq_id < p_sys->islands[isl_id].n_freqs, "Supplied frequency ID out of range");
  p_elem->isl_freq_avg_C[isl_id][freq_id] = C;
}

void dag_set_pow_diff(int isl_id, int freq_id, double pow_diff) {
  check(isl_id >= 0 && isl_id < p_sys->n_islands, "Supplied island ID out of range");
  dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
  dag_set_pow_diff_task_id(isl_id, freq_id, p_sys->n_dags - 1, p_dag->num_elems - 1, pow_diff);
}

void dag_set_pow_diff_task_id(int isl_id, int freq_id, int dag_id, int task_id, double pow_diff) {
  check(isl_id >= 0 && isl_id < p_sys->n_islands, "Supplied island ID out of range");
  check(dag_id >=0 && dag_id < p_sys->n_dags && task_id >= 0 && task_id < p_sys->dags[dag_id]->num_elems, "Supplied task id out of range");
  dag_t *p_dag = p_sys->dags[dag_id];
  dag_elem_t *p_elem = &(p_dag->elems[task_id]);
  check(freq_id >= 0 && freq_id < p_sys->islands[isl_id].n_freqs, "Supplied frequency ID out of range");
  p_elem->isl_freq_pow_diff[isl_id][freq_id] = pow_diff;
}

int dag_elem_has_prev(dag_t *p_dag, int e, int q) {
  foreach_elem_prev(p_dag, e)
    if (p == q)
      return 1;
  return 0;
}

int dag_elem_has_next(dag_t *p_dag, int e, int m) {
  foreach_elem_next(p_dag, e)
    if (n == m)
      return 1;
  return 0;
}

int dag_elem_same_prev(dag_t *p_dag, int a, int b) {
  if (p_dag->elems[a].n_prev != p_dag->elems[b].n_prev)
    return 0;
  foreach_elem_prev(p_dag, a)
    if (!dag_elem_has_prev(p_dag, b, p))
      return 0;
  foreach_elem_prev(p_dag, b)
    if (!dag_elem_has_prev(p_dag, a, p))
      return 0;
  return 1;
}

int dag_elem_same_next(dag_t *p_dag, int a, int b) {
  if (p_dag->elems[a].n_next != p_dag->elems[b].n_next)
    return 0;
  foreach_elem_next(p_dag, a)
    if (!dag_elem_has_next(p_dag, b, n))
      return 0;
  foreach_elem_next(p_dag, b)
    if (!dag_elem_has_next(p_dag, b, n))
      return 0;
  return 1;
}

// returning -1 if not found
int dag_elem_find_aaf(dag_elem_t *p_elem, int e) {
  for (int i = 0; i < p_elem->n_anti_affinity; i++)
    if (p_elem->anti_affinity[i] == e)
      return i;
  return -1;
}

int dag_elem_same_aaf(dag_t *p_dag, int a, int b) {
  if (p_dag->elems[a].n_anti_affinity != p_dag->elems[b].n_anti_affinity)
    return 0;
  for (int i = 0; i < p_dag->elems[a].n_anti_affinity; i++)
    if (dag_elem_find_aaf(&p_dag->elems[b], p_dag->elems[a].anti_affinity[i]) == -1)
      return 0;
  for (int i = 0; i < p_dag->elems[b].n_anti_affinity; i++)
    if (dag_elem_find_aaf(&p_dag->elems[a], p_dag->elems[b].anti_affinity[i]) == -1)
      return 0;
  return 1;
}

double add_val_none(double v, double w) {
  if (v == C_NONE)
    return w;
  if (w == C_NONE)
    return v;
  return v + w;
}

void dag_replace_aaf(dag_t *p_dag, int b, int a) {
  for (int i = 0; i < p_dag->num_elems; ++i) {
    dag_elem_t *p_elem = &p_dag->elems[i];
    if (dag_elem_del_aaf(p_elem, b))
      dag_set_aaf(p_dag, i, a);
  }
}

// Merge contents of elem b into elem a, then delete elem b
void dag_merge_elems(sys_t *p_sys, dag_t *p_dag, int a, int b) {
  LOG(DEBUG, "Merging elems %d (C=%g, n_aff=%d) and %d (C=%g, n_aff=%d)\n",
      a, dag_elem_wcet_unscaled(p_sys, p_dag, a), p_dag->elems[a].n_anti_affinity,
      b, dag_elem_wcet_unscaled(p_sys, p_dag, b), p_dag->elems[b].n_anti_affinity);
  p_dag->elems[a].C = add_val_none(p_dag->elems[a].C, p_dag->elems[b].C);
  p_dag->elems[a].C_ns = add_val_none(p_dag->elems[a].C_ns, p_dag->elems[b].C_ns);
  for (int i = 0; i < p_sys->n_islands; i++) {
    if (p_sys->islands[i].type == ACCELERATOR && (!(p_dag->elems[a].accelerators & (1 << i))))
      continue;

    p_dag->elems[a].isl_C[i] = add_val_none(p_dag->elems[a].isl_C[i], p_dag->elems[b].isl_C[i]);
    p_dag->elems[a].isl_C_ns[i] = add_val_none(p_dag->elems[a].isl_C_ns[i], p_dag->elems[b].isl_C_ns[i]);
    for (int j = 0; j < p_sys->islands[i].n_freqs; j++) {
      p_dag->elems[a].isl_freq_C[i][j] = add_val_none(p_dag->elems[a].isl_freq_C[i][j], p_dag->elems[b].isl_freq_C[i][j]);
      // we *sum* here the avg of the two merged avg C
      p_dag->elems[a].isl_freq_avg_C[i][j] = add_val_none(p_dag->elems[a].isl_freq_avg_C[i][j], p_dag->elems[b].isl_freq_avg_C[i][j]);

      double avg_a = dag_elem_avg_exec_time_scaled(p_sys, p_dag, a, &p_sys->islands[i], j);
      double avg_b = dag_elem_avg_exec_time_scaled(p_sys, p_dag, b, &p_sys->islands[i], j);
      p_dag->elems[a].isl_freq_pow_diff[i][j] = (p_dag->elems[a].isl_freq_pow_diff[i][j] * avg_a + p_dag->elems[b].isl_freq_pow_diff[i][j] * avg_b) / (p_dag->elems[a].isl_freq_avg_C[i][j]);
    }
  }

  for (int i = 0; i < p_dag->elems[b].n_anti_affinity; ++i) {
    dag_set_aaf(p_dag, a, p_dag->elems[b].anti_affinity[i]);
  }

  dag_replace_aaf(p_dag, b, a);

  p_dag->elems[a].accelerators &= p_dag->elems[b].accelerators;
  foreach_elem_next(p_dag, b)
    dag_add_next(p_dag, a, n);
  dag_del_elem(p_dag, b);

  LOG(DEBUG, "Merged elem %d (C=%g, n_aff=%d)\n", a, dag_elem_wcet_unscaled(p_sys, p_dag, a), p_dag->elems[a].n_anti_affinity);
}

int dag_mergeable(dag_t *p_dag, int a, int b) {
  return dag_elem_same_prev(p_dag, a, b)
    /* && p_dag->elems[a].n_next <= 32 && p_dag->elems[b].n_next <= 32 */
    && dag_elem_find_aaf(&p_dag->elems[a], b) == -1 && dag_elem_find_aaf(&p_dag->elems[b], a) == -1
    && p_dag->elems[a].accelerators == p_dag->elems[b].accelerators
    && p_dag->elems[a].omp_env_id == p_dag->elems[b].omp_env_id
    && !p_dag->elems[a].fictitious && !p_dag->elems[b].fictitious;
}

int dag_mergeable_aaf(dag_t *p_dag, int a, int b) {
  if (!dag_mergeable(p_dag, a, b)
    || p_dag->elems[a].n_anti_affinity != p_dag->elems[b].n_anti_affinity)
    return 0;
  for (int i = 0; i < p_dag->elems[a].n_anti_affinity; i++) {
    if (!dag_mergeable(p_dag, p_dag->elems[a].anti_affinity[i], p_dag->elems[b].anti_affinity[i]))
      return 0;
  }
  return 1;
}

void dag_simplify_aaf(sys_t *p_sys, dag_t *p_dag) {
  LOG(DEBUG, "dag_simplify_aaf(%d) beg: num_elems=%d\n", sys_find_dag(p_sys, p_dag), p_dag->num_elems);
  for (int a = 0; a < p_dag->num_elems-1; a++) {
    for (int b = a+1; b < p_dag->num_elems; b++) {
      if (dag_mergeable_aaf(p_dag, a, b)) {
        LOG(DEBUG, "%d mergeable_aaf with %d\n", a, b);
        for (int i = 0; i < p_dag->elems[a].n_anti_affinity; i++) {
          // elem b aaf[0] is deleted by dag_del_elem() at end of merge,
          // so use always aaf[0] for b only
          dag_merge_elems(p_sys, p_dag, p_dag->elems[a].anti_affinity[i], p_dag->elems[b].anti_affinity[0]);
        }
        dag_merge_elems(p_sys, p_dag, a, b);
        LOG(DEBUG, "elem %d AAF:\n", a);
        for (int i = 0; i < p_dag->elems[a].n_anti_affinity; i++)
          LOG(DEBUG, "-> %d (n_aaf: %d)\n", p_dag->elems[a].anti_affinity[i], p_dag->elems[p_dag->elems[a].anti_affinity[i]].n_anti_affinity);
        break;
      }
    }
  }
  LOG(DEBUG, "dag_simplify_aaf(%d) end: num_elems=%d\n", sys_find_dag(p_sys, p_dag), p_dag->num_elems);
}

void dag_simplify(sys_t *p_sys, dag_t *p_dag) {
  printf("dag_simplify(%d) beg: num_elems=%d\n", sys_find_dag(p_sys, p_dag), p_dag->num_elems);
  for (int a = 0; a < p_dag->num_elems-1; a++) {
    for (int b = a+1; b < p_dag->num_elems; b++) {
      if (dag_mergeable(p_dag, a, b)) {
        dag_merge_elems(p_sys, p_dag, a, b);
        break;
      }
    }
  }
  printf("dag_simplify(%d) end: num_elems=%d\n", sys_find_dag(p_sys, p_dag), p_dag->num_elems);
}

void dag_comp_relatives(dag_t *p_dag) {
  p_dag->relatives = malloc(sizeof(int*) * p_dag->num_elems);
  assert(p_dag->relatives != NULL);
  for (int i = 0; i < p_dag->num_elems; i++) {
    p_dag->relatives[i] = malloc(sizeof(int) * p_dag->num_elems);
    assert(p_dag->relatives[i] != NULL);
  }
  for (int a = 0; a < p_dag->num_elems-1; a++)
    for (int b = a+1; b < p_dag->num_elems; b++)
      if (dag_is_reachable(p_dag, a, b) || dag_is_reachable(p_dag, b, a)) {
        p_dag->relatives[a][b] = 1;
        p_dag->relatives[b][a] = 1;
      } else {
        p_dag->relatives[a][b] = 0;
        p_dag->relatives[b][a] = 0;
      }
}

// return 1 if unrelated[] size was increased
int dag_add_unrelated(dag_t *p_dag, unsigned long s) {
  for (int i = 0; i < p_dag->unrelated_size; i++) {
    if ((p_dag->unrelated[i] & s) == s) {
      // s is included in, or is equal to, a set already in unrelated[], nothing to do
      return 0;
    } else if ((p_dag->unrelated[i] & s) == p_dag->unrelated[i]) {
      // s strictly includes, thus expands, a set already in unrelated[], so the latter is replaced by s
      p_dag->unrelated[i] = s;
      if (LOG_LEVEL >= DEBUG) {
        printf("unrelated[] changed to: ");
        dag_dump_unrelated(p_dag);
      }
      return 0;
    }
  }
  check(p_dag->unrelated_size < p_dag->unrelated_max_size, "Unexpected number of unrelated sets found (num_elems: %d)", p_dag->num_elems);
  p_dag->unrelated[p_dag->unrelated_size++] = s;
  if (LOG_LEVEL >= DEBUG) {
    printf("unrelated[] changed to: ");
    dag_dump_unrelated(p_dag);
  }
  if (p_dag->unrelated_size % (1 << 10) == 0)
    printf("unrelated_size: %d\n", p_dag->unrelated_size);
  return 1;
}

void dag_unrelated_squash(dag_t *p_dag) {
  for (int s1 = 0; s1 < p_dag->unrelated_size; s1++) {
    for (int s2 = s1+1; s2 < p_dag->unrelated_size; s2++) {
      if ((p_dag->unrelated[s1] & p_dag->unrelated[s2]) == p_dag->unrelated[s2]) {
        // s2 subset of s1 => delete s2
        p_dag->unrelated[s2] = p_dag->unrelated[--p_dag->unrelated_size];
        // repeat loop body with new replaced s2
        s2--;
      } else if ((p_dag->unrelated[s1] & p_dag->unrelated[s2]) == p_dag->unrelated[s1]) {
        // s1 subset of s2 => delete s1
        p_dag->unrelated[s1] = p_dag->unrelated[--p_dag->unrelated_size];
        // abort inner loop, and repeat outer loop body with new replaced s1
        s1--;
        break;
      }
    }
  }
}

int dag_have_no_relatives(dag_t *p_dag, unsigned long s) {
  for (int i = 0; i < p_dag->num_elems - 1; i++) {
    if (s & (1UL << i)) {
      for (int j = i + 1; j < p_dag->num_elems; j++) {
        if (s & (1UL << j)) {
          // for each elem i and j in set s:
          if (p_dag->relatives[i][j])
            return 0;
        }
      }
    }
  }
  return 1;
}

int dag_have_no_relatives_map(dag_t *p_dag, unsigned long s, short *map, int num_elems) {
  for (int i = 0; i < num_elems - 1; i++) {
    if (s & (1UL << i)) {
      for (int j = i + 1; j < num_elems; j++) {
        if (s & (1UL << j)) {
          // for each elem i and j in set s:
          if (p_dag->relatives[map[i]][map[j]])
            return 0;
        }
      }
    }
  }
  return 1;
}

// use map[] to expand each bit corresponding to the first replicated task,
// with the full bitmak of all the replicas, in each set s within unrelated_sets[]
void dag_unrelated_map(dag_t *p_dag, short *map, int num_elems) {
  for (int i = 0; i < p_dag->unrelated_size; i++) {
    unsigned long s = 0;
    for(int j = 0; j < num_elems; j++) {
      if (p_dag->unrelated[i] & (1UL << j)) {
        dag_elem_t *p_elem = &p_dag->elems[map[j]];
        s |= (1UL << map[j]);
        for (int k = 0; k < p_elem->n_anti_affinity; k++)
          s |= (1UL << p_elem->anti_affinity[k]);
      }
    }
    p_dag->unrelated[i] = s;
  }
}

void dag_comp_unrelated_old(dag_t *p_dag) {
  dag_comp_relatives(p_dag);
  // (1UL << 64) would not be correctly computed
  assert(p_dag->num_elems <= sizeof(p_dag->unrelated[0]) * 8 - 1);
  p_dag->unrelated_max_size = (1UL << 24); // Will 16M subsets suffice?
  if ((1UL << p_dag->num_elems) < p_dag->unrelated_max_size)
    p_dag->unrelated_max_size = (1UL << p_dag->num_elems);
  p_dag->unrelated = malloc(sizeof(p_dag->unrelated[0]) * p_dag->unrelated_max_size);
  check(p_dag->unrelated != NULL, "Could not allocate %lu bytes", sizeof(p_dag->unrelated[0]) * p_dag->unrelated_max_size);
  p_dag->unrelated_size = 0;
  int num_useful_elems = p_dag->num_elems;
  if ((p_dag->elems[p_dag->num_elems - 1].n_next == 0 || p_dag->elems[p_dag->num_elems - 1].n_prev == 0)
      && (p_dag->elems[p_dag->num_elems - 2].n_next == 0 || p_dag->elems[p_dag->num_elems - 2].n_prev == 0)) {
    dag_add_unrelated(p_dag, 1UL << (p_dag->num_elems - 1));
    dag_add_unrelated(p_dag, 1UL << (p_dag->num_elems - 2));
    num_useful_elems -= 2;
  }

  short map[p_dag->num_elems];   // map elems in reduced-size search space to (first replica of) elems in original DAG
  int ee = 0;                    // actual numbering of elems to consider in reduced-size search for unrelated sets
  for (int e = 0; e < num_useful_elems; e++) {
    int is_first_replica = 1;
    for (int i = 0; i < p_dag->elems[e].n_anti_affinity; i++) {
      if (p_dag->elems[e].anti_affinity[i] < e) {
        is_first_replica = 0;
        break;
      }
    }
    if (is_first_replica)
      map[ee++] = e;
  }

  num_useful_elems = ee;

  printf("num_useful_elems: %d, num_elems: %d\n", num_useful_elems, p_dag->num_elems);

  // loop over all non-empty subsets of elems
  #pragma omp parallel for firstprivate(p_dag)
  for (unsigned long s = 1UL; s < (1UL << num_useful_elems); s++) {
    if (s % 10000000UL == 0)
      printf("thread=%d, s=%g%%\n", omp_get_thread_num(), 100.0 * s / (1UL << num_useful_elems));
    if (dag_have_no_relatives_map(p_dag, s, map, num_useful_elems)) {
      #pragma omp critical
      dag_add_unrelated(p_dag, s); // sets s are built using reduced-size task identifiers
    }
  }
  // this works also with reduced-size task identifiers
  dag_unrelated_squash(p_dag);

  // adjust unrelated sets to actual task numbering in original DAG
  dag_unrelated_map(p_dag, map, num_useful_elems);
}

unsigned long dag_elemset_del_reachable(dag_t *p_dag, unsigned long s) {
  for (int e1 = 0; e1 < p_dag->num_elems - 1; e1++)
    if (s & (1UL << e1))
      for (int e2 = e1 + 1; e2 < p_dag->num_elems; e2++)
        if (s & (1UL << e2)) {
          if (dag_is_reachable(p_dag, e1, e2))
            s &= ~(1UL << e2);
          else if (dag_is_reachable(p_dag, e2, e1))
            s &= ~(1UL << e1);
        }
  return s;
}

void dag_comp_unrelated_recur(dag_t *p_dag, unsigned long curr_cut) {
  LOG(DEBUG, "curr_cut = %lu\n", curr_cut);
  for (int i = 0; i < p_dag->num_elems; i++) {
    if (curr_cut & (1UL << i)) {
      unsigned long cut = curr_cut & (~(1UL << i));
      // TODO: consider all subsets in i->next[] with no relationships
      // add them to cut, then recur.
      dag_elem_t *p_elem = &p_dag->elems[i];
      if (p_elem->n_next == 0)
        continue;

      unsigned long ns = 0;
      foreach_elem_next(p_dag, i)
        ns |= (1UL << n);

      unsigned long cut2 = cut | ns;
      unsigned long cut2_new = dag_elemset_del_reachable(p_dag, cut2);

      LOG(DEBUG, "cut2 = %lu, cut2_new=%lu\n", cut2, cut2_new);

      if (cut2_new != 0) {
        assert(dag_have_no_relatives(p_dag, cut2_new));
        dag_add_unrelated(p_dag, cut2_new);
      }
      // TODO: keep going with original cut2, or reduced cut2_new?
      dag_comp_unrelated_recur(p_dag, cut2_new);

      /* // loop with s over all subsets of elems[i]->next[] */
      /* for (int s = 1; s < (1 << p_elem->n_next); s++) { */
      /*   // from the subset of next indexes s, build the actual subset of next tasks ns */
      /*   unsigned long ns = 0; */
      /*   for (int j = 0; j < p_elem->n_next; j++) */
      /*     if (s & (1 << j)) */
      /*       ns |= (1UL << p_elem->next[j]); */
      /*   if (dag_have_no_relatives(p_dag, ns)) { */
      /*     unsigned long cut2 = (cut | ns); */
      /*     if (dag_have_no_relatives(p_dag, cut2)) { */
      /*       dag_add_unrelated(p_dag, cut2); */
      /*       //printf("after adding %lx, unrelated[]: ", cut2); */
      /*       //dag_dump_unrelated(p_dag); */
      /*       dag_comp_unrelated_recur(p_dag, cut2); */
      /*     } */
      /*   } */
      /* } */
    }
  }
}

void dag_comp_unrelated_new(dag_t *p_dag) {
  dag_comp_relatives(p_dag);

  assert(p_dag->num_elems <= sizeof(p_dag->unrelated[0]) * 8 - 1);
  p_dag->unrelated_max_size = (1UL << 24); // Will 16M subsets suffice?
  if ((1UL << p_dag->num_elems) < p_dag->unrelated_max_size)
    p_dag->unrelated_max_size = (1UL << p_dag->num_elems);
  p_dag->unrelated = malloc(sizeof(p_dag->unrelated[0]) * p_dag->unrelated_max_size);
  check(p_dag->unrelated != NULL, "Could not allocate %lu bytes", sizeof(p_dag->unrelated[0]) * p_dag->unrelated_max_size);
  p_dag->unrelated_size = 0;

  unsigned long curr_cut = (1UL << p_dag->first);
  dag_add_unrelated(p_dag, curr_cut);
  dag_comp_unrelated_recur(p_dag, curr_cut);

  dag_unrelated_squash(p_dag);
}

void dag_comp_unrelated(dag_t *p_dag) {
  if (new_comp_unrelated)
    dag_comp_unrelated_new(p_dag);
  else
    dag_comp_unrelated_old(p_dag);
}

void sys_load_unrelated(sys_t *p_sys, char *fname) {
  FILE *f = fopen(fname, "r");
  while (!feof(f)) {
    int d;
    if (fscanf(f, "Unrelated for DAG %d:", &d) != 1)
      break;
    check(d < p_sys->n_dags, "Wrong DAG n.%d (n_dag=%d)", d, p_sys->n_dags);
    printf("DAG n.%d\n", d);
    dag_t *p_dag = p_sys->dags[d];
    if (p_dag->unrelated == NULL) {
      int max_unrel_elems = p_dag->num_elems;
      if (max_unrel_elems > 20)
        max_unrel_elems = 20;
      p_dag->unrelated = malloc(sizeof(long)*(1UL << max_unrel_elems));
      check(p_dag->unrelated != NULL, "Could not allocate %lu bytes", sizeof(long)*(1UL << max_unrel_elems));
    }
    p_dag->unrelated_size = 0;
    int ch;
    do {
      unsigned long s = 0;
      check(fscanf(f, " { ") != EOF, "Expecting ' { ' while parsing %s", fname);
      do {
        int val;
        check(fscanf(f, "%d", &val) == 1, "Expecting integer while parsing %s", fname);
        s |= (1UL << val);
        ch = fgetc(f);
      } while (ch == ',');
      check(ch == ' ', "Expecting space while parsing %s", fname);
      check(fscanf(f, "}") != EOF, "Expecting '}' while parsing %s", fname);
      p_dag->unrelated[p_dag->unrelated_size++] = s;
      ch = fgetc(f);
    } while (ch == ',');
    check(ch == '\n', "Could not find newline");
    printf("Parsed %d unrelated sets\n", p_dag->unrelated_size);
  }
  fclose(f);
}

void dag_dump_unrelated(dag_t *p_dag) {
  for (int i = 0; i < p_dag->unrelated_size; i++) {
    printf("{ ");
    int k = 0;
    for (int j = 0; j < p_dag->num_elems; j++)
      if (p_dag->unrelated[i] & (1UL<<j))
        printf("%s%d", k++ > 0 ? ", " : "", j);
    printf(" }%s", i < p_dag->unrelated_size - 1 ? ", " : "");
  }
  printf("\n");
}

double dag_elem_wcet_scaled(sys_t *p_sys, dag_t *p_dag, int i, island_t *p_isl, int m) {
  int s = p_isl - &p_sys->islands[0];
  //   printf("s=%d, p_isl->type=%d\n", s, p_isl->type);
  assert(s >= 0 && s < MAX_ISLANDS);
  assert(m >= 0 && m < p_isl->n_freqs);
  double C_s = C_NONE, C_ns = C_NONE;
  if (p_dag->elems[i].isl_freq_C[s][m] != C_NONE)
    // trust the customized per-island and per-frequency WCET
    return p_dag->elems[i].isl_freq_C[s][m];
  if (p_dag->elems[i].isl_C[s] != C_NONE) {
    // trust the customized WCET and ignore the island capacity
    C_ns = p_dag->elems[i].isl_C_ns[s];
    C_s = p_dag->elems[i].isl_C[s] - C_ns;
  } else {
    // use island capacity
    C_ns = p_dag->elems[i].C_ns;
    C_s = (p_dag->elems[i].C - C_ns) / p_isl->capacity;
  }
  check(p_dag->elems[i].fictitious || (C_s != C_NONE && C_ns != C_NONE), "WCET values not initialized, asking WCET for task %d at frequency %d on island %d", i, m, s);
  return C_ns + C_s * p_isl->freqs[0] / p_isl->freqs[m];
}

double dag_elem_wcet_unscaled(sys_t *p_sys, dag_t *p_dag, int e) {
  if (p_dag->elems[e].C != C_NONE)
    return p_dag->elems[e].C;
  int s = p_dag->elems[e].opt_island;
  if (s != -1) {
    int m = p_sys->islands[s].opt_freq;
    if (m != -1)
      return p_dag->elems[e].isl_freq_C[s][m] * p_sys->islands[s].freqs[m] / p_sys->islands[s].freqs[0] * p_sys->islands[s].capacity;
    assert(p_dag->elems[e].isl_C[s] != C_NONE && p_dag->elems[e].isl_C_ns[s] != C_NONE);
    return p_dag->elems[e].isl_C_ns[s] + (p_dag->elems[e].isl_C[s] - p_dag->elems[e].isl_C_ns[s]) / p_sys->islands[s].capacity;
  } else {
    // looking for the CPU island with capacity 1
    for (int s = 0; s < p_sys->n_islands; ++s) {
      if (p_sys->islands[s].capacity == 1.0 && p_dag->elems[e].isl_freq_C[s][0] != C_NONE) {
        return p_dag->elems[e].isl_freq_C[s][0];
      }
    }
  }
  check(0, "Unexpected state when calling dag_elem_unscaled_wcet()");
  return 0.0;
}

double dag_elem_avg_exec_time_scaled(sys_t *p_sys, dag_t *p_dag, int i, island_t *p_isl, int m) {
  int s = p_isl - &p_sys->islands[0];
  assert(s >= 0 && s < MAX_ISLANDS);
  assert(m >= 0 && m < p_isl->n_freqs);

  if (p_dag->elems[i].isl_freq_avg_C[s][m] != C_NONE) {
    // trust the customized per-island and per-frequency average exec time
    return p_dag->elems[i].isl_freq_avg_C[s][m];
  }

  return dag_elem_wcet_scaled(p_sys, p_dag, i, p_isl, m);
}

double dag_elem_pow_diff(sys_t *p_sys, dag_t *p_dag, int i, island_t *p_isl, int m) {
  int s = p_isl - &p_sys->islands[0];
  assert(s >= 0 && s < MAX_ISLANDS);
  assert(m >= 0 && m < p_isl->n_freqs);

  double pow_busy = p_isl->pows_busy[m];
  if (p_dag->elems[i].isl_freq_pow_diff[s][m] != POW_NONE) {
    // trust the customized per-island and per-frequency power diff
    pow_busy = p_dag->elems[i].isl_freq_pow_diff[s][m];
  }

  // use the power model
  double pow_diff = pow_busy - p_isl->pows_idle[m];

  //check(pow_diff > 0, "Negative diff");

  return pow_diff;
}

// min WCET among configs, realized at max frequency, i.e., first frequency in island
double dag_elem_wcet_scaled_min(sys_t *p_sys, dag_t *p_dag, int i) {
  double min_wcet = FLT_MAX;
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];

    if (p_isl->type == ACCELERATOR && !(p_dag->elems[i].accelerators & (1<<s))) {
      continue;
    }

    double wcet = dag_elem_wcet_scaled(p_sys, p_dag, i, p_isl, 0);
    if (wcet < min_wcet)
      min_wcet = wcet;
  }
  return min_wcet;
}

// max WCET among configs, realized at min frequency, i.e., last frequency in island
double dag_elem_wcet_scaled_max(sys_t *p_sys, dag_t *p_dag, int i) {
  double max_wcet = 0;
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    double wcet = dag_elem_wcet_scaled(p_sys, p_dag, i, p_isl, p_isl->n_freqs - 1);
    if (wcet > max_wcet)
      max_wcet = wcet;
  }
  return max_wcet;
}

double dag_load(dag_t *p_dag) {
  double u = 0.0;
  double den = p_dag->deadline < p_dag->period ? p_dag->deadline : p_dag->period;
  for (int e = 0; e < p_dag->num_elems; e++) {
    dag_elem_t *p_elem = &p_dag->elems[e];
    u += p_elem->C / den;
  }
  return u;
}

double sys_load(sys_t *p_sys) {
  double u = 0.0;
  for (int i = 0; i < p_sys->n_dags; i++) {
    dag_t *p_dag = p_sys->dags[i];
    u += dag_load(p_dag);
  }
  return u;
}

// TODO: check deadline constraints considering also elems placed on accelerators
int dag_check_deadlines(dag_t *p_dag, int s, double partial, int check_wcets) {
  if (p_dag->elems[s].d == -1)
    return 0;
  if (check_wcets) {
    double wcet = dag_elem_wcet_or_scaled(p_dag, p_sys, s);
    if (p_dag->elems[s].d < wcet) {
      LOG(DEBUG, "Found deadline of elem %d %lf lower than scaled wcet %lf",
          s, p_dag->elems[s].d, wcet);
      return 0;
    }
  }
  partial += p_dag->elems[s].d;
  if (s == p_dag->last) {
    if (partial > p_dag->deadline) {
      LOG(DEBUG, "End-to-end deadline violated: partial=%lf, deadline=%lf", partial, p_dag->deadline);
      return 0;
    }
  }
  foreach_elem_next(p_dag, s)
    if (!dag_check_deadlines(p_dag, n, partial, check_wcets))
      return 0;
  return 1;
}

void dag_dump(dag_t *p_dag) {
  printf("DAG deadline: %f, period: %f, num_elems: %d\n", p_dag->deadline, p_dag->period, p_dag->num_elems);
  for (int e = 0; e < p_dag->num_elems; e++) {
    dag_elem_t *p_elem = &p_dag->elems[e];
    printf("elem %d: (%f, %f) / %f", e, p_elem->C, p_elem->C_ns, p_elem->d);
    // dont print hw platform data into a function for DAGs
    /*
    if (p_elem->opt_island != -1) {
      island_t *p_isl = &p_sys->islands[p_elem->opt_island];
      printf(" [%.2f] -> (%d,%d)",
             dag_elem_wcet_scaled(p_dag, e, p_isl, p_isl->opt_freq) / p_elem->d,
             p_elem->opt_island, p_elem->opt_punit);
    }
    */
    printf(", prev(");
    int k = 0;
    foreach_elem_prev(p_dag, e)
      printf("%s%d", k++ > 0 ? ", " : "", p);
    printf("), next(");
    k = 0;
    foreach_elem_next(p_dag, e)
      printf("%s%d", k++ > 0 ? ", " : "", n);
    if (p_dag->elems[e].longest_len != -1) {
      printf("), longest_len(%lf), longest_path(", p_dag->elems[e].longest_len);
      foreach_longest_next(p_dag, e, n)
        printf("%s%d", n == e ? "" : ", ", n);
    }
    printf("), aaf(");
    for (int i = 0; i < p_elem->n_anti_affinity; i++)
      printf("%s%d", i == 0 ? "" : ", ", p_elem->anti_affinity[i]);
    printf(")\n");
  }

  printf("\nSuccessors:\n");
  printf("   ");
  for (int b = 0; b < dag_num_elems(p_dag); b++)
    printf("%2d ", b);
  printf("\n");
  for (int a = 0; a < dag_num_elems(p_dag); a++) {
    printf("%2d ", a);
    for (int b = 0; b < dag_num_elems(p_dag); b++) {
      if (dag_is_successor(p_dag, a, b))
        printf(" 1 ");
      else
        printf(" 0 ");
    }
    printf("\n");
  }

  printf("\nReachability:\n");
  printf("   ");
  for (int b = 0; b < dag_num_elems(p_dag); b++)
    printf("%2d ", b);
  printf("\n");
  for (int a = 0; a < dag_num_elems(p_dag); a++) {
    printf("%2d ", a);
    for (int b = 0; b < dag_num_elems(p_dag); b++) {
      if (dag_is_reachable(p_dag, a, b) || dag_is_reachable(p_dag, b, a))
        printf(" 1 ");
      else
        printf(" 0 ");
    }
    printf("\n");
  }
}

void dag_dump_dot(dag_t *p_dag, char *fname) {
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  fprintf(f, "digraph G {\n  rankdir=LR;\n");
  for (int e = 0; e < p_dag->num_elems; e++) {
    fprintf(f, "  n%d [ label=\"n%d (%g,%g) / %g -> (%d,%d)\" ];\n", e, e,
            p_dag->elems[e].C, p_dag->elems[e].C_ns, p_dag->elems[e].d,
            p_dag->elems[e].opt_island, p_dag->elems[e].opt_punit);
    foreach_elem_next(p_dag, e)
      //TODO: use dag_longest_path to get the dag critical path and mark the dot edge with 'color="red"' attribute
      fprintf(f, "  n%d -> n%d\n", e, n);
  }
  fprintf(f, "}\n");
  fclose(f);
}

void dag_dump_dot2(dag_t *p_dag, double C_sum, char *fname, dag_path_t *p_path) {
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  // fprintf(f, "digraph G {\n  rankdir=LR;\n");
  fprintf(f, "digraph G {\n");
  fprintf(f, "  graph [ critical_path_length=%d, deadline=%g, period=%g]\n", (int)C_sum, p_dag->deadline, p_dag->period);
  for (int e = 0; e < p_dag->num_elems; e++) {
    fprintf(f, "  %d [ label=%d, C=%g, C_ns=%g ];\n", e, e,
            p_dag->elems[e].C, p_dag->elems[e].C_ns);
  }
  int critical_idx = 0;
  for (int e = 0; e < p_dag->num_elems; e++) {
    foreach_elem_next(p_dag, e){
      //TODO: when communication is mapped into this dot file, write down the # of bytes as 'label' attribute
      if (critical_idx <= p_path->n_elems-2 && // if it is not the end of the critical path
        p_path->elems[critical_idx] == e &&    // if the source node match w the one in the critical path
        n == p_path->elems[critical_idx+1]){   // if the target node match w the one in the critical path
        //fprintf(f, "  %d -> %d [color=black, label=%d];\n", e, n, bytes);
        //printf ("%d, %d, %d, %d, %d, %d, %d\n", p_path->n_elems, p_dag->num_elems,  critical_idx, p_path->elems[critical_idx], p_path->elems[critical_idx+1], e, n );
        fprintf(f, "  %d -> %d [color=red];\n", e, n);
        critical_idx ++;
      }else{
        fprintf(f, "  %d -> %d [color=black];\n", e, n);
      }

    }
  }
  fprintf(f, "}\n");
  fclose(f);
}

void sys_dump_dags_yaml(sys_t *p_sys, char *fname) {
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  dag_t *p_dag;
  fprintf(f, "dags:\n");
  for (int i = 0; i < p_sys->n_dags; i++) {
    p_dag = p_sys->dags[i];
    fprintf(f, "  - activation_period: %f\n", p_dag->period);
    fprintf(f, "    deadline: %f\n",p_dag->deadline);
    fprintf(f, "    tasks:\n");
    for (int e = 0; e < p_dag->num_elems; e++) {
        fprintf(f, "      - wcet_ref: %f\n", dag_elem_wcet_unscaled(p_sys, p_dag, e));
        fprintf(f, "        wcet_ref_ns: %f\n", p_dag->elems[e].C_ns);
        fprintf(f, "      - wcet_scaled: %f\n", dag_elem_wcet_or_scaled(p_dag, p_sys, e));
    }
    fprintf(f, "    edge_list:\n");
    fprintf(f, "     [\n");
    for (int e = 0; e < p_dag->num_elems; e++) {
        foreach_elem_next(p_dag, e)
        fprintf(f, "      [%d,%d],\n",e,n);
    }
    fprintf(f, "     ]\n");
  }
  fclose(f);
}

// Return the unique punit identifier given its island and punit p relative to the island
int sys_get_punit_id(sys_t *p_sys, island_t *p_island, int p) {
  int cpu_id = 0;
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    if (p_island == p_isl)
      return cpu_id + p;
    cpu_id += p_isl->n_punits;
  }
  assert(0);
}

#define RTSIM_WL_BUSY "bzip2"
#define RTSIM_WL_IDLE "idle"

void sys_dump_dags_yaml_rtsim(sys_t *p_sys, char *fname) {
  printf("Dumping DAGs to RTSim YAML file: %s\n", fname);
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  dag_t *p_dag;
  fprintf(f, "taskset:\n");
  for (int d = 0; d < p_sys->n_dags; d++) {
    p_dag = p_sys->dags[d];
    for (int e = 0; e < p_dag->num_elems; e++) {
      fprintf(f, "  - name: task_%d_%d\n", d, e);
      // All tasks belonging to the same DAG have the same period == DAG period
      fprintf(f, "    iat: %f\n", p_dag->period);
      island_t *p_isl = &p_sys->islands[p_dag->elems[e].opt_island];
      double wcet = dag_elem_wcet_scaled(p_sys, p_dag, e, p_isl, p_isl->opt_freq);
      if (p_isl->type == CPU) {
        fprintf(f, "    cbs_runtime: %f\n", wcet);
        fprintf(f, "    cbs_period: %f\n", p_dag->elems[e].d);
      }
      fprintf(f, "    rdl: %f\n", p_dag->deadline);
      fprintf(f, "    startcpu: %d\n", sys_get_punit_id(p_sys, p_isl, p_dag->elems[e].opt_punit));
      fprintf(f, "    code:\n");
      if (p_dag->elems[e].n_prev == 0)
        // first DAG in task: this prevents two concurrent activations in case of late completion
        fprintf(f, "      - lock(L_DAG_%d)\n", d);
      //
      foreach_elem_prev(p_dag, e) {
        fprintf(f, "      - lock(L_TSK_%d_%d_%d)\n", d, p, e);
      }
      fprintf(f, "      - fixed(%f," RTSIM_WL_BUSY ")\n", wcet);
      foreach_elem_next(p_dag, e) {
        fprintf(f, "      - unlock(L_TSK_%d_%d_%d)\n", d, e, n);
      }
      //
      if (p_dag->elems[e].n_next == 0)
        fprintf(f, "      - unlock(L_DAG_%d)\n", d);
    }
  }
  fprintf(f, "resources:\n");
  for (int d = 0; d < p_sys->n_dags; d++) {
    p_dag = p_sys->dags[d];
    fprintf(f, "  - name: L_DAG_%d\n", d);
    fprintf(f, "    initial_state: unlocked\n");
    for (int e = 0; e < p_dag->num_elems; e++) {
      foreach_elem_next(p_dag, e) {
        fprintf(f, "  - name: L_TSK_%d_%d_%d\n", d, e, n);
        fprintf(f, "    initial_state: locked\n");
      }
    }
  }
  fclose(f);
}

void sys_dump_isl_yaml_rtsim(sys_t *p_sys, char *fname) {
  printf("Dumping islands to RTSim YAML file: %s\n", fname);
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  fprintf(f, "cpu_islands:\n");
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    fprintf(f, "  - name: island%d\n", s);
    fprintf(f, "    numcpus: %d\n", p_isl->n_punits);
    fprintf(f, "    kernel:\n");
    fprintf(f, "      scheduler: %s\n", p_isl->type == CPU ? "edf" : "fifo");
    fprintf(f, "      task_placement: partitioned\n");
    fprintf(f, "    volts:\n");
    fprintf(f, "      [");
    for (int m = p_isl->n_freqs - 1; m >= 0; --m)
      fprintf(f, "%f%s", 1.0, m > 0 ? "," : "");
    fprintf(f, "]\n");
    fprintf(f, "    freqs:\n");
    fprintf(f, "      [");
    for (int m = p_isl->n_freqs - 1; m >= 0; --m)
      fprintf(f, "%d%s", (int)p_isl->freqs[m], m > 0 ? "," : "");
    fprintf(f, "]\n");
    fprintf(f, "    base_freq: %d\n", (int)p_isl->freqs[p_isl->opt_freq]);
    fprintf(f, "    power_model: island%d_psm\n", s);
    fprintf(f, "    speed_model: island%d_psm\n", s);
  }
  static char psm_fname[256];
  fprintf(f, "power_models:\n");
  int num_elems = snprintf(psm_fname, sizeof(psm_fname), "%s_islands_psm.csv", fname);
  assert(num_elems < sizeof(psm_fname));
  for (int s = 0; s < p_sys->n_islands; s++) {
    fprintf(f, "  - name: island%d_psm\n", s);
    fprintf(f, "    type: table_based\n");
    fprintf(f, "    filename: %s\n", psm_fname);
  }
  fclose(f);
  printf("Dumping power-speed model to RTSim CSV file: %s\n", psm_fname);
  f = fopen(psm_fname, "w");
  assert(f != NULL);
  fprintf(f, "model,freq,workload,power,speed\n");
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    for (int m = 0; m < p_isl->n_freqs; m++) {
      fprintf(f, "island%d_psm,%d," RTSIM_WL_BUSY ",%f,%f\n", s, (int)p_isl->freqs[m],
              p_isl->pows_busy[m],
              p_isl->freqs[m] * p_isl->capacity / p_isl->freqs[0]);
    }
    for (int m = 0; m < p_isl->n_freqs; m++) {
      fprintf(f, "island%d_psm,%d," RTSIM_WL_IDLE ",%f,%f\n", s, (int)p_isl->freqs[m],
              p_isl->pows_idle[m],
              p_isl->freqs[m] * p_isl->capacity / p_isl->freqs[0]);
    }
  }
  fclose(f);
}

/* example:
   -d 100,100 \
      -t 0,0 \
      -t 80,3 \
      -t 0,0 \
      -n 0,1 \
      -n 1,2 \
*/

void dag_dump_args(dag_t *p_dag, FILE *f) {
  fprintf(f, "    -d %f,%f \\\n",p_dag->deadline, p_dag->period);
  for (int e = 0; e < p_dag->num_elems; e++) {
    fprintf(f,"      -t %f,%f \\\n", p_dag->elems[e].C, p_dag->elems[e].C_ns);
  }
  for (int e = 0; e < p_dag->num_elems; e++) {
    foreach_elem_next(p_dag, e)
      fprintf(f, "      -n %d,%d \\\n", e, n);
  }
}

void sys_dump_args(sys_t *p_sys, char *fname) {
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  dag_t *p_dag;
  fprintf(f, "#!/bin/bash\n\n");
  fprintf(f, "../dag/dag \\\n");
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    if (p_isl->type == CPU) {
      fprintf(f, "    -i %d,%f \\\n", p_isl->n_punits, p_isl->capacity);
    } else if (p_isl->type == ACCELERATOR) {
      if (p_isl->cs_delay == 0.0)
        fprintf(f, "    -ai %d,%f \\\n", p_isl->n_punits, p_isl->capacity);
      else
        fprintf(f, "    -ai %d,%f,%f \\\n", p_isl->n_punits, p_isl->capacity, p_isl->cs_delay);
    } else {
      fprintf(stderr, "Error: unable to dump island type: %u!\n", p_isl->type);
      exit(1);
    }
  }
  for (int i = 0; i < p_sys->n_dags; i++) {
    p_dag = p_sys->dags[i];
    dag_dump_args(p_dag, f);
  }
  fprintf(f, "\"$@\"\n");
  fclose(f);
}

void sys_dump_dot_placed(sys_t *p_sys, char *fname) {
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  fprintf(f, "digraph G {\n");
  fprintf(f, "  rankdir=LR;\n");
  fprintf(f, "  label=\"Power=%fW\"\n", p_sys->opt_pow);

  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    for (int e = 0; e < p_dag->num_elems; e++) {
      dag_elem_t *p_elem = &p_dag->elems[e];
      if (p_elem->opt_island == -1) {
        if (p_dag->elems[e].C_ns > 0)
          fprintf(f, "    n_%d_%d [ label=\"n%d,%d",
                  d, e, d, e);
        else
          fprintf(f, "    n_%d_%d [ label=\"n%d,%d",
                  d, e, d, e);
        if (e == p_dag->last)
          fprintf(f, "\nE2E deadline=%lg", p_dag->deadline);
        fprintf(f, "\", color=%s, penwidth=%d];\n", node_colors[d], nodes_width);
      }
    }
  }

  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    fprintf(f, "  subgraph cluster_island_%d {\n", s);
    char *isl_type = p_isl->type == ACCELERATOR ? "GPU" : "CPU";

    fprintf(f, "    label=\"%s (island %d) [freq:%.2fHz]\";\n", isl_type, s, p_isl->freqs[p_isl->opt_freq]);
    for (int p = 0; p < p_isl->n_punits; p++) {
      fprintf(f, "    subgraph cluster_pu_%d {\n", p);
      fprintf(f, "      label=\"PU %d [U:%.2f]\";\n", p, punit_tot_util(p_sys, s, p));
      fprintf(f, "      bgcolor=%s;\n", pu_color);

      // drawing the non-omp tasks
      for (int d = 0; d < p_sys->n_dags; d++) {
        dag_t *p_dag = p_sys->dags[d];
        for (int e = 0; e < p_dag->num_elems; e++) {
          dag_elem_t *p_elem = &p_dag->elems[e];
          if (p_dag->elems[e].omp_env_id != -1) {
            continue;
          }

          if (p_elem->opt_island == s && p_elem->opt_punit == p) {
            island_t *p_isl = &p_sys->islands[p_elem->opt_island];

            if (p_dag->elems[e].C_ns > 0) {
              fprintf(f, "      n_%d_%d [ label=\"n%d,%d (%f,%f) / %f [%.2f]\", color=%s ];\n",
                      d, e, d, e,
                      p_dag->elems[e].C, p_dag->elems[e].C_ns, p_dag->elems[e].d,
                      dag_elem_wcet_scaled(p_sys, p_dag, e, p_isl, p_isl->opt_freq) / p_dag->elems[e].d,
                      node_colors[d]);
            } else {
              fprintf(f, "      n_%d_%d [ label=\"n%d,%d C:%.2fus \\n D:%.2fus [bw:%.2f]\", color=%s, penwidth=%d ];\n",
                      d, e, d, e,
                      dag_elem_wcet_scaled(p_sys, p_dag, e, p_isl, p_isl->opt_freq) / 1000, p_dag->elems[e].d / 1000,
                      dag_elem_wcet_scaled(p_sys, p_dag, e, p_isl, p_isl->opt_freq) / p_dag->elems[e].d,
                      node_colors[d],
                      nodes_width);
            }
          }
        }
      }

      // drawing the omp tasks
      for (int o = 0; o < p_sys->n_omp_envs; ++o) {
        fprintf(f, "      subgraph cluster_omp_wt_pu_%d_env_%d {\n", p, o);
        fprintf(f, "        label=\"OpenMP env %d -> runtime: %.2fus period: %.2fus\";\n", o, p_sys->omp_envs[o].worker_threads[s][p].opt_budget / 1000, p_sys->omp_envs[o].worker_threads[s][p].opt_period / 1000);
        fprintf(f, "        bgcolor=%s;\n", omp_env_colors[o]);
        for (int d = 0; d < p_sys->n_dags; d++) {
          dag_t *p_dag = p_sys->dags[d];
          for (int e = 0; e < p_dag->num_elems; e++) {
            dag_elem_t *p_elem = &p_dag->elems[e];
            if (p_dag->elems[e].omp_env_id != o) {
              continue;
            }

            if (p_elem->opt_island == s && p_elem->opt_punit == p) {
              island_t *p_isl = &p_sys->islands[p_elem->opt_island];
              fprintf(f, "      n_%d_%d [ label=\"n%d,%d C:%.2fus \\n D:%.2fus [bw:%.2f]\", color=%s, penwidth=%d ];\n",
                      d, e, d, e,
                      dag_elem_wcet_scaled(p_sys, p_dag, e, p_isl, p_isl->opt_freq) / 1000, p_dag->elems[e].d / 1000,
                      dag_elem_wcet_scaled(p_sys, p_dag, e, p_isl, p_isl->opt_freq) / p_dag->elems[e].d,
                      node_colors[d],
                      nodes_width);
            }
          }
        }

        fprintf(f, "      }\n");
      }
      fprintf(f, "    }\n");
    }
    fprintf(f, "  }\n");
  }
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    for (int e = 0; e < p_dag->num_elems; e++) {
      foreach_elem_next(p_dag, e)
        fprintf(f, "  n_%d_%d -> n_%d_%d\n", d, e, d, n);
    }
  }
  fprintf(f, "}\n");
  fclose(f);
}

int to_hide(dag_t *p_dag, int e) {
  if (!dot_hide_aaf)
    return 0;
  dag_elem_t *p_elem = &p_dag->elems[e];
  if (p_elem->n_anti_affinity == 0)
    return 0;
  int first = 1;
  for (int i = 0; i < p_elem->n_anti_affinity; i++) {
    //printf("d=%d, e=%d, [%d]=%d\n", d, e, i, p_elem->anti_affinity[i]);
    if (e > p_elem->anti_affinity[i]) {
      first = 0;
      break;
    }
  }
  return (!first);
}

void sys_dump_dot_input_dag(sys_t *p_sys, char *fname) {
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  fprintf(f, "digraph G {\n");
  fprintf(f, "  rankdir=LR;\n");

  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];

    fprintf(f, "  subgraph cluster_dag_%d {\n", d);
    fprintf(f, "    label=\"DAG %d - E2E deadline=%.2f ms\";\n", d, p_dag->deadline / 1000000);

    for (int e = 0; e < p_dag->num_elems; e++) {
      dag_elem_t *p_elem = &p_dag->elems[e];
      if (p_elem->opt_island == -1) {
        if (to_hide(p_dag, e))
          continue;
        fprintf(f, "    n_%d_%d [ label=\"n%d,%d", d, e, d, e);
        fprintf(f, "\", color=%s, penwidth=%d", node_colors[d], nodes_width);
        if (p_elem->n_anti_affinity > 0)
          fprintf(f, ", shape=doubleoctagon");
        fprintf(f, "];\n");
      }
    }
    fprintf(f, "  }\n");
  }

  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    for (int e = 0; e < p_dag->num_elems; e++) {
      foreach_elem_next(p_dag, e) {
        if (to_hide(p_dag, e) || to_hide(p_dag, n))
          continue;
        fprintf(f, "  n_%d_%d -> n_%d_%d\n", d, e, d, n);
      }
    }
  }

  fprintf(f, "}\n");
  fclose(f);
}

void sys_dump_bbsearch(sys_t *p_sys, char *fname) {
    printf("\n\nCOMPACT OUTPUT FORMAT:\n");
    assert(p_sys->opt_pow!=0);
    printf("Power: %f W\n",p_sys->opt_pow);

    // print tasks not placed !?!?
    for (int d = 0; d < p_sys->n_dags; d++) {
        dag_t *p_dag = p_sys->dags[d];
        for (int e = 0; e < p_dag->num_elems; e++) {
            dag_elem_t *p_elem = &p_dag->elems[e];
            if (p_elem->opt_island == -1) {
                printf("WARNING: task_%d_%d (%f,%f) was not placed!\n",
                    d, e, p_dag->elems[e].C, p_dag->elems[e].C_ns);
            }
        }
    }

    int island_mapping[MAX_TASKS_PER_DAG];
    int task_idx=0;
    // this is necessary because the elements are not ordered
    for (int d = 0; d < p_sys->n_dags; d++) {
        dag_t *p_dag = p_sys->dags[d];
        for (int e = 0; e < p_dag->num_elems; e++) {
            dag_elem_t *p_elem = &p_dag->elems[e];
            if (p_elem->C == 0)
              continue;
            island_mapping[task_idx] = p_elem->opt_island;
            ++task_idx;
        }
    }

    for (int s = 0; s < p_sys->n_islands; s++) {
        island_t *p_isl = &p_sys->islands[s];
        printf("Island %d (%.2f Hz): ", s, p_isl->freqs[p_isl->opt_freq]);
        for (int t = task_idx-1; t >= 0; --t) {
            if (s == island_mapping[t]){
                printf("1,");
            }else{
                printf("0,");
            }
        }
        printf("\n");
    }
}

double sys_tot_pow(sys_t *p_sys) {
  double pow = 0.0;
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    // pows_idle[] are per-island, all punits included
    pow += p_isl->pows_idle[p_isl->opt_freq];
  }
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    for (int e = 0; e < p_dag->num_elems; e++) {
      dag_elem_t *p_elem = &p_dag->elems[e];
      island_t *p_isl = &p_sys->islands[p_elem->opt_island];
      // same for CPU and ACCEL: each task is activated once per DAG period
      double wcet = dag_elem_wcet_scaled(p_sys, p_dag, e, p_isl, p_isl->opt_freq);
      pow += wcet * pow_exec_times_scale * (p_isl->pows_busy[p_isl->opt_freq] - p_isl->pows_idle[p_isl->opt_freq]) / p_dag->period;
    }
  }
  return pow;
}

// output binary variables for DAG d: x_dag_task_island_punit
void dump_lp_dag_bin(sys_t *p_sys, int d, FILE *f) {
  dag_t *p_dag = p_sys->dags[d];
  for (int i = 0; i < p_dag->num_elems; i++) {
    for (int s = 0; s < p_sys->n_islands; s++) {
      island_t *p_isl = &p_sys->islands[s];
      for (int p = 0; p < p_isl->n_punits; p++) {
        fprintf(f, " x_%d_%d_%d_%d\n", d, i, s, p);
        for (int m = 0; m < p_isl->n_freqs; m++) {
          fprintf(f, " z_%d_%d_%d_%d_%d\n", d, i, s, p, m);
        }
      }
    }
  }
}

// output constraints for DAG d
void dump_lp_dag_constr(sys_t *p_sys, int d, FILE *f) {
  dag_t *p_dag = p_sys->dags[d];
  for (int i = 0; i < p_dag->num_elems; i++) {
    if (p_dag->elems[i].omp_env_id == -1) {
      fprintf(f, " [ d_%d_%d * di_%d_%d ] = 1\n", d, i, d, i);
    }
  }
//    if (p_dag->elems[i].C > 0)
//      fprintf(f, " [ d_%d_%d * di_%d_%d ] = 1\n", d, i, d, i);
//    else
//      fprintf(f, " d_%d_%d = 0\n", d, i);

  for (int i = 0; i < p_dag->num_elems; i++) {
    for (int s = 0; s < p_sys->n_islands; s++) {
      island_t *p_isl = &p_sys->islands[s];
      for (int p = 0; p < p_isl->n_punits; p++)
        fprintf(f, " + x_%d_%d_%d_%d", d, i, s, p);
    }
    fprintf(f, " = 1\n");
  }

  // generating the anti-affinity constraints
  for (int i = 0; i < p_dag->num_elems; i++) {
    dag_elem_t *p_elem = &p_dag->elems[i];
    for (int j = 0; j < p_elem->n_anti_affinity; ++j) {
      int replica_id = p_elem->anti_affinity[j];
      for (int s = 0; s < p_sys->n_islands; ++s) {
        island_t *p_isl = &p_sys->islands[s];
        for (int p = 0; p < p_isl->n_punits; ++p) {
          fprintf(f, " x_%d_%d_%d_%d + x_%d_%d_%d_%d <= 1\n", d, i, s, p, d, replica_id, s, p);
        }
      }
    }
  }

  fprintf(f, " f_%d_%d - d_%d_%d = 0\n", d, p_dag->first, d, p_dag->first);
  for (int i = 0; i < p_dag->num_elems; i++) {
    if (i == p_dag->first)
      continue;
    foreach_elem_prev(p_dag, i) {
      fprintf(f, " f_%d_%d - f_%d_%d - d_%d_%d >= 0\n", d, i, d, p, d, i);
    }
  }
  fprintf(f, " %g f_%d_%d <= 1\n", lp_scale / p_dag->deadline, d, p_dag->last);

  for (int i = 0; i < p_dag->num_elems; i++) {
    for (int s = 0; s < p_sys->n_islands; s++) {
      island_t *p_isl = &p_sys->islands[s];
      for (int m = 0; m < p_isl->n_freqs; m++) {
        for (int p = 0; p < p_isl->n_punits; p++) {
          fprintf(f, " z_%d_%d_%d_%d_%d - x_%d_%d_%d_%d <= 0\n", d, i, s, p, m, d, i, s, p);
          fprintf(f, " z_%d_%d_%d_%d_%d - y_%d_%d <= 0\n", d, i, s, p, m, s, m);
          fprintf(f, " z_%d_%d_%d_%d_%d - x_%d_%d_%d_%d - y_%d_%d >= -1\n", d, i, s, p, m, d, i, s, p, s, m);
//          fprintf(f, " z_%d_%d_%d_%d_%d - [ x_%d_%d_%d_%d * y_%d_%d ] = 0\n", d, i, s, p, m, d, i, s, p, s, m);
        }
      }
    }
  }
}

void dump_lp_dag_pow(FILE *f, sys_t *p_sys, island_t *p_isl, int s, int p, int m, int d) {
  dag_t *p_dag = p_sys->dags[d];
  for (int i = 0; i < p_dag->num_elems; i++) {
    double pow_diff = dag_elem_pow_diff(p_sys, p_dag, i, p_isl, m);
    dag_elem_t *p_elem = &p_dag->elems[i];
    if (p_isl->cs_delay > 0 && (p_elem->accelerators & (1<<s))) {
      fprintf(f, " + %g h_%d_%d_%d_%d_%d", pow_diff * pow_exec_times_scale * lp_scale / p_dag->period, d, i, s, p, m);
    } else if (p_isl->type == CPU || (p_elem->accelerators & (1<<s))){
      fprintf(f, " + %g z_%d_%d_%d_%d_%d", pow_diff * pow_exec_times_scale * dag_elem_avg_exec_time_scaled(p_sys, p_dag, i, p_isl, m) / p_dag->period, d, i, s, p, m);
    }
  }
}

// easy but pessimistic: sum of bandwidths of all deployed tasks
void dump_lp_punit_constr_easy(FILE *f, sys_t *p_sys, int s, int p, int d) {
  island_t *p_isl = &p_sys->islands[s];
  dag_t *p_dag = p_sys->dags[d];
  fprintf(f, " - u_%d_%d_%d + [ ", d, s, p);
  for (int i = 0; i < p_dag->num_elems; i++) {
    if (p_dag->elems[i].omp_env_id == -1 && !p_dag->elems[i].fictitious) {
      for (int m = 0; m < p_isl->n_freqs; m++) {
        fprintf(f, " + %lg z_%d_%d_%d_%d_%d * di_%d_%d", dag_elem_wcet_scaled(p_sys, p_dag, i, p_isl, m) / lp_scale, d, i, s, p, m, d, i);
      }
    }
  }

  // adding the contributions of the OpenMP worker threads, if any
  for (int i = 0; i < p_sys->n_omp_envs; ++i) {
    fprintf(f, " + Q_%d_%d_%d * Pi_%d_%d_%d", i, s, p, i, s, p);
  }

  fprintf(f, " ] <= 0\n");
}

// less pessimistic: sum of bandwidths of all unrelated task subsets in each DAG
void dump_lp_punit_constr_unrelated(FILE *f, sys_t *p_sys, int s, int p, int d) {
  island_t *p_isl = &p_sys->islands[s];
  dag_t *p_dag = p_sys->dags[d];
  for (int ur = 0; ur < p_dag->unrelated_size; ur++) {
    fprintf(f, " - u_%d_%d_%d + [ ", d, s, p);

    for (int i = 0; i < p_dag->num_elems; i++) {
      if (!p_dag->elems[i].fictitious && (p_dag->unrelated[ur] & (1<<i))) {
        // skipping OpenMP tasks, as the contribution of their
        // servers will be added later
        if (p_dag->elems[i].omp_env_id != -1) {
          continue;
        }

        for (int m = 0; m < p_isl->n_freqs; m++) {
          fprintf(f, " + %g z_%d_%d_%d_%d_%d * di_%d_%d", dag_elem_wcet_scaled(p_sys, p_dag, i, p_isl, m) / lp_scale, d, i, s, p, m, d, i);
        }
      }
    }
    fprintf(f, " ] <= 0\n");
  }
}

void dump_lp_island_constr(FILE *f, sys_t *p_sys, int s) {
  island_t *p_isl = &p_sys->islands[s];
  for (int m = 0; m < p_isl->n_freqs; m++) {
    fprintf(f, " + y_%d_%d", s, m);
  }
  fprintf(f, " = 1\n");

  if (p_isl->type != CPU) {
    return;
  }

  for (int p = 0; p < p_isl->n_punits; p++) {
    for (int d = 0; d < p_sys->n_dags; d++) {
      dag_t *p_dag = p_sys->dags[d];
      if (p_dag->unrelated_size == 0) {
        LOG(INFO,"Using easy but pessimistic utilization constraints for island %d, punit %d, dag %d\n", s, p, d);
        dump_lp_punit_constr_easy(f, p_sys, s, p, d);
      } else {
        LOG(INFO,"Using unrelated-based utilization constraints for island %d, punit %d, dag %d\n", s, p, d);
        dump_lp_punit_constr_unrelated(f, p_sys, s, p, d);
      }
    }
  }
  // with just 1 DAG this constraint is already included in the bounds for the u_* variables (if create_non_binary_bounds == 1)
  // but still needed with OpenMP environments, as the bandwidth of the servers shall be taken into account
  if (p_sys->n_dags > 1 || !create_non_binary_bounds || p_sys->n_omp_envs > 0) {
    for (int p = 0; p < p_isl->n_punits; p++) {
      for (int d = 0; d < p_sys->n_dags; d++) {
        fprintf(f, " + u_%d_%d_%d", d, s, p);
      }

      // adding the contribution of the OpenMP worker
      // threads to the total utilization of the core
      if (p_sys->n_omp_envs > 0) {
        fprintf(f, " + [");

        for (int o = 0; o < p_sys->n_omp_envs; ++o) {
          fprintf(f, " + Q_%d_%d_%d * Pi_%d_%d_%d", o, s, p, o, s, p);
        }

        fprintf(f, " ]");
      }

      fprintf(f, " <= %g\n", max_util);
    }
  }
}

void dump_lp_non_acceleratable_constr(FILE *f, sys_t *p_sys, island_t* isl, int s) {
  for (int d = 0; d < p_sys->n_dags; ++d) {
    dag_t* p_dag = p_sys->dags[d];

    for (int i = 0; i < p_dag->num_elems; ++i) {
      dag_elem_t *p_elem = &p_dag->elems[i];

      if (!(p_elem->accelerators & (1<<s))) {
        // the task cannot be accelerated on the current accelerator
        // => forcing the corresponding allocation variable to 0
        for (int p = 0; p < isl->n_punits; ++p) {
          fprintf(f, " x_%d_%d_%d_%d = 0\n", d, i, s, p);
        }
      }
    }
  }
}

void dump_lp_acc_waiting_time_bound(FILE *f, sys_t *p_sys, island_t* p_isl, int s, int p) {
  // my task against all the others
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t* p_dag = p_sys->dags[d];

    for (int i = 0; i < p_dag->num_elems; ++i) {
      if (!(p_dag->elems[i].accelerators & (1<<s))) {
        continue;
      }

      // taking the unrelated sets of the current dag
      // in which the current task is present
      int non_empty_unrelated_sets_counter = 0;
      for (int j = 0; j < p_dag->unrelated_size; ++j) {
        //if (p_dag->elems[i].omp_env_id == -1 && (p_dag->unrelated[j] & (1<<i))) {
        if (p_dag->unrelated[j] & (1<<i)) {
          int unrelated_tasks_counter = 0;
          // the maximum is turned into a series of ">=" constraints
          // iterating over the tasks in the j-th unrelated set except the i-th one
          for (int k = 0; k < p_dag->num_elems; ++k) {
            //if (k == i || !(p_dag->unrelated[j] & (1<<k)) || p_dag->elems[k].omp_env_id != -1 || !(p_dag->elems[k].accelerators & (1<<s))) {
            if (k == i || !(p_dag->unrelated[j] & (1<<k)) || !(p_dag->elems[k].accelerators & (1<<s))) {
              continue;
            }

            if (unrelated_tasks_counter == 0) {
              non_empty_unrelated_sets_counter++;
            }
            unrelated_tasks_counter++;

            if (unrelated_tasks_counter == 1) {
              fprintf(f, " bown_%d_%d_%d_%d", d, i, s, p);
            }

            for (int m = 0; m < p_isl->n_freqs; m++) {
              dag_elem_t *p_elem = &p_dag->elems[k];
              if (p_isl->cs_delay > 0 && (p_elem->accelerators & (1<<s))) {
                fprintf(f, " - h_%d_%d_%d_%d_%d", d, k, s, p, m);
              } else {
                fprintf(f, " - %g z_%d_%d_%d_%d_%d", dag_elem_wcet_scaled(p_sys, p_dag, k, p_isl, m) / lp_scale, d, k, s, p, m);
              }
            }
          }

          if (unrelated_tasks_counter > 0) {
            fprintf(f, " >=  0\n");
          }
        }
      }

      // computing the contribution to the queuing time by the other DAGs
      int dags_counter = 0;
      for (int h = 0; h < p_sys->n_dags; h++) {
        if (h == d) {
          continue;
        }
        dag_t* p_dag_h = p_sys->dags[h];

        dags_counter++;

        for (int j = 0; j < p_dag_h->unrelated_size; ++j) {
          // the maximum is turned into a series of ">=" constraints
          fprintf(f, " both_%d_%d_%d_%d", d, i, s, p);
          // iterating over the tasks in the j-th unrelated set except the i-th one
          for (int k = 0; k < p_dag_h->num_elems; ++k) {
            //if (p_dag_h->elems[k].omp_env_id != -1 || !(p_dag_h->unrelated[j] & (1<<k)) || !(p_dag_h->elems[k].accelerators & (1<<s))) {
            if (!(p_dag_h->unrelated[j] & (1<<k)) || !(p_dag_h->elems[k].accelerators & (1<<s))) {
              continue;
            }

            for (int m = 0; m < p_isl->n_freqs; m++) {
              dag_elem_t p_elem = p_dag_h->elems[k];
              if (p_isl->cs_delay > 0 && (p_elem.accelerators & (1<<s))) {
                fprintf(f, " - h_%d_%d_%d_%d_%d", h, k, s, p, m);
              } else {
                fprintf(f, " - %g z_%d_%d_%d_%d_%d", dag_elem_wcet_scaled(p_sys, p_dag_h, k, p_isl, m) / lp_scale, h, k, s, p, m);
              }
            }
          }
          fprintf(f, " >= 0\n");
        }
      }

      // writing the final bound to the deadline
      fprintf(f, " x_%d_%d_%d_%d = 1 -> d_%d_%d", d, i, s, p, d, i);
      for (int m = 0; m < p_isl->n_freqs; m++) {
        dag_elem_t p_elem = p_dag->elems[i];
        if (p_isl->cs_delay > 0 && (p_elem.accelerators & (1<<s))) {
          fprintf(f, " - h_%d_%d_%d_%d_%d", d, i, s, p, m);
        } else {
          fprintf(f, " - %g z_%d_%d_%d_%d_%d", dag_elem_wcet_scaled(p_sys, p_dag, i, p_isl, m) / lp_scale, d, i, s, p, m);
        }
      }

      if (non_empty_unrelated_sets_counter > 0) {
        fprintf(f, " - bown_%d_%d_%d_%d", d, i, s, p);
      }

      if (dags_counter > 0) {
        fprintf(f, " - both_%d_%d_%d_%d", d, i, s, p);
      }

      fprintf(f, " >= 0\n");
    }
  }
}

void dump_lp_omp_tasks_waiting_time(FILE *f, sys_t *p_sys, omp_wt_t *wt, int o, int s, int p, int constrained_omp_servers) {
  p_island_t p_isl = wt->p_island;

  // my task against all the others
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t* p_dag = p_sys->dags[d];

    for (int i = 0; i < p_dag->num_elems; ++i) {
      if (p_dag->elems[i].omp_env_id != o) {
        continue;
      }

      // taking the unrelated sets of the current dag
      // in which the current task is present
      int non_empty_unrelated_sets_counter = 0;
      for (int j = 0; j < p_dag->unrelated_size; ++j) {
        if (p_dag->unrelated[j] & (1<<i)) {
          int unrelated_tasks_counter = 0;
          // the maximum is turned into a series of ">=" constraints
          // iterating over the tasks in the j-th unrelated set except the i-th one
          for (int k = 0; k < p_dag->num_elems; ++k) {
            dag_elem_t *p_elem_k = &p_dag->elems[k];

            if (k == i || !(p_dag->unrelated[j] & (1<<k)) || p_elem_k->omp_env_id != o) {
              continue;
            }

            if (unrelated_tasks_counter == 0) {
              non_empty_unrelated_sets_counter++;
            }
            unrelated_tasks_counter++;

            if (unrelated_tasks_counter == 1) {
              fprintf(f, " bown_%d_%d_%d_%d", d, i, s, p);
            }

            for (int m = 0; m < p_isl->n_freqs; m++) {
              fprintf(f, " - %g z_%d_%d_%d_%d_%d", dag_elem_wcet_scaled(p_sys, p_dag, k, p_isl, m) / lp_scale, d, k, s, p, m);
            }
          }

          if (unrelated_tasks_counter > 0) {
            fprintf(f, " >=  0\n");
          }
        }
      }

      // computing the contribution to the queuing time by the other DAGs
      int dags_counter = 0;
      for (int h = 0; h < p_sys->n_dags; h++) {
        if (h == d) {
          continue;
        }
        dag_t* p_dag_h = p_sys->dags[h];

        dags_counter++;

        for (int j = 0; j < p_dag_h->unrelated_size; ++j) {
          // the maximum is turned into a series of ">=" constraints
          fprintf(f, " both_%d_%d_%d_%d", d, i, s, p);
          // iterating over the tasks in the j-th unrelated set except the i-th one
          for (int k = 0; k < p_dag_h->num_elems; ++k) {
            dag_elem_t *p_elem_k = &p_dag->elems[k];
            if (p_elem_k->omp_env_id != o || !(p_dag_h->unrelated[j] & (1<<k))) {
              continue;
            }

            for (int m = 0; m < p_isl->n_freqs; m++) {
              fprintf(f, " - %g z_%d_%d_%d_%d_%d", dag_elem_wcet_scaled(p_sys, p_dag_h, k, p_isl, m) / lp_scale, h, k, s, p, m);
            }
          }
          fprintf(f, " >= 0\n");
        }
      }

      fprintf(f, " l_%d_%d_%d_%d", d, i, s, p);
      for (int m = 0; m < p_isl->n_freqs; m++) {
        fprintf(f, " - %g z_%d_%d_%d_%d_%d", dag_elem_wcet_scaled(p_sys, p_dag, i, p_isl, m) / lp_scale, d, i, s, p, m);
      }

      if (non_empty_unrelated_sets_counter > 0) {
        fprintf(f, " - bown_%d_%d_%d_%d", d, i, s, p);
      }

      if (dags_counter > 0) {
        fprintf(f, " - both_%d_%d_%d_%d", d, i, s, p);
      }

      // TODO: try with \geq
      fprintf(f, " >= 0\n");

      if (!constrained_omp_servers) {
        // linearizing the ceiling due to the bandwidth of the server
        fprintf(f, " [ K_%d_%d_%d_%d * Q_%d_%d_%d ] - l_%d_%d_%d_%d >= 0\n", d, i, s, p, o, s, p, d, i, s, p);
        fprintf(f, " [ K_%d_%d_%d_%d * Q_%d_%d_%d ] - l_%d_%d_%d_%d - Q_%d_%d_%d <= -0.00001\n", d, i, s, p, o, s, p, d, i, s, p, o, s, p);

        // writing the final bound to the deadline
        // FIXME: find a tighter bigM value
        fprintf(f, " d_%d_%d - [ K_%d_%d_%d_%d * P_%d_%d_%d ] - %g x_%d_%d_%d_%d >= - %g\n", d, i, d, i, s, p, o, s, p, 700.0, d, i, s, p, 700.0);

        // alternative implementation without bigM
        //fprintf(f, " x_%d_%d_%d_%d = 1 -> d_%d_%d - J_%d_%d_%d_%d >= 0\n", d, i, s, p, d, i, d, i, s, p);
        //fprintf(f, " J_%d_%d_%d_%d - [ K_%d_%d_%d_%d * P_%d_%d_%d ] = 0\n", d, i, s, p, d, i, s, p, o, s, p);
      } else {
        fprintf(f, " x_%d_%d_%d_%d = 1 -> d_%d_%d - P_%d_%d_%d >= 0\n", d, i, s, p, d, i, o, s, p);
      }
      // <<Indicator constraint has to be linear>>
      // fprintf(f, " x_%d_%d_%d_%d = 1 -> d_%d_%d - [ K_%d_%d_%d_%d * P_%d_%d_%d ] >= 0\n", d, i, s, p, d, i, d, i, s, p, o, s, p);
    }
  }
}

void dump_lp_accelerators_constr(FILE *f, sys_t *p_sys) {
  for (int s = 0; s < p_sys->n_islands; ++s) {
    island_t *p_isl = &p_sys->islands[s];
    if (p_isl->type != ACCELERATOR) {
      continue;
    }

    // preventing tasks not having an implementation for the
    // current accelerator to run on it
    dump_lp_non_acceleratable_constr(f, p_sys, p_isl, s);

    for (int p = 0; p < p_isl->n_punits; ++p) {
      dump_lp_acc_waiting_time_bound(f, p_sys, p_isl, s, p);
    }
  }
}

void dump_lp_accelerators_delay_constr(FILE *f, sys_t *p_sys) {
  int n_total_tasks = 0;

  for (int d = 0; d < p_sys->n_dags; d++) {
      dag_t *p_dag = p_sys->dags[d];
      n_total_tasks += p_dag->num_elems;
  }

  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];

    if (p_isl->type == CPU || p_isl->cs_delay <= 0) {
      continue;
    }

    for (int p = 0; p < p_isl->n_punits; p++) {
      int task_counter = 0;
      for (int d = 0; d < p_sys->n_dags; d++) {
        dag_t *p_dag = p_sys->dags[d];
        for (int i = 0; i < p_dag->num_elems; i++) {
          // not even checking whether the task may/may not be mapped
          // to the accelerator, since in case it cannot, its x var is already 0
          if (p_dag->elems[i].omp_env_id != -1) {
            continue;
          }

          if (task_counter == 0) {
            fprintf(f, " m_%d_%d", s, p);
          }

          task_counter++;

          fprintf(f, " - x_%d_%d_%d_%d", d, i, s, p);
        }
      }

      fprintf(f, " = 0\n");
    }

    for (int p = 0; p < p_isl->n_punits; p++) {
      fprintf(f, " s_%d_%d - 0.5 m_%d_%d <= 0\n", s, p, s, p);
      fprintf(f, " s_%d_%d - %g m_%d_%d >= - %g\n", s, p, 1.0 / n_total_tasks, s, p, 1.0 / n_total_tasks);

      for (int d = 0; d < p_sys->n_dags; d++) {
        dag_t *p_dag = p_sys->dags[d];
        for (int i = 0; i < p_dag->num_elems; i++) {
          if (p_dag->elems[i].omp_env_id != -1 || !(p_dag->elems[i].accelerators & (1<<s))) {
            continue;
          }

          for (int m = 0; m < p_isl->n_freqs; m++) {
            fprintf(f, " r_%d_%d_%d_%d_%d - z_%d_%d_%d_%d_%d <= 0\n", d, i, s, p, m, d, i, s, p, m);
            fprintf(f, " r_%d_%d_%d_%d_%d - s_%d_%d <= 0\n", d, i, s, p, m, s, p);
            fprintf(f, " r_%d_%d_%d_%d_%d - z_%d_%d_%d_%d_%d - s_%d_%d >= -1\n", d, i, s, p, m, d, i, s, p, m, s, p);

            fprintf(f, " h_%d_%d_%d_%d_%d - %g z_%d_%d_%d_%d_%d - %g r_%d_%d_%d_%d_%d = 0\n", d, i, s, p, m, dag_elem_wcet_scaled(p_sys, p_dag, i, p_isl, m) / lp_scale, d, i, s, p, m, p_isl->cs_delay / lp_scale, d, i, s, p, m);
          }
        }
      }
    }
  }
}

void dump_lp_variables_bounds(FILE *f, sys_t *p_sys, int bound_slack, int constrained_omp_servers) {
  // finding the upper bound of the additional power terms
  // to be added to the power expression
  double additional_pow_ub = 0.0;
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    for (int i = 0; i < p_dag->num_elems; i++) {
      for (int s = 0; s < p_sys->n_islands; s++) {
        if (p_dag->elems[i].isl_pow[s] > additional_pow_ub) {
          additional_pow_ub = p_dag->elems[i].isl_pow[s];
        }
      }
    }
  }

  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    for (int i = 0; i < p_dag->num_elems; i++) {
      double wcet_min = dag_elem_wcet_scaled_min(p_sys, p_dag, i);
      double min_dl = (min_deadline > wcet_min ? min_deadline : wcet_min); // works also when min_deadline = 0
      fprintf(f, " %g <= d_%d_%d <= %g\n", min_dl / lp_scale, d, i, p_dag->deadline / lp_scale);
      fprintf(f, " %g <= di_%d_%d <= %g\n", lp_scale / p_dag->deadline, d, i, lp_scale / min_dl);
      fprintf(f, " 0 <= f_%d_%d <= %g\n", d, i, p_dag->deadline / lp_scale);
    }

    for (int s = 0; s < p_sys->n_islands; s++) {
      island_t *p_isl = &p_sys->islands[s];

      for (int p = 0; p < p_isl->n_punits; p++) {
        if (p_isl->type == CPU) {
          fprintf(f, " 0 <= u_%d_%d_%d <= %g\n", d, s, p, max_util);

          for (int i = 0; i < p_dag->num_elems; i++) {
            // OpenMP tasks
            if (p_dag->elems[i].omp_env_id != -1) {
              if (!constrained_omp_servers) {
                fprintf(f, " 0 <= K_%d_%d_%d_%d <= %g\n", d, i, s, p, p_dag->deadline / lp_scale);
              } else {
                fprintf(f, " 0 <= g_%d_%d_%d_%d <= %g\n", d, i, s, p, p_dag->deadline / lp_scale);
              }

              fprintf(f, " 0 <= l_%d_%d_%d_%d <= %g\n", d, i, s, p, p_dag->deadline / lp_scale);
            }
          }


        } else if (p_isl->type == ACCELERATOR) {
          if (p_isl->cs_delay > 0) {
            int n_total_tasks = 0;

            for (int d = 0; d < p_sys->n_dags; d++) {
                dag_t *p_dag = p_sys->dags[d];
                n_total_tasks += p_dag->num_elems;
            }

            fprintf(f, " 0 <= m_%d_%d <= %d\n", s, p, n_total_tasks);
          }
          for (int i = 0; i < p_dag->num_elems; i++) {
            fprintf(f, " 0 <= bown_%d_%d_%d_%d <= %g\n", d, i, s, p, p_dag->deadline / lp_scale);
            fprintf(f, " 0 <= both_%d_%d_%d_%d <= %g\n", d, i, s, p, p_dag->deadline / lp_scale);
          }
        }

        fprintf(f, " 0 <= Pmax_%d_%d <= %g\n", s, p, additional_pow_ub);
      }
    }
  }

  // bounding the parameters of the OpenMP worker threads
  for (int o = 0; o < p_sys->n_omp_envs; ++o) {
    omp_env_t * env = &p_sys->omp_envs[o];

    // FIXME: are there better bounds?
    for (int s = 0; s < env->n_islands; ++s) {
      for (int p = 0; p < env->n_punits[s]; ++p) {
        fprintf(f, "0 <= Q_%d_%d_%d\n", o, s, p);
        fprintf(f, "0 <= P_%d_%d_%d\n", o, s, p);
        fprintf(f, "0 <= Pi_%d_%d_%d\n", o, s, p);
      }
    }
  }

  if (bound_slack == 1) {
    fprintf(f, " 0 <= slack <= 1\n");
  }
}

void dump_lp_set_min_pow_obj_fun(sys_t *p_sys, FILE *f) {
  fprintf(f, "Minimize\n obj: pow\n");
  fprintf(f, "\nSubject To\n");
}

void dump_lp_set_multi_objectives(sys_t *p_sys, FILE *f) {
  fprintf(f, "Minimize multi-objectives\n");

  // power objective function (highest priority)
  fprintf(f, "  OBJ0: Priority=2 Weight=1 AbsTol=0 RelTol=0\n    pow\n");

  // slack objective function (lowest priority), with negative weight
  // so that to maximize it
  fprintf(f, "  OBJ1: Priority=1 Weight=-1 AbsTol=0 RelTol=0\n    slack\n");

  fprintf(f, "\nSubject To\n");

  for (int d = 0; d < p_sys->n_dags; d++) {
      dag_t *p_dag = p_sys->dags[d];
      fprintf(f, " slack + %g f_%d_%d <= 1\n", lp_scale / p_dag->deadline, d, p_dag->last);
    }
}

void dump_lp_set_max_slack_obj_fun(sys_t *p_sys, FILE *f, double power_budget) {
  fprintf(f, "Maximize\n obj: slack\n");
  fprintf(f, "\nSubject To\n");

  fprintf(f, " %g pow <= 1\n", 1.0 / power_budget);

  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    fprintf(f, " slack + %g f_%d_%d <= 1\n", lp_scale / p_dag->deadline, d, p_dag->last);
  }
}

void dump_lp_accel_delay_bin(FILE *f, sys_t *p_sys) {
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    if (p_isl->type == CPU || p_isl->cs_delay <= 0) {
      continue;
    }
    for (int p = 0; p < p_isl->n_punits; p++) {
      fprintf(f, " s_%d_%d\n", s, p);
      for (int d = 0; d < p_sys->n_dags; d++) {
        dag_t *p_dag = p_sys->dags[d];
        for (int i = 0; i < p_dag->num_elems; i++) {
          for (int m = 0; m < p_isl->n_freqs; m++) {
            fprintf(f, " r_%d_%d_%d_%d_%d\n", d, i, s, p, m);
          }
        }
      }

    }
  }
}

void dump_lp_fix_deadlines(FILE *f, sys_t *p_sys) {
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    for (int i = 0; i < p_dag->num_elems; i++) {
      dag_elem_t* p_elem = &p_dag->elems[i];
      fprintf(f, " d_%d_%d = %g\n", d, i, p_elem->d / lp_scale);
    }
  }
}

void dump_lp_accelerators_additional_pow(FILE *f, sys_t *p_sys) {
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    for (int i = 0; i < p_dag->num_elems; i++) {
      if (p_dag->elems[i].omp_env_id != -1) {
        continue;
      }

      for (int s = 0; s < p_sys->n_islands; ++s) {
        island_t *p_isl = &p_sys->islands[s];
        // only accelerators (or in general, islands) for which an additional
        // power consumption is specified shall be considered
        // assumption: CPU islands always 0 and accelerators for which
        // the task does not have an implementation always 0
        if (p_dag->elems[i].isl_pow[s] <= 0.0) {
          continue;
        }

        for (int p = 0; p < p_isl->n_punits; p++) {
          fprintf(f, " Pmax_%d_%d - %g x_%d_%d_%d_%d >= 0\n", s, p, p_dag->elems[i].isl_pow[s], d, i, s, p);
        }
      }
    }
  }
}

void dump_lp_omp_server_budget_constr(FILE *f, sys_t *p_sys, int o, int s, int p) {
  for (int d = 0; d < p_sys->n_dags; ++d) {
      dag_t* p_dag = p_sys->dags[d];

      for (int i = 0; i < p_dag->num_elems; ++i) {
        if (p_dag->elems[i].omp_env_id != o) {
          continue;
        }

        fprintf(f, " Q_%d_%d_%d - g_%d_%d_%d_%d >= 0\n", o, s, p, d, i, s, p);

        // alternative encoding with indicator constraints
        fprintf(f, " x_%d_%d_%d_%d = 0 -> g_%d_%d_%d_%d = 0\n", d, i, s, p, d, i, s, p);
        fprintf(f, " x_%d_%d_%d_%d = 1 -> g_%d_%d_%d_%d - l_%d_%d_%d_%d = 0\n", d, i, s, p, d, i, s, p, d, i, s, p);

//        fprintf(f, " g_%d_%d_%d_%d - %g x_%d_%d_%d_%d <= 0\n", d, i, s, p, p_dag->deadline / lp_scale, d, i, s, p);
//        fprintf(f, " l_%d_%d_%d_%d + %g x_%d_%d_%d_%d - g_%d_%d_%d_%d <= %g\n", d, i, s, p, p_dag->deadline / lp_scale, d, i, s, p, d, i, s, p, p_dag->deadline / lp_scale);
//        fprintf(f, " g_%d_%d_%d_%d - l_%d_%d_%d_%d <= 0\n", d, i, s, p, d, i, s, p);
      }
  }
}

void dump_lp_omp_tasks_constr(FILE *f, sys_t *p_sys, int constrained_omp_servers) {
  // blocking the assignment on accelerators
  for (int d = 0; d < p_sys->n_dags; ++d) {
    dag_t* p_dag = p_sys->dags[d];

    for (int i = 0; i < p_dag->num_elems; ++i) {
      dag_elem_t *p_elem = &p_dag->elems[i];

      if (p_elem->omp_env_id != -1) {
        for (int s = 0; s < p_sys->n_islands; ++s) {
          island_t *p_isl = &p_sys->islands[s];
          if (p_isl->type != ACCELERATOR) {
            continue;
          }

          for (int p = 0; p < p_isl->n_punits; ++p) {
            fprintf(f, " x_%d_%d_%d_%d = 0\n", d, i, s, p);
          }
        }
      }
    }
  }

  for (int o = 0; o < p_sys->n_omp_envs; ++o) {
    omp_env_t * env = &p_sys->omp_envs[o];
    for (int s = 0; s < env->n_islands; ++s) {
      if (p_sys->islands[s].type == ACCELERATOR) {
        continue;
      }

      for (int p = 0; p < env->n_punits[s]; ++p) {
        omp_wt_t *wt = &env->worker_threads[s][p];

        // creating the inverse of the period of the current
        // worker thread (used in the CPU schedulability test)
        fprintf(f, " [ P_%d_%d_%d * Pi_%d_%d_%d ] = 1\n", o, s, p, o, s, p);

        // bounding the traversing time of the OpenMP tasks when served
        // by the current worker thread (computation of interference
        // of the same environment)
        dump_lp_omp_tasks_waiting_time(f, p_sys, wt, o, s, p, constrained_omp_servers);

        if (constrained_omp_servers) {
          dump_lp_omp_server_budget_constr(f, p_sys, o, s, p);
        }
      }
    }
  }
}

void dump_lp_omp_int_vars(FILE* f, sys_t* p_sys) {
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];

    for (int s = 0; s < p_sys->n_islands; s++) {
      island_t *p_isl = &p_sys->islands[s];

      for (int p = 0; p < p_isl->n_punits; p++) {
        for (int i = 0; i < p_dag->num_elems; i++) {
          // OpenMP tasks
          if (p_dag->elems[i].omp_env_id != -1) {
            fprintf(f, " K_%d_%d_%d_%d\n", d, i, s, p);
          }
        }
      }
    }
  }
}

void dump_lp_intermediate_dls(FILE *f, sys_t *p_sys) {
  for (int d = 0; d < p_sys->n_dags; ++d) {
    dag_t *p_dag = p_sys->dags[d];

    for (int j = 0; j < p_dag->n_intermediate_dls; ++j) {
      intermediate_dl *dl = &(p_dag->inter_dls[j]);

      if (dl->start_elem_id == 0) {
        fprintf(f, " f_%d_%d <= %g\n", d, dl->end_elem_id, dl->deadline / lp_scale);
      } else {
        foreach_elem_prev(p_dag, dl->start_elem_id)
          fprintf(f, " f_%d_%d - f_%d_%d <= %g\n", d, dl->end_elem_id, d, p, dl->deadline / lp_scale);
      }
    }
  }
}

void dump_lp_sys(sys_t *p_sys, const char *fname, double power_budget,
    int use_dl_splitting, int multi_objective, int constrained_omp_servers) {
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  assert(p_sys != NULL);

  //TODO: CHECK THAT THE NEW OPENMP TASKS ARE COMPATIBLE WITH THE SLACK OBJ FUN
  if (power_budget > 0) {
    dump_lp_set_max_slack_obj_fun(p_sys, f, power_budget);
  } else if (multi_objective == 1) {
    dump_lp_set_multi_objectives(p_sys, f);
  } else {
    dump_lp_set_min_pow_obj_fun(p_sys, f);
  }

  fprintf(f, " - pow");

  // the idle power is per island, no matter the load, so this is summed up first
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    for (int m = 0; m < p_isl->n_freqs; m++) {
      fprintf(f, " + %g y_%d_%d", p_isl->pows_idle[m], s, m);
    }
  }

  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    for (int p = 0; p < p_isl->n_punits; p++) {
      fprintf(f, " + Pmax_%d_%d", s, p);

      for (int m = 0; m < p_isl->n_freqs; m++) {
        for (int d = 0; d < p_sys->n_dags; d++)
          dump_lp_dag_pow(f, p_sys, p_isl, s, p, m, d);
      }
    }
  }
  fprintf(f, " = 0\n");

  for (int d = 0; d < p_sys->n_dags; d++) {
    dump_lp_dag_constr(p_sys, d, f);
  }
  for (int s = 0; s < p_sys->n_islands; s++) {
    dump_lp_island_constr(f, p_sys, s);
  }

  dump_lp_accelerators_constr(f, p_sys);

  dump_lp_accelerators_delay_constr(f, p_sys);

  dump_lp_accelerators_additional_pow(f, p_sys);

  dump_lp_omp_tasks_constr(f, p_sys, constrained_omp_servers);

  // FIXME: is it working/needed with OpenMP tasks?
  if (use_dl_splitting == 1) {
    sys_set_deadlines(p_sys);
    dump_lp_fix_deadlines(f, p_sys);
  }

  dump_lp_intermediate_dls(f, p_sys);

  // FIXME: add the new variables everywhere
  if (create_non_binary_bounds == 1) {
    fprintf(f, "\nBounds\n");
    int bound_slack = (power_budget > 0 || multi_objective == 1) ? 1 : 0;
    // FIXME: not sure whether the bounds (like on d_i) still fit OpenMP tasks
    dump_lp_variables_bounds(f, p_sys, bound_slack, constrained_omp_servers);
  }

  fprintf(f, "\nBinary\n");
  for (int d = 0; d < p_sys->n_dags; d++) {
    dump_lp_dag_bin(p_sys, d, f);
  }
  dump_lp_accel_delay_bin(f, p_sys);

  // output binary variables for island power modes: y_island_mode
  for (int s = 0; s < p_sys->n_islands; s++)
    for (int m = 0; m < p_sys->islands[s].n_freqs; m++)
      fprintf(f, " y_%d_%d\n", s, m);

  if (!constrained_omp_servers) {
    fprintf(f, "\nGeneral\n");
    dump_lp_omp_int_vars(f, p_sys);
  }

  fprintf(f, "\nEnd\n");
  fclose(f);
}

void dag_path_init(dag_path_t *p_dag_path) {
  p_dag_path->n_elems = 0;
}

void dag_path_init_single(dag_path_t *p_path, int n) {
  p_path->n_elems = 1;
  p_path->elems[0] = n;
}

void dag_path_cat(dag_path_t *p_dst_path, dag_path_t *p_src_path) {
  check(p_dst_path->n_elems + p_src_path->n_elems <= MAX_PATH_LEN, "MAX_PATH_LEN exceeded%s","");
  for (int i = 0; i < p_src_path->n_elems; i++)
    p_dst_path->elems[p_dst_path->n_elems++] = p_src_path->elems[i];
}

void dag_path_append(dag_path_t *p_path, int n) {
  check(p_path->n_elems < MAX_PATH_LEN, "MAX_PATH_LEN exceeded%s","");
  p_path->elems[p_path->n_elems++] = n;
}

// return scaled WCET if elem e of dag is placed on island with assigned opt_freq;
// return scaled WCET at max island freq if elem e is placed on some island;
// otherwise, return just elem's WCET
double dag_elem_wcet_or_scaled(dag_t *p_dag, sys_t *p_sys, int e) {
  int opt_s = p_dag->elems[e].opt_island;
  if (opt_s == -1 || p_sys == NULL)
    return p_dag->elems[e].C;
  else if (p_sys->islands[opt_s].opt_freq == -1)
    return dag_elem_wcet_scaled(p_sys, p_dag, e, &p_sys->islands[opt_s], 0);
  else
    return dag_elem_wcet_scaled(p_sys, p_dag, e, &p_sys->islands[opt_s], p_sys->islands[opt_s].opt_freq);
}

// return path length from s to e, both included, or -1 if e is not reachable from s
static double dag_longest_path_cached(dag_t *p_dag, sys_t *p_sys, int s, int e, int use_dl) {
  double ret_code;
  (void) ret_code;
  LOG(DEBUG,"s=%d, e=%d\n", s, e);
  if (p_dag->elems[s].longest_len != -1) {
    // longest path from here to e already computed, return it right away
    return p_dag->elems[s].longest_len;
  }
  if (s == e) {
    double len = (use_dl ? p_dag->elems[e].d : dag_elem_wcet_or_scaled(p_dag, p_sys, e));
    p_dag->elems[s].longest_len = len;
    p_dag->elems[s].longest_next = -1;
    LOG(DEBUG,"returning node %d (opt_isl %d) len: %f\n", s, p_dag->elems[s].opt_island, len);
    return len;
  }
  double max_len = -1;
  int max_idx = -1;
  foreach_elem_next(p_dag, s) {
    double len = dag_longest_path_cached(p_dag, p_sys, n, e, use_dl);
    if (len != -1 && len > max_len) {
      max_len = len;
      max_idx = n;
    }
  }
  if (max_len == -1)
    return -1;

  double len = (use_dl ? p_dag->elems[s].d : dag_elem_wcet_or_scaled(p_dag, p_sys, s)) + max_len;
  p_dag->elems[s].longest_len = len;
  p_dag->elems[s].longest_next = max_idx;
  LOG(DEBUG,"returning %f\n", len);

  return len;
}

// clear internal cache of longest paths before calling dag_longest_path_cached()
// Warning: dag_longest_path_dl() and dag_longest_path() overwite the cache of longest_next
double dag_longest_path(dag_t *p_dag, sys_t *p_sys, dag_path_t *p_path) {
  for (int i = 0; i < p_dag->num_elems; i++) {
    p_dag->elems[i].longest_len = -1;
    p_dag->elems[i].longest_next = -1;
  }
  double len = dag_longest_path_cached(p_dag, p_sys, p_dag->first, p_dag->last, 0);
  if (p_path != NULL && len != -1) {
    dag_path_init(p_path);
    foreach_longest_next(p_dag, p_dag->first, n)
      dag_path_append(p_path, n);
  }
  return len;
}

// Use deadlines, so compute the worst-case response-time (useful for deriving the slack)
// Warning: dag_longest_path_dl() and dag_longest_path() overwite the cache of longest_next
double dag_longest_path_dl(dag_t *p_dag, sys_t *p_sys, dag_path_t *p_path) {
  for (int i = 0; i < p_dag->num_elems; i++) {
    p_dag->elems[i].longest_len = -1;
    p_dag->elems[i].longest_next = -1;
  }
  double len = dag_longest_path_cached(p_dag, p_sys, p_dag->first, p_dag->last, 1);
  if (p_path != NULL && len != -1) {
    dag_path_init(p_path);
    foreach_longest_next(p_dag, p_dag->first, n)
      dag_path_append(p_path, n);
  }
  return len;
}

double dag_path_len(dag_t *p_dag, sys_t *p_sys, dag_path_t *p_path) {
  double len = 0.0;
  for (int i = 0; i < p_path->n_elems; i++)
    len += dag_elem_wcet_or_scaled(p_dag, p_sys, p_path->elems[i]);
  return len;
}

double dag_longest_len(dag_t *p_dag, sys_t *p_sys, int s) {
  double len = 0.0;
  foreach_longest_next(p_dag, s, n)
    len += dag_elem_wcet_or_scaled(p_dag, p_sys, n);
  return len;
}

void dag_path_dump(dag_path_t *p_path) {
  for (int i = 0; i < p_path->n_elems; i++)
    printf("%d,", p_path->elems[i]);
}

void dag_longest_dump(dag_t *p_dag, int s) {
  foreach_longest_next(p_dag, s, n)
    printf("%d,", n);
}

void dag_clear_deadlines(dag_t *p_dag) {
  for (int e = 0; e < p_dag->num_elems; e++) {
    p_dag->elems[e].d = -1;
    LOG(DEBUG, "Deadline elems[%d].d = %f\n", e, p_dag->elems[e].d);
  }
}

const double eps = 1e-5;

void dag_set_deadlines_ex(dag_t *p_dag, sys_t *p_sys, int s, int e, double D) {
  LOG(DEBUG,"s=%d, e=%d, D=%f, elem[s].C=%f, elem[e].C=%f\n", s, e, D,
      p_dag->elems[s].C, p_dag->elems[e].C);

  double C_sum = dag_longest_path_cached(p_dag, p_sys, s, e, 0);
  LOG(DEBUG,"path (C_sum=%f), path_len=%f", C_sum, dag_longest_len(p_dag, p_sys, s));
#if LOG_LEVEL >= DEBUG
  printf(" [");
  dag_longest_dump(p_dag, s);
  printf(" ]\n");
#endif

  if (C_sum == -1) // possible in recursive calls with e non-reachable from s
    return;

  double D_sum = D;
  foreach_longest_next(p_dag, s, n) {
    dag_elem_t *p_elem = &p_dag->elems[n];
    if (p_elem->d != -1) {
      double wcet = dag_elem_wcet_or_scaled(p_dag, p_sys, n);
      LOG(DEBUG, "Found already assigned params for node %d: d=%lf, wcet=%lf\n",
             n, p_elem->d, wcet);
      double d = D_sum * wcet / C_sum;
      if (d < p_elem->d) {
        // tightening a deadline won't break e2e constraints on a path where it was set looser
        LOG(DEBUG, "Anticipating deadline of node %d from %lf to %lf (D_sum=%lf, wcet=%lf, C_sum=%lf)\n", n, p_elem->d, d, D_sum, wcet, C_sum);
        p_elem->d = d;
        LOG(DEBUG,"set elems[%d]->d = %f\n", n, p_elem->d);
        assert(p_elem->d >= 0);
      }
      LOG(DEBUG, "Discounting already assigned params for node %d: d=%lf, wcet=%lf\n",
          n, p_elem->d, wcet);
      D_sum -= p_elem->d;
      C_sum -= wcet;
      LOG(DEBUG, "After discount for pre-assigned deadline: D_sum=%lf, C_sum=%lf\n", D_sum, C_sum);
      assert(D_sum >= -eps && C_sum >= -eps);
      if (D_sum < 0)
        D_sum = 0.0;
      if (C_sum < 0)
        C_sum = 0.0;
    }
  }

  foreach_longest_next(p_dag, s, n) {
    dag_elem_t *p_elem = &p_dag->elems[n];
    if (p_elem->d == -1) {
      LOG(DEBUG,"C_sum[%f]; D_sum[%f]\n",C_sum, D_sum);
      double wcet;
      if (p_elem->C > 0) {
        wcet = dag_elem_wcet_or_scaled(p_dag, p_sys, n);
        assert(C_sum > 0);
        p_elem->d = D_sum * wcet / C_sum;
      } else {
        wcet = 0;
        p_elem->d = 0;
      }
      LOG(DEBUG,"set elems[%d]->d = %f\n", n, p_elem->d);
      assert(p_elem->d >= 0);
      D_sum -= p_elem->d;
      C_sum -= wcet;
      LOG(DEBUG, "After discount for new assigned deadline: D_sum=%f, C_sum=%f\n", D_sum, C_sum);
      assert(D_sum >= -eps && C_sum >= -eps);
      if (D_sum < 0)
        D_sum = 0.0;
      if (C_sum < 0)
        C_sum = 0.0;
    }
  }
  LOG(DEBUG, "Residual C_sum[%f]; D_sum[%f]\n",C_sum, D_sum);

  D -= p_dag->elems[s].d;
  if (D < -eps)
    return;
  else if (D < 0)
    D = 0;

  foreach_elem_next(p_dag, s) {
    LOG(DEBUG,"node %d, [node].d=%lf, D=%lf\n", n, p_dag->elems[n].d, D);
    dag_set_deadlines_ex(p_dag, p_sys, n, e, D);
  }
}

// TODO: set deadlines for accelerators first
void dag_set_deadlines(dag_t *p_dag, sys_t *p_sys, int s, int e, double D, int clear) {
  LOG(DEBUG, "clear=%d\n", clear);
  assert(p_dag->elems[s].n_prev == 0);
  assert(p_dag->elems[e].n_next == 0);
  if (clear)
    dag_clear_deadlines(p_dag);
  dag_longest_path(p_dag, p_sys, NULL);
  dag_set_deadlines_ex(p_dag, p_sys, s, e, D * MAX_DEADLINE_PERC);
}

// TODO: set deadlines for accelerators first
void sys_set_deadlines(sys_t *p_sys) {
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    if (p_dag->unrelated_size == 0)
      dag_comp_unrelated(p_dag);
    dag_set_deadlines(p_dag, p_sys, p_dag->first, p_dag->last, p_dag->deadline, 1);
  }
}

void sys_clear_deadlines(sys_t *p_sys) {
  for (int d = 0; d < p_sys->n_dags; d++)
    dag_clear_deadlines(p_sys->dags[d]);
}

void sys_init(sys_t *p_sys) {
  p_sys->n_islands = 0;
  p_sys->n_dags = 0;
  p_sys->hyperperiod = 0;
  p_sys->opt_pow = 0.0;
  p_sys->n_omp_envs = 0;
  p_sys->n_amalthea_dags = 0;

  for (int i = 0; i < MAX_DAGS; ++i) {
    p_sys->amalthea_to_dag_id[i] = -1;
    p_sys->amalthea_to_dag_task_id_offset[i] = -1;
    p_sys->amalthea_dag_names[i] = NULL;
  }

  p_sys->last_dag = NULL;
}

void island_dump(island_t *p_isl) {
  printf("punits %d, cap %f, sched %u, opt_freq: %d\n",
         p_isl->n_punits, p_isl->capacity, p_isl->sched, p_isl->opt_freq);
  assert(p_isl->table != NULL);
  print_table(p_isl->table);
  // this is a simpler printing format
  //print_csv(p_isl->table);
}

void sys_dump(sys_t *p_sys) {
  printf("System:\n");
  if (p_sys->n_islands == 0)
    printf("  no islands defined!\n");
  for (int i = 0; i < p_sys->n_islands; i++) {
    printf("  island %d: ", i);
    island_dump(&p_sys->islands[i]);
  }
  printf("DAGs with hyperperiod %ld, load %g\n", p_sys->hyperperiod, sys_load(p_sys));
  if (p_sys->n_dags == 0)
    printf("  no dags defined!\n");
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    dag_dump(p_dag);
    printf("Unrelated for DAG %d: ", d);
    dag_dump_unrelated(p_dag);
  }
}

void sys_cleanup(sys_t *p_sys){
  for (int d = 0; d < p_sys->n_dags; d++) {
      dag_cleanup(p_sys->dags[d]);
  }
  for (int s = 0; s < p_sys->n_islands; s++) {
    free_table(p_sys->islands[s].table);
  }

  // freeing DAG names
  for (int i = 0; i < p_sys->n_amalthea_dags; ++i) {
    free(p_sys->amalthea_dag_names[i]);
  }
}

// cs_delay is in nanosecs
island_t *sys_add_island(sys_t *p_sys, double capacity, int n_punits, sys_sched_t sched, island_type_enum type, double cs_delay) {
  assert(p_sys->n_islands <= MAX_ISLANDS);
  assert(n_punits <= MAX_PUNITS);
  island_t *p_island = &p_sys->islands[p_sys->n_islands++];
  p_island->n_punits = n_punits;
  p_island->n_freqs = 0;
  p_island->sched = sched;
  p_island->capacity = capacity;
  p_island->opt_freq = -1;
  for (int i = 0; i < n_punits; i++) {
    p_island->punits[i].p_island = p_island;
  }
  p_island->type = type;
  p_island->cs_delay = cs_delay;
  char table_name[64];
  snprintf(table_name,64,"Power Table Island %d", p_sys->n_islands-1);
  p_island->table = table_init(stdout, (unsigned char*)table_name, 1);
  table_row(p_island->table, (unsigned char*)"%s%s%s", "Freq (Hz)", "Busy Power (W)", "Idle Power (W)");
  table_separator(p_island->table);
  return p_island;
}

unsigned long gcd(unsigned long a, unsigned long b) {
  while (a != b) {
    if (a > b) {
      a %= b;
      if (a == 0)
        return b;
    } else {
      b %= a;
      if (b == 0)
        return a;
    }
  }
  return a;
}

unsigned long mcm(unsigned long a, unsigned long b) {
  unsigned long d = gcd(a, b);
  return a / d * b;
}

int sys_find_dag(sys_t *p_sys, dag_t *p_dag) {
  for (int d = 0; d < p_sys->n_dags; d++)
    if (p_sys->dags[d] == p_dag)
      return d;
  return -1;
}

void sys_add_dag(sys_t *p_sys, dag_t *p_dag) {
  assert(p_sys->n_dags < MAX_DAGS);
  p_sys->dags[p_sys->n_dags++] = p_dag;
  if (p_sys->n_dags == 1)
    p_sys->hyperperiod = p_dag->period;
  else
    p_sys->hyperperiod = mcm(p_sys->hyperperiod, p_dag->period);
  printf("Updated hyperperiod %ld\n", p_sys->hyperperiod);
}

/*
Add an OPP to the current island with the specified frequency and per-island power values.
Busy power refers to one CPU busy, including the island idle power.

additional explanation from the heuristics source code:
// delta power (delta = busy - idle power) represents one task fully using one cpu.
// For example, if the island has a load of 0.5, meaning just one core has 50% of load, then the island power is:
// island idle power + (delta power * 0.5)
// If an island has 4 cpus, the power in a fully loaded scenario would be:
// island idle power + (delta power * 0.95 * 4)
// since the max allowed cpu load is 95%
*/
void island_add_freq(island_t *p_sys, double f, double pow_busy, double pow_idle) {
  assert(p_sys->n_freqs < MAX_FREQS);
  p_sys->freqs[p_sys->n_freqs] = f;
  p_sys->pows_busy[p_sys->n_freqs] = pow_busy;
  p_sys->pows_idle[p_sys->n_freqs++] = pow_idle;
  // populate the table for nice priting
  table_row(p_sys->table, (unsigned char*)"%.2f%.8f%.8f", f, pow_busy, pow_idle);
}

int parse_gurobi_sol(sys_t *p_sys, const char *fname) {
  int rv = 0;
  size_t size = 256;
  char *line = malloc(size);
  double power=0;
  // the whether the file was generated w any content
  FILE *f = fopen(fname, "r");
  if(f == NULL){
      printf ("Could not open solution file: %s!\n\n", fname);
      exit(1);
  }
  fseek(f, 0L, SEEK_END);
  if (ftell(f) == 0){
      printf ("Unable to find a valid solution in file: %s!\n\n", fname);
      exit(1);
  }
  fseek(f, 0L, SEEK_SET);
  // extract the resulting power to check whether any solution was generated
  while (!feof(f)) {
      size_t size;
      line[0] = 0;
      int read_chars = getline(&line, &size, f);
      if (read_chars == 0)
          continue;
      if (sscanf(line, "pow %lf", &power) == 2) {
          break;
      }
  }
  fseek(f, 0L, SEEK_SET);

  while (!feof(f)) {
    line[0] = 0;
    int read_chars = getline(&line, &size, f);
    if (read_chars == 0)
      continue;
    int d, i, s, p, m, o;
    double val;
    double dline;
    if (strncmp(line, "# Solution for model", 20) == 0) {
      rv = 1;
    } else if (sscanf(line, "pow %lf", &val) == 1) {
      p_sys->opt_pow = val;
    } else if (sscanf(line, "d_%d_%d %lf", &d, &i, &dline) == 3) {
      dline *= lp_scale;
      assert(d >= 0 && d < p_sys->n_dags);
      assert(i >= 0 && i < p_sys->dags[d]->num_elems);
      p_sys->dags[d]->elems[i].d = dline;
    } else if (sscanf(line, "x_%d_%d_%d_%d %lf", &d, &i, &s, &p, &val) == 5 && FEQUAL(val, 1.0f)) {
      assert(d >= 0 && d < p_sys->n_dags);
      assert(i >= 0 && i < p_sys->dags[d]->num_elems);
      assert(s >= 0 && s < p_sys->n_islands);
      assert(p >= 0 && p < p_sys->islands[s].n_punits);
      p_sys->dags[d]->elems[i].opt_island = s;
      p_sys->dags[d]->elems[i].opt_punit = p;
    } else if (sscanf(line, "y_%d_%d %lf", &s, &m, &val) == 3 && FEQUAL(val, 1.0f)) {
      assert(s >= 0 && s < p_sys->n_islands);
      assert(m >= 0 && m < p_sys->islands[s].n_freqs);
      p_sys->islands[s].opt_freq = m;
    } else if (sscanf(line, "Q_%d_%d_%d %lf", &o, &s, &p, &val) == 4) {
      val *= lp_scale;
      assert(s >= 0 && s < p_sys->n_islands);
      assert(p >= 0 && p < p_sys->islands[s].n_punits);
      assert(o >= 0 && o < p_sys->n_omp_envs);
      p_sys->omp_envs[o].worker_threads[s][p].opt_budget = val;
    } else if (sscanf(line, "P_%d_%d_%d %lf", &o, &s, &p, &val) == 4) {
      val *= lp_scale;
      assert(s >= 0 && s < p_sys->n_islands);
      assert(p >= 0 && p < p_sys->islands[s].n_punits);
      assert(o >= 0 && o < p_sys->n_omp_envs);
      p_sys->omp_envs[o].worker_threads[s][p].opt_period = val;
    }
  }
  free(line);
  fclose(f);
  return rv;
}

static char cmd[1024];
int solve_gurobi(sys_t *p_sys) {
  printf("Solving with gurobi...\n");
  int ret = snprintf(cmd, sizeof(cmd),
    "rm -f %s_log.txt && gurobi_cl Threads=%d NonConvex=2 ResultFile=%s LogFile=%s_log.txt SolFiles=%s %s\n",
    out_sols_pname, gurobi_max_threads, out_sol_fname, out_sols_pname, out_sols_pname, out_lp_fname);
  assert(ret < sizeof(cmd));
  printf("Running cmd: %s\n", cmd);
  int rv = system(cmd);
  if (rv == -1) {
    fprintf(stderr, "Error: %s\n", strerror(errno));
    exit(1);
  }
  ret = snprintf(cmd, sizeof(cmd),
           "grep 'Optimal solution found' %s_log.txt > /dev/null 2>&1 "
           "&& ! grep 'model may be infeasible' %s_log.txt > /dev/null 2>&1"
           "&& ! grep 'max constraint violation .* exceeds tolerance' %s_log.txt > /dev/null 2>&1",
           out_sols_pname, out_sols_pname, out_sols_pname);
  assert(ret < sizeof(cmd));
  printf("Running cmd: %s\n", cmd);
  rv = system(cmd);
  if (rv != 0) {
    fprintf(stderr, "Error: Gurobi could not find an optimal feasible solution!\n");
    exit(1);
  }
  return parse_gurobi_sol(p_sys, out_sol_fname);
}

double punit_tot_util(sys_t *p_sys, int s, int p) {
  island_t *p_isl = &p_sys->islands[s];
  int m = p_isl->opt_freq;
  double u_punit_tot = 0.0;
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    double u_dag_max = 0.0; // max U occupied by p_dag on punit (s, p)
    for (int ur = 0; ur < p_dag->unrelated_size; ur++) {
      double u_dag_sum = 0.0;
      for (int e = 0; e < p_dag->num_elems; e++) {
        dag_elem_t *p_elem = &p_dag->elems[e];
        if ((p_dag->unrelated[ur] & (1<<e)) && !p_elem->fictitious && p_elem->omp_env_id == -1 && p_elem->opt_island == s && p_elem->opt_punit == p) {
          double u = dag_elem_wcet_scaled(p_sys, p_dag, e, p_isl, m)
            / (p_elem->d != -1 ? p_elem->d : p_dag->deadline);
          u_dag_sum += u;
        }
      }
      if (u_dag_sum > u_dag_max)
        u_dag_max = u_dag_sum;
    }
    u_punit_tot += u_dag_max;
  }

  if (p_isl->type != ACCELERATOR) {
    // adding the contribution of OpenMP worker threads
    for (int o = 0; o < p_sys->n_omp_envs; ++o) {
      omp_wt_t *wt = &(p_sys->omp_envs[o].worker_threads[s][p]);
      u_punit_tot += wt->opt_budget / wt->opt_period;
    }
  }

  return u_punit_tot;
}

void place_wf_elem_island(sys_t *p_sys, dag_elem_t *p_elem, int s) {
  int opt_p = -1;
  double opt_u = 1.0;
  island_t *p_isl = &p_sys->islands[s];
  assert(p_isl->type != ACCELERATOR || (p_elem->accelerators & (1 << s)) != 0);
  for (int p = 0; p < p_isl->n_punits; p++) {
    double u_old = punit_tot_util(p_sys, s, p);
    p_elem->opt_island = s;
    p_elem->opt_punit = p;
    double u_new = punit_tot_util(p_sys, s, p);
    LOG(DEBUG, "(%g/%g): s=%d, p=%d, u_old=%g, u_new=%g\n", p_elem->C, p_elem->d, s, p, u_old, u_new);
    if (opt_p == -1 || u_new < opt_u) {
      opt_p = p;
      opt_u = u_new;
    }
  }
  p_elem->opt_island = s;
  p_elem->opt_punit = opt_p;
}

/*
 * worst-fit: search for (island, core) minimizing the max occupied bandwidth after addition of p_elem
 *   bandwidths obtained dividing by either the relative deadlines if present, or the dag deadlines
 *   occupied bandwidth computed according to the unrelated[] sets, that must have been pre-computed
 */
void place_wf_elem(sys_t *p_sys, dag_elem_t *p_elem) {
  int opt_s = -1, opt_p = -1;
  double opt_u = 1.0;
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    if (p_isl->type == ACCELERATOR && (p_elem->accelerators & (1 << s)) == 0)
      continue;
    for (int p = 0; p < p_isl->n_punits; p++) {
      double u_old = punit_tot_util(p_sys, s, p);
      p_elem->opt_island = s;
      p_elem->opt_punit = p;
      double u_new = punit_tot_util(p_sys, s, p);
      LOG(DEBUG, "(%g/%g): s=%d, p=%d, u_old=%g, u_new=%g\n", p_elem->C, p_elem->d, s, p, u_old, u_new);
      if (opt_s == -1 || u_new < opt_u) {
        opt_s = s;
        opt_p = p;
        opt_u = u_new;
      }
    }
  }
  double u_old = punit_tot_util(p_sys, opt_s, opt_p);
  p_elem->opt_island = opt_s;
  p_elem->opt_punit = opt_p;
  double u_new = punit_tot_util(p_sys, opt_s, opt_p);
  LOG(DEBUG, "(%g/%g): opt_s=%d, opt_p=%d, u_old=%g, u_new=%g\n", p_elem->C, p_elem->d, opt_s, opt_p, u_old, u_new);
}

void place_wf(sys_t *p_sys) {
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    for (int e = 0; e < p_dag->num_elems; e++) {
      dag_elem_t *p_elem = &p_dag->elems[e];
      if (p_elem->opt_island == -1) {
        place_wf_elem(p_sys, p_elem);
      }
    }
  }
}

int check_constraints(sys_t *p_sys) {
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    for (int i = 0; i < p_dag->num_elems; i++) {
      dag_elem_t *p_elem = &p_dag->elems[i];
      if (p_elem->opt_island == -1 || p_elem->opt_punit == -1)
        return 0;
    }
    if (!dag_check_deadlines(p_dag, p_dag->first, 0.0, 1))
      return 0;
  }
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    if (p_isl->type == ACCELERATOR)
      continue;
    for (int p = 0; p < p_isl->n_punits; p++) {
      double u = punit_tot_util(p_sys, s, p);
      if (u > max_util) {
        LOG(DEBUG, "Utilization of punit (%d,%d) over limit: u=%lf, max_util=%lf\n",
            s, p, u, max_util);
        return 0;
      }
    }
  }
  return 1;
}

int solve_heur(sys_t *p_sys) {
  printf("Solving with greedy heuristics based on worst-fit...\n");

  if (out_sols_pname == NULL) {
    out_sols_pname = strdup("/tmp/dag-XXXXXX");
    out_sols_pname = mkdtemp(out_sols_pname);
  }
  // check assumption: islands ordered from max to min capacity
  for (int s = 0; s < p_sys->n_islands - 1; s++) {
    check(p_sys->islands[s].type != CPU || p_sys->islands[s+1].type != CPU
          || p_sys->islands[s].capacity >= p_sys->islands[s+1].capacity,
          "Islands %d and %d are not in correct capacity order", s, s+1);
  }
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    // check assumption: OPPs ordered from max to min frequency
    for (int m = 0; m < p_isl->n_freqs - 1; m++) {
      check(p_isl->freqs[m] >= p_isl->freqs[m+1],
            "Island %d has OPPs %d and %d with incorrect frequency order", s, m, m+1);
    }
    p_isl->opt_freq = 0;
  }
  sys_set_deadlines(p_sys);
  place_wf(p_sys);
  if (!check_constraints(p_sys))
    return 0;

  // try to fill lower-capacity islands if compatible with constraints
  for (int s = 0; s < p_sys->n_islands; s++) {
    if (p_sys->islands[s].capacity < 1) {
      for (int d = 0; d < p_sys->n_dags; d++) {
        dag_t *p_dag = p_sys->dags[d];
        for (int e = 0; e < p_dag->num_elems; e++) {
          dag_elem_t *p_elem = &p_dag->elems[e];
          int opt_s = p_elem->opt_island;
          int opt_p = p_elem->opt_punit;
          if (opt_s == s || p_sys->islands[opt_s].capacity < p_sys->islands[s].capacity)
            continue;
          p_elem->opt_island = -1;
          p_elem->opt_punit = -1;
          // TODO: check delta power before doing this?
          place_wf_elem_island(p_sys, p_elem, s);
          sys_set_deadlines(p_sys);
          if (!check_constraints(p_sys)) {
            // unfeasible, rollback relocation of elem
            p_elem->opt_island = opt_s;
            p_elem->opt_punit = opt_p;
          }
        }
      }
    }
  }

  sys_set_deadlines(p_sys);
  assert(check_constraints(p_sys));

  // try to lower OPPs if compatible with constraints
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    double u_max = 0.0;
    for (int p = 0; p < p_isl->n_punits; p++) {
      double u = punit_tot_util(p_sys, s, p);
      if (u > u_max)
        u_max = u;
    }
    if (u_max == 0.0) {
      // entire island is empty, just set it at its min frequency
      p_isl->opt_freq = p_isl->n_freqs - 1;
      continue;
    }
    while (p_isl->opt_freq < p_isl->n_freqs - 1 && u_max * p_isl->freqs[0] / p_isl->freqs[p_isl->opt_freq + 1]) {
      p_isl->opt_freq++;
      sys_set_deadlines(p_sys);
      if (!check_constraints(p_sys)) {
        p_isl->opt_freq--;
        break;
      }
    }
  }

  sys_set_deadlines(p_sys);
  assert(check_constraints(p_sys));

  p_sys->opt_pow = sys_tot_pow(p_sys);

  return 1;
}

dag_t *build_dag_random(int max_period, int max_elems, int wcet_max, int max_edges) {
  dag_t *p_dag = malloc(sizeof(dag_t));
  assert(p_dag != NULL);
  p_dag->unrelated = NULL;
  p_dag->relatives = NULL;
  int period = max_period/4 + 3*max_period/4 * (long)rand() / (RAND_MAX + 1l);
  int deadline = period/4 + 3*period/4 * (long)rand() / (RAND_MAX + 1l);
  // sched deadline does not accept a period/deadline < 1024 (time unit is ns)
  assert(period >= 1024);
  assert(deadline >= 1024);
  dag_init(p_dag, deadline, period);
  // 3+ means that the dag must have at least one source/target node + another node
  int n_elems = 3 + (max_elems - 2) * (long)rand() / (RAND_MAX + 1l);
  // printf("deadline %d, period %d, n_elems %d\n", deadline, period, n_elems);
  for (int i = 0; i < n_elems; i++) {
    // sched deadline does not accept a task runtime < 1024
    int wcet = wcet_max/4 + 3*wcet_max/4 * (long)rand() / (RAND_MAX + 1l);
    assert(wcet >= 1024);
    int wcet_ns = wcet_max/4 * (long)rand() / (RAND_MAX + 1l);
    dag_add_elem(p_dag, wcet, wcet_ns, -1);
  }
  int n_edges = max_edges/4 + 3*max_edges/4 * (long)rand() / (RAND_MAX + 1l);
  do {
    int i = (n_elems-1) * (long)rand() / (RAND_MAX + 1l);
    int j = i+1 + (n_elems-1-i) * (long)rand() / (RAND_MAX + 1l);
    LOG(DEBUG,"i=%d,j=%d\n", i, j);
    assert(i >= 0 && i < n_elems-1 && j > i && j < n_elems);
    // does not allow a starting node to connect directly to the last node
    if (i==0 &&  j==(n_elems-1))
        continue;
    if (p_dag->elems[i].n_next < MAX_NEXT && p_dag->elems[j].n_prev < MAX_PREV){
      dag_add_prev(p_dag, j, i);
    }
  } while (--n_edges > 0);
  // this part makes sure that the 1st and last tasks are the single input/output tasks
  int p = 0;
  for (int i = 1; i < n_elems; i++){
    if (!dag_is_reachable(p_dag, 0, i)){
        if (p_dag->elems[p].n_next < MAX_NEXT && p_dag->elems[i].n_prev < MAX_PREV){
          dag_add_prev(p_dag, i, p);
        }else{
          free(p_dag);
          return NULL;
        }
    }else{
      p = i;
    }
  }
  int n = n_elems-1;
  for (int i = n_elems-2; i >=0; i--){
    if (!dag_is_reachable(p_dag, i, n_elems-1)){
        if (p_dag->elems[i].n_next < MAX_NEXT && p_dag->elems[n].n_prev < MAX_PREV){
          dag_add_prev(p_dag, n, i);
        }else{
          free(p_dag);
          return NULL;
        }
    }
  }

  return p_dag;
}

void sys_dump_output_yaml(sys_t *p_sys, char *solver_name, char *fname,
    char *dag_name, char *platform_name, unsigned long exec_time_us){
    FILE *f = fopen(fname, "w");
    assert(f != NULL);
    assert(platform_name != NULL);
    assert(dag_name != NULL);

    // complement the output yaml with more information from the main() context:
    fprintf(f, "tool_name: \"%s\"\n",solver_name);
    fprintf(f, "platform_filename: \"\"\n"); // dag.c has not file describing the hw platform
    fprintf(f, "platform_name: \"%s\"\n", platform_name);
    fprintf(f, "dag_filename: \"%s\"\n", dag_name); // in this case, its the lp filename wo extension
    fprintf(f, "execution_time_us: %lu\n", exec_time_us);
    fprintf(f, "cpu_load: %lf\n", sys_load(p_sys));

    // TODO: add yaml attribute ... need to parse the gurobi log file
    // - gap, to say whehter the solution is optimal
    fprintf(f, "n_dags: %d\n",p_sys->n_dags);
    assert(p_sys->opt_pow!=0);
    fprintf(f, "power: %f\n",p_sys->opt_pow);

    fprintf(f, "islands:\n");
    for (int s = 0; s < p_sys->n_islands; s++) {
        island_t *p_isl = &p_sys->islands[s];
        fprintf(f, "    - capacity: %f\n", p_isl->capacity);
        fprintf(f, "      frequency: %d\n", (int)p_isl->freqs[p_isl->opt_freq]);
        fprintf(f, "      tasks: [\n");
        // get all tasks that are mapped to this island
        for (int d = 0; d < p_sys->n_dags; d++) {
            dag_t *p_dag = p_sys->dags[d];
            for (int e = 0; e < p_dag->num_elems; e++) {
                dag_elem_t *p_elem = &p_dag->elems[e];
                if (p_elem->opt_island == s){
                    fprintf(f, "                [%d,%d],\n", d, e);
                }
            }
        }
        fprintf(f, "             ]\n");
        fprintf(f, "      pus: \n");
        for (int p = 0; p < p_isl->n_punits; p++) {
            fprintf(f, "            - [\n");
            // get all tasks that are mapped onto unit p and island s
            for (int d = 0; d < p_sys->n_dags; d++) {
                dag_t *p_dag = p_sys->dags[d];
                for (int e = 0; e < p_dag->num_elems; e++) {
                    dag_elem_t *p_elem = &p_dag->elems[e];
                    if (p_elem->opt_island == s && p_elem->opt_punit == p){
                        // wcet must be truncated, otherwise it created situations where wcet/del > 1
                        double wcet = dag_elem_wcet_scaled(p_sys, p_dag, e, p_isl, p_isl->opt_freq);
                        double util = wcet / p_dag->elems[e].d;
                        assert(util <= 1.0);
                        fprintf(f, "                 [%f,%d,%d],\n", util, d, e);
                    }
                }
            }
            fprintf(f, "              ]\n");
        }
    }

    fprintf(f, "tasks:\n");
    for (int d = 0; d < p_sys->n_dags; d++) {
        dag_t *p_dag = p_sys->dags[d];
        for (int e = 0; e < p_dag->num_elems; e++) {
            dag_elem_t *p_elem = &p_dag->elems[e];
            fprintf(f, "    - id: [%d,%d]\n", d, e);
            double dline = p_dag->elems[e].d;
            fprintf(f, "      deadline: %f\n", dline);
            island_t *p_isl = &p_sys->islands[p_elem->opt_island];
            double wcet = dag_elem_wcet_scaled(p_sys, p_dag, e, p_isl, p_isl->opt_freq);
            fprintf(f, "      wcet: %f\n", wcet);
            assert (wcet <= dline);
        }
    }

    fclose(f);
}

dag_t *sys_new_dag(sys_t *p_sys, double deadline, double period) {
  dag_t *p_dag = malloc(sizeof(dag_t));
  check(p_dag != NULL, "Out of memory%s","");
  dag_init(p_dag, deadline, period);
  sys_add_dag(p_sys, p_dag);
  return p_dag;
}

void dag_add_acc_power(int acc_id, double power){
  assert(p_sys->n_islands > acc_id);
  dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
  dag_elem_t *p_elem = &(p_dag->elems[p_dag->num_elems - 1]);
  p_elem->isl_pow[acc_id] = power;
  LOG(INFO,"Added additional power consumption of task %d in dag %d for the accelerated island %d\n", p_dag->num_elems - 1, p_sys->n_dags - 1, acc_id);
}

void dag_add_acc_task_id(int acc_id, int dag_id, int task_id, double C, double C_ns){
    assert(p_sys->n_islands > acc_id);
    dag_t *p_dag = p_sys->dags[dag_id];
    dag_elem_t *p_elem = &(p_dag->elems[task_id]);
    p_elem->accelerators |= (1 << acc_id);
    p_elem->isl_C[acc_id] = C;
    p_elem->isl_C_ns[acc_id] = C_ns;
    if (C == C_NONE)
      LOG(INFO,"Added implementation of task %d in dag %d for the accelerated island %d\n", p_dag->num_elems - 1, p_sys->n_dags - 1, acc_id);
    else
      LOG(INFO,"Added implementation of task %d in dag %d for the accelerated island %d with custom exec time C=%g, C_ns=%g\n", p_dag->num_elems - 1, p_sys->n_dags - 1, acc_id, C, C_ns);
}

void dag_add_acc(int acc_id, double C, double C_ns){
    dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
    dag_add_acc_task_id(acc_id, p_sys->n_dags - 1, p_dag->num_elems - 1, C, C_ns);
}

void dag_add_omp_env(int omp_env_id){
  assert(p_sys->n_omp_envs > omp_env_id);
  dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
  dag_add_omp_env_task_id(omp_env_id, p_sys->n_dags - 1, p_dag->num_elems - 1);
}

void dag_add_omp_env_task_id(int omp_env_id, int dag_id, int task_id){
  assert(p_sys->n_omp_envs > omp_env_id);
  dag_t *p_dag = p_sys->dags[dag_id];
  dag_elem_t *p_elem = &(p_dag->elems[task_id]);
  p_elem->omp_env_id = omp_env_id;
  LOG(INFO,"Task %d in dag %d was added to the OpenMP environment %d\n", task_id, dag_id, omp_env_id);
}

void sys_add_omp_envs(sys_t *p_sys, int n_omp_envs) {
  assert(n_omp_envs < MAX_OMP_ENVS);

  for (; n_omp_envs > 0; --n_omp_envs) {
    omp_env_t *env = &p_sys->omp_envs[p_sys->n_omp_envs++];
    env->n_islands = p_sys->n_islands;

    for (int i = 0; i < p_sys->n_islands; ++i) {
      island_t *isl = &p_sys->islands[i];

      if (isl->type == ACCELERATOR) {
        continue;
      }

      env->n_punits[i] = isl->n_punits;

      for (int j = 0; j < isl->n_punits; ++j) {
        omp_wt_t *wt = &env->worker_threads[i][j];
        wt->p_island = isl;
      }
    }
  }
}
